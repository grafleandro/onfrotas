<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Pusher\Pusher;

class OrdemServicoEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message)
    {
        $this->message = $message;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return void
     * @throws \Pusher\PusherException
     */
    public function broadcastOn()
    {
        $options = array(
            'cluster' => 'mt1',
            'encrypted' => false
        );

        $pusher = new Pusher(
            '1b2b3fcd2d70a93b38d1',
            'bac2526a35a3f7c11135',
            '570428',
            $options
        );

        $data['message'] = 'hello world';
        $pusher->trigger('channel-name', 'my-event', $data);
    }
}
