<?php

namespace App\Http\Controllers\Abastecimento;

use App\Repository\AbastecimentoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class abastecimentoController extends Controller
{
    private $AbastecimentoRepository;
    private $empresa;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(AbastecimentoRepository $abastecimentoRepository)
    {
        $this->AbastecimentoRepository = $abastecimentoRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
//            $errors = $ordemServicoRequest->validarOrdemServico($ordemServicoRequest->all());
//
//            if (sizeof($errors)) {
//                Common::setError($errors['alert']);
//            }

            $resposta = $this->AbastecimentoRepository->salvarDados($request);

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('abastecimento/cadastrar');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
