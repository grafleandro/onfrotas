<?php

namespace App\Http\Controllers\Aparelho;

use App\Http\Controllers\Controller;
use App\Repository\AparelhoRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AparelhoController extends Controller
{
    private $aparelhoRepository;

    public function __construct(AparelhoRepository $aparelhoRepository)
    {
        $this->aparelhoRepository = $aparelhoRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function index(Request $request)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'index');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function create(Request $request)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'create');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse|Response
     */
    public function store(Request $request)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'store');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function show(Request $request, $id)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'show');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function edit(Request $request, $id)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'edit');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return JsonResponse|Response
     */
    public function update(Request $request, $id)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'update');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse|Response
     */
    public function destroy(Request $request, $id)
    {
        try{
            $this->aparelhoRepository->registrarArquivo($request, 'destroy');

            return response()->json(['success' => 1]);
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
