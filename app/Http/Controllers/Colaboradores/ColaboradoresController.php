<?php

namespace App\Http\Controllers\Colaboradores;

use App\Http\Requests\ColaboradorRequest;
use App\Repository\ColaboradorRepository;
use App\Utils\Common;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ColaboradoresController extends Controller
{
    private $ColaboradorRepository;


    public function __construct(ColaboradorRepository $colaboradorRepository)
    {
        $this->ColaboradorRepository = $colaboradorRepository;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request->all()['search_value'])){
            $autocomplete = $this->ColaboradorRepository->autocomplete($request->all()['search_value']);

            return response()->json(['success' => 1, 'colaboradores' => $autocomplete]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ColaboradorRequest $colaboradorRequest
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function store(ColaboradorRequest $colaboradorRequest)
    {
        try {

            if(!isset($colaboradorRequest->all()['usuario_primario'])) {
                $errors = $colaboradorRequest->validarColaborador($colaboradorRequest->all());

                if (sizeof($errors)) {
                    Common::setError($errors['alert']);
                }
            }
            $resposta = $this->ColaboradorRepository->salvarDados($colaboradorRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }
        }catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $resource)
    {
        try{
            switch ($resource){
                case 'cadastrar':
                    return view('colaboradores/cadastrar');
                    break;
                case 'listar':
                    return view('colaboradores/listar');
                    break;
                case 'tabela':
                    return $this->ColaboradorRepository->tabela();
                    break;
                case 'cadastrar_empresa_usuario':
                    $menus = $this->ColaboradorRepository->selecionarMenus();
                    return view('empresa/cadastrar_usuario_primario', ['menus' => $menus, 'empresa' => ($request->all()['empresa']) ?? '']);
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function edit($id)
    {
        try{
            $dadosUsuario = $this->ColaboradorRepository->findById($id);


            $dadosUsuario['pessoa']['pess_data_nasc'] = Carbon::createFromFormat('Y-m-d', $dadosUsuario['pessoa']['pess_data_nasc'])->format('d/m/Y');
            $dadosUsuario['pessoa']['pess_vencimento_cnh'] = Carbon::createFromFormat('Y-m-d', $dadosUsuario['pessoa']['pess_vencimento_cnh'])->format('d/m/Y');

            if(count($dadosUsuario)){
                $resposta = [
                    'success' => 1,
                    'view' => view('colaboradores/formulario', ['colaborador' => $dadosUsuario])->render()
                ];
                return response()->json($resposta);
            }

            return response()->json(['success' => 0]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ColaboradorRequest $colaboradorRequest, $id)
    {
        try{

            $errors = $colaboradorRequest->validarColaboradorUpdate($colaboradorRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->ColaboradorRepository->atualizarUsuario($colaboradorRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->ColaboradorRepository->deletarUsuario($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
