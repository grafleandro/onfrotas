<?php

namespace App\Http\Controllers\Configuracao;

use App\Http\Requests\StatusOSRequest;
use App\Repository\StatusOSRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusOrdemServicoController extends Controller
{
    private $StatusOrdemServicoRepository;


    public function __construct(StatusOSRepository $statusOSRepository)
    {
        $this->StatusOrdemServicoRepository = $statusOSRepository;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StatusOSRequest $statusOSRequest)
    {
        try {
            $errors = $statusOSRequest->validarStatus($statusOSRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->StatusOrdemServicoRepository->salvarDados($statusOSRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }
        }catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('configuracao/status_os/cadastrar');
                break;
            case 'listar':
                return view('configuracao/status_os/listar');
                break;
            case 'tabela':
                return $this->StatusOrdemServicoRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $dadosStatus = $this->StatusOrdemServicoRepository->findById($id);

            if(count($dadosStatus)){
                $resposta = [
                    'success' => 1,
                    'view' => view('configuracao/status_os/formulario', ['status' => $dadosStatus])->render()
                ];
                return response()->json($resposta);
            }

            return response()->json(['success' => 0]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StatusOSRequest $statusOSRequest, $id)
    {
        try{

            $errors = $statusOSRequest->validarStatus($statusOSRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->StatusOrdemServicoRepository->atualizarUsuario($statusOSRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->StatusOrdemServicoRepository->deletarUsuario($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        };
    }
}
