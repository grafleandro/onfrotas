<?php

namespace App\Http\Controllers\Dashboard;

use App\Repository\DashboardRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboarController extends Controller
{
    private $dashboardRepository;

    public function __construct(DashboardRepository $dashboardRepository)
    {
        $this->dashboardRepository = $dashboardRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     */
    public function show($resource)
    {
        try{
            switch ($resource){
                case 'aniversariantes':
                    return view('dashboard/aniversariantes', ['aniversariantes' => $this->dashboardRepository->aniversariantes()]);
                    break;
                case 'grafico-os':
                    return response()->json(['success' => 1, 'dados' => $this->dashboardRepository->graficoOS()]);
                    break;
                case 'doc_veiculo':
                    return view('dashboard/doc_veiculo');
                case 'tabela_veiculo_doc_vencido':
                    return $this->dashboardRepository->veiculo_doc_vencido();
                    break;
                case 'cnh_vencidas':
                    return view('dashboard/cnh_vencida');
                    break;
                case 'tabela_cnh_vencida':
                    return $this->dashboardRepository->cnh_vencida();
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->doc_veiculo) {
                $docPago = $this->dashboardRepository->docPago($id);

                if ($docPago) {
                    return response()->json(['success' => $docPago]);
                }

                return response()->json(['success' => $docPago, 'alert' => 'Erro ao salvar a informação de documento pago.']);
            }elseif ($request->cnh_vencida){
                $cnh_renovada = $this->dashboardRepository->docPago($id);

                if ($docPago) {
                    return response()->json(['success' => $docPago]);
                }

                return response()->json(['success' => $docPago, 'alert' => 'Erro ao salvar a informação de documento pago.']);
            }


        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
