<?php

namespace App\Http\Controllers\Detran;

use App\Http\Requests\DetranRequest;
use App\Repository\DetranRepository;
use App\Utils\Common;
use App\Utils\DetranUtils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DetranController extends Controller
{
    private $detranRepository;

    public function __construct(DetranRepository $detranRepository)
    {
        $this->detranRepository = $detranRepository;
    }

    public function consultarDebito(DetranRequest $detranRequest){
        try{

            $errors = $detranRequest->validarDetran($detranRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            if($detranRequest->all()['retorno'] !== 'json' && $detranRequest->all()['retorno']  !== 'array'){
                Common::setError('Você precisa informar o tipo do retorno: JSON ou ARRAY');
            }

            $outros = (isset($detranRequest->all()['outros']) && !empty($detranRequest->all()['outros'])) ? $detranRequest->all()['outros'] : [DetranUtils::_TODOS];

            $resposta = $this->detranRepository->extrairInformacoes($detranRequest->all()['placa'], $detranRequest->all()['renavam'], $outros);

            if($detranRequest->all()['retorno'] === 'json'){
                return response()->json($resposta);
            } else if($detranRequest->all()['retorno'] === 'array'){
                return $resposta;
            }

        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
