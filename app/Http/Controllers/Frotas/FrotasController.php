<?php

namespace App\Http\Controllers\Frotas;

use App\Http\Requests\FrotasRequest;
use App\Repository\FrotasRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Psy\Command\Command;

class FrotasController extends Controller
{
    private $frotasRepository;

    public function __construct(FrotasRepository $frotasRepository)
    {
        $this->frotasRepository = $frotasRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FrotasRequest $frotasRequest
     * @return \Illuminate\Http\Response
     */
    public function store(FrotasRequest $frotasRequest)
    {
        try{
            $errors = $frotasRequest->validarFrota($frotasRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->frotasRepository->salvarDados($frotasRequest->all());

            if(!$resposta){
                Common::setError('Houve um erro ao salvar os dados!');
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $resource
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('frotas/cadastrar');
                break;
            case 'listar':
                return view('frotas/listar');
                break;
            case 'tabela':
                return $this->frotasRepository->tabela($request->all());
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function edit($id)
    {
        try{
            $dadosFrota = $this->frotasRepository->editarFrota($id);

            return view('frotas/cadastrar', $dadosFrota);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FrotasRequest $frotasRequest
     * @param  int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\JsonResponse|\Illuminate\View\View
     */
    public function update(FrotasRequest $frotasRequest, $id)
    {
        try{
            $errors = $frotasRequest->validarFrota($frotasRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->frotasRepository->atualizarFrota($frotasRequest, $id);

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->frotasRepository->deletarFrota($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
