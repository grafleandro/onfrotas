<?php

namespace App\Http\Controllers;

use App\Model\PessoaModel;
use App\Repository\HomeRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    private $homeRepository;

    /**
     * Create a new controller instance.
     *
     * @param HomeRepository $homeRepository
     */
    public function __construct(HomeRepository $homeRepository)
    {
        $this->middleware('auth');

        $this->homeRepository = $homeRepository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', array(
            'aniversariantes' => $this->homeRepository->aniversariantes(),
            'ordem_servico_solicitacao' => $this->homeRepository->ordemServicoSolicitacao(),
            'veiculo_doc_vencimento' => $this->homeRepository->documentoVeiculoVencimento(),
            'cnh_vencida' => count($this->homeRepository->cnhMotoristaVencida()),
        ));
    }
}
