<?php

namespace App\Http\Controllers\Mecanicas;

use App\Http\Requests\MecanicaRequest;
use App\Repository\MecanicaRepository;
use App\Utils\Common;
use App\Utils\OrdemServicoUtils;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MecanicasController extends Controller
{
    private $MecanicaRepository;


    public function __construct(MecanicaRepository $mecanicaRepository)
    {
        $this->MecanicaRepository = $mecanicaRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try{
            if(isset($request->all()['search_value'])){
                $autocomplete = $this->MecanicaRepository->autocomplete($request->all()['search_value']);

                return response()->json(['success' => 1, 'oficina' => $autocomplete]);
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MecanicaRequest $mecanicaRequest
     * @return \Illuminate\Http\Response
     */
    public function store(mecanicaRequest $mecanicaRequest)
    {
        try{
            $errors = $mecanicaRequest->validarMecanica($mecanicaRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->MecanicaRepository->salvarDados($mecanicaRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('mecanicas/cadastrar' , [
                    'mecanica' => ['ofic_interna_externa' => OrdemServicoUtils::_EXTERNA]
                ]);
                break;
            case 'listar':
                return view('mecanicas/listar');
                break;
            case 'tabela':
                return $this->MecanicaRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function edit($id)
    {
        try{
            $dadosMecanica = $this->MecanicaRepository->findById($id);

            if(count($dadosMecanica)){
                $resposta = [
                    'success' => 1,
                    'view' => view('mecanicas/formulario', ['mecanica' => $dadosMecanica])->render()
                ];
                return response()->json($resposta);
            }

            return response()->json(['success' => 0]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MecanicaRequest $mecanicaRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(MecanicaRequest $mecanicaRequest, $id)
    {
        try{

            $errors = $mecanicaRequest->validarMecanica($mecanicaRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->MecanicaRepository->atualizarMecanica($mecanicaRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->MecanicaRepository->deletarMecanica($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
