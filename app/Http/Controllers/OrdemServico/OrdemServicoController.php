<?php

namespace App\Http\Controllers\OrdemServico;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrdemServicoRequest;
use App\Model\PessoaModel;
use App\Repository\OrdemServicoRepository;
use App\Utils\Common;
use App\Utils\OrdemServicoUtils;
use App\Utils\UsuarioUtils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrdemServicoController extends Controller
{

    private $OrdemServicoRepository;
    private $empresa;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(OrdemServicoRepository $ordem_servicoRepository)
    {
        $this->OrdemServicoRepository = $ordem_servicoRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $dadosFiltro = $request->all();

        if($dadosFiltro['tipo'] == 'info'){
            $resposta = $this->OrdemServicoRepository->ordemServicoInfo($dadosFiltro);
        }

        return response()->json(['success' => 1, 'historico' => $resposta]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param OrdemServicoRequest $ordemServicoRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(OrdemServicoRequest $ordemServicoRequest)
    {
        $errors = $ordemServicoRequest->validarOrdemServico($ordemServicoRequest->all());

        if (sizeof($errors)) {
            Common::setError($errors['alert']);
        }

        $resposta = $this->OrdemServicoRepository->salvarDados($ordemServicoRequest);

        if(!$resposta){
            Common::setError('Houve um erro ao salvar os dados!');
        }

        return response()->json($resposta);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $resource
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Throwable
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                $tipoManutecao = $this->OrdemServicoRepository->getTipoManutencao();

                $prioridadeManutencao = $this->OrdemServicoRepository->getPrioridadeManutencao();

                $ordemServicoStatus = $this->OrdemServicoRepository->getOrdemServicoStatus($request);

                return view(
                    'ordem_servico/cadastrar', [
                        'prioridade'  => $prioridadeManutencao,
                        'tipo_manutencao' => $tipoManutecao,
                        'status_os' => $ordemServicoStatus,
                        'perfil' => $request->user()->perfil,
                        'ordem_servico' => [
                            'executar' => OrdemServicoUtils::_EXTERNA,
                            'os_status' => (UsuarioUtils::usuarioEhGestor($request->user()->perfil)) ? null : OrdemServicoUtils::_SOLICITACAO,
                        ]
                    ]);
                break;
            case 'listar':
                $ordemServicoStatus = $this->OrdemServicoRepository->getOrdemServicoStatus($request);

                return view('ordem_servico/listar', ['status_os' => $ordemServicoStatus] );
                break;
            case 'tabela':
                return $this->OrdemServicoRepository->tabela($request);
                break;
            case 'imprimir':
                return $this->OrdemServicoRepository->imprimir($request->ordem_servico);
                break;
            case 'view':
                $dadosOS = $this->OrdemServicoRepository->editarOS($request, $request->ordem_servico);
                $resposta = [
                    'success' => 1,
                    'view' => view('ordem_servico/formulario_gestor', $dadosOS)->render(),
                    ];
                return response()->json($resposta);
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        try{
            $dadosOS = $this->OrdemServicoRepository->editarOS($request, $id);

            return view('ordem_servico/cadastrar', $dadosOS);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param OrdemServicoRequest $ordemServicoRequest
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(OrdemServicoRequest $ordemServicoRequest, $id)
    {
        try{
            if(isset($ordemServicoRequest->all()['status'])){
                $resposta = $this->OrdemServicoRepository->atualizarStatus($ordemServicoRequest->all(), $ordemServicoRequest->all()['orse']);
            } else {
                $errors = $ordemServicoRequest->validarOrdemServico($ordemServicoRequest->all());

                if (sizeof($errors)) {
                    Common::setError($errors['alert']);
                }

                $resposta = $this->OrdemServicoRepository->atualizarDados($ordemServicoRequest, $id);
            }

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->OrdemServicoRepository->deletarOrdemServico($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
