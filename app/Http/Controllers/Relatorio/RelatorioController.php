<?php

namespace App\Http\Controllers\Relatorio;

use App\Repository\RelatorioRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelatorioController extends Controller
{
    private $RelatorioRepository;


    public function __construct(RelatorioRepository $relatorioRepository)
    {
        $this->RelatorioRepository = $relatorioRepository;

        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$resource)
    {
        try {
            switch ($resource) {
                case 'veiculo':
                    return view('relatorio/veiculo/veiculo');
                    break;
                case 'manutencao':
                    return view('relatorio/manutencao/manutencao');
                    break;
                case 'tabela_veiculo':
                return $this->RelatorioRepository->relatorioVeiculo($request->veiculo);
                    break;
                case 'tabela_manutencao':
                    return $this->RelatorioRepository->relatorioManutencao($request->all());
                    break;
                case 'imprimir':
                    return $this->RelatorioRepository->imprimir($request->ordem_servico);
                    break;
                case 'imprimirManutencao':
                    return $this->RelatorioRepository->imprimirManutencao($request->all());
                    break;
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

}
