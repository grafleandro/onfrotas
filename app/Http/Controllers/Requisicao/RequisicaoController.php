<?php

namespace App\Http\Controllers\Requisicao;

use App\Http\Requests\requisicaoRequest;
use App\Repository\RequisicaoRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequisicaoController extends Controller
{
    private $RequisicaoRepository;


    public function __construct(RequisicaoRepository $requisicaoRepository)
    {
        $this->RequisicaoRepository = $requisicaoRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

    }
//
//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//        //
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param MecanicaRequest $mecanicaRequest
//     * @return \Illuminate\Http\Response
//     */
    public function store(requisicaoRequest $requisicaoRequest)
    {
        try{
            $errors = $requisicaoRequest->validarRequisicao($requisicaoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->RequisicaoRepository->salvarDados($requisicaoRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                return view('requisicao/cadastrar');
                break;
            case 'listar':
                return view('requisicao/listar');
                break;
            case 'tabela':
                return $this->RequisicaoRepository->tabela($request->all());
                break;
            case 'imprimir':
                return $this->RequisicaoRepository->imprimir($request->ordem_servico);
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function edit($id)
    {
        try{
            $dadosRequisicao = $this->RequisicaoRepository->findById($id);

            if(count($dadosRequisicao)){
                $resposta = [
                    'success' => 1,
                    'view' => view('requisicao/formulario', ['requisicao' => $dadosRequisicao])->render()
                ];
                return response()->json($resposta);
            }

            return response()->json(['success' => 0]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MecanicaRequest $mecanicaRequest
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(requisicaoRequest $requisicaoRequest, $id)
    {
        try{

            $errors = $requisicaoRequest->validarRequisicao($requisicaoRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->RequisicaoRepository->atualizarRequisicao($requisicaoRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->RequisicaoRepository->deletarRequisicao($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
