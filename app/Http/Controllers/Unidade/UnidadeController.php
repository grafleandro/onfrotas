<?php

namespace App\Http\Controllers\Unidade;

use App\Http\Requests\UnidadeRequest;
use App\Repository\UnidadeRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UnidadeController extends Controller
{
    private $UnidadeRepository;

    public function __construct(UnidadeRepository $unidadeRepository)
    {
        $this->UnidadeRepository = $unidadeRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        if(isset($request->all()['search_value'])){
            $autocomplete = $this->UnidadeRepository->autocomplete($request->all()['search_value']);

            return response()->json(['success' => 1, 'unidades' => $autocomplete]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UnidadeRequest $unidadeRequest
     * @return \Illuminate\Http\Response
     */
    public function store(UnidadeRequest $unidadeRequest)
    {
        try{

            $errors = $unidadeRequest->validarUnidade($unidadeRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->UnidadeRepository->salvarDados($unidadeRequest->all());

            if($resposta > 0){
                return response()->json(['success' => 1]);
            }else{
                if($resposta == -1){
                    return response()->json(['success' => 0, 'alert' => "Ja existe uma Unidade cadastrada com este CNPJ!!!"]);
                }else{
                    return response()->json(['success' => 0]);
                }
            }
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show($resource)
    {
        switch ($resource){
            case 'cadastrar':
                $tipoLogradouro = $this->UnidadeRepository->getTipoLogradouro();
                return view('unidade/cadastrar', ['tipoLogradouro' => $tipoLogradouro]);
                break;
            case 'listar':
                return view('unidade/listar');
                break;
            case 'tabela':
                return $this->UnidadeRepository->tabela();
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @throws \Throwable
     */
    public function edit($id)
    {
        try{

            $dadosUnidade = $this->UnidadeRepository->findById($id);
            $tipoLogradouro = $this->UnidadeRepository->getTipoLogradouro();

            if(count($dadosUnidade)){
                $resposta = [
                    'success' => 1,
                    'view' => view('unidade/formulario', ['unidade' => $dadosUnidade, 'tipoLogradouro' => $tipoLogradouro])->render(),
                    'tipo_logradouro' => $dadosUnidade['unidade_endereco']['enlo_id'],
                ];

                return response()->json($resposta);
            }

            return response()->json(['success' => 0]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UnidadeRequest $unidadeRequest, $id)
    {
            try{
            $errors = $unidadeRequest->validarUnidade($unidadeRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->UnidadeRepository->atualizarUnidade($unidadeRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $resp = $this->UnidadeRepository->deletarUnidade($id);

            if($resp > 0){
                return response()->json(['success' => 1]);
            }else{
                return response()->json(['success' => 0]);
            }

        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
