<?php

namespace App\Http\Controllers\Veiculos;

use App\Http\Requests\VeiculosRequest;
use App\Model\VeiculoCorModel;
use App\Model\VeiculoTipoModel;
use App\Repository\VeiculosRepository;
use App\Utils\Common;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utils\VeiculoUtils;

class VeiculosController extends Controller
{
    private $veiculoTipoModel;
    private $veiculoCorModel;
    private $veiculosRepository;

    public function __construct(VeiculoTipoModel $veiculoTipoModel, VeiculoCorModel $veiculoCorModel, VeiculosRepository $veiculosRepository)
    {
        $this->veiculoTipoModel = $veiculoTipoModel;
        $this->veiculoCorModel = $veiculoCorModel;
        $this->veiculosRepository = $veiculosRepository;

        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param VeiculosRequest $veiculosRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VeiculosRequest $veiculosRequest)
    {
        try{
            $errors = $veiculosRequest->validarVeiculo($veiculosRequest->all());

            if (sizeof($errors)) {
                Common::setError($errors['alert']);
            }

            $resposta = $this->veiculosRepository->salvarDados($veiculosRequest->all());

            return response()->json($resposta);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param $resource
     * @return array|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function show(Request $request, $resource)
    {
        switch ($resource){
            case 'cadastrar':
                $params = [
                    'veiculo_tipo' => $this->veiculoTipoModel->orderBy('veti_titulo', 'asc')->get()->toArray(),
                    'veiculo_cor' => $this->veiculoCorModel->orderBy('veco_titulo', 'asc')->get()->toArray(),
                    'veiculo_combustivel' => VeiculoUtils::combustivelVeiculo()
                ];

                return view('veiculos/cadastrar', $params);
                break;
            case 'listar':
                return view('veiculos/listar');
                break;
            case 'tabela':
                return $this->veiculosRepository->tabela($request);
                break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function edit($id)
    {
        try{
            $dadosVeiculo = $this->veiculosRepository->findById($id);
            $params = [
                'veiculo_tipo' => $this->veiculoTipoModel->orderBy('veti_titulo', 'asc')->get()->toArray(),
                'veiculo_cor' => $this->veiculoCorModel->orderBy('veco_titulo', 'asc')->get()->toArray(),
                'veiculo_combustivel' => VeiculoUtils::combustivelVeiculo(),
                'veiculo' => $dadosVeiculo
            ];

            if(count($dadosVeiculo)){
                $resposta = [
                    'success' => 1,
                    'view' => view('veiculos/formulario', $params)->render()
                ];

                return response()->json($resposta);
            }

            return response()->json(['success' => 0]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param VeiculosRequest $veiculosRequest
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VeiculosRequest $veiculosRequest, $id)
    {
        try{
            if(!isset($veiculosRequest->all()['odometro'])) {
                $errors = $veiculosRequest->validarVeiculo($veiculosRequest->all());

                if (sizeof($errors)) {
                    Common::setError($errors['alert']);
                }
            }

            $resposta = $this->veiculosRepository->atualizarVeiculo($veiculosRequest->all(), $id);

            if(!$resposta){
                Common::setError('Houve um erro ao atualizar os dados!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try{
            $resposta = $this->veiculosRepository->deletarVeiculo($id);

            if(!$resposta){
                Common::setError('Erro ao tentar excluir o item!');
            }

            return response()->json(['success' => 1]);
        } catch ( \Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()]);
        }
    }
}
