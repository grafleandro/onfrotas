<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class ColaboradorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function colaboradorRules(){
        return [
            'nome' => 'required|regex:/^[\pL\s\-]+$/u',
            'dt_nascimento' => 'required|date_format:d/m/Y',
            'dt_venc_cnh' => 'required|date_format:d/m/Y',
            'cpf' => 'required|string',
            'perfil' => 'required|numeric',
            'habilitacao' => 'max:11',
            'categoria' => 'required|numeric',
            'email' => 'required|email',
            'password' => 'required|string',
        ];
    }

    public function colaboradorUpdateRules(){
        return [
            'nome' => 'required|regex:/^[\pL\s\-]+$/u',
            'dt_nascimento' => 'required|date_format:d/m/Y',
            'dt_venc_cnh' => 'required|date_format:d/m/Y',
            'cpf' => 'required|string',
            'perfil' => 'required|numeric',
            'habilitacao' => 'max:11',
            'categoria' => 'required|numeric',
            'email' => 'required|email',

        ];
    }

    public function validarColaborador($colaborador){
        $validator = Validator::make($colaborador, $this->colaboradorRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    public function validarColaboradorUpdate($colaborador){
        $validator = Validator::make($colaborador, $this->colaboradorUpdateRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
