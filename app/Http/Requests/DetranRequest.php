<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class DetranRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function detranRules(){
        return [
            'placa' => 'required|regex:/\w{3}\d{4}/|size:7',
            'renavam' => 'required|numeric',
            'retorno' => 'required|string',
            'outros' => 'nullable'
        ];
    }

    public function validarDetran($dados){
        $validator = Validator::make($dados, $this->detranRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
