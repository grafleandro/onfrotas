<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class FrotasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function frotaRules(){
        return [
            'unidade' => 'nullable|string',
            'id_unidade' => 'required|required_with:unidade|numeric',
            'responsavel' => 'nullable|string',
            'id_pessoa' => 'required|required_with:responsavel|numeric',
            'cod_frota' => 'nullable|string',
            'id_frota' => 'nullable|numeric',
            'veiculos' => 'required|required_with:id_unidade|array|min:1'
        ];
    }

    public function validarFrota($frota){
        $validator = Validator::make($frota, $this->frotaRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }

    public function messages()
    {
        return [
            'id_unidade.required' => 'Você precisa selecionar uma Unidade!',
            'id_pessoa.required' => 'Você precisa selecionar uma pessoa Responsável!',
            'veiculos.required' => 'Você precisa selecionar pelo menos 1 Veículo!',
        ];
    }
}
