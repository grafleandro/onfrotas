<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class MecanicaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function mecanicaRules(){
        return [
            'nome' => 'required|regex:/^[\pL\s\-]+$/u',
            'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|max:15',
            'responsavel' => 'nullable|regex:/^[\pL\s\-]+$/u',
//            'id_pessoa' => 'nullable|required_with:responsavel|numeric',
        ];
    }

    public function validarMecanica($mecanica){
        $validator = Validator::make($mecanica, $this->mecanicaRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
