<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class OrdemServicoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function ordemServicoRules(){
        return [
            'unidade' => 'required|string',
            'id_unidade' => 'nullable|required_with:unidade|numeric',
            'oficina' => 'nullable|string',
            'id_oficina' => 'nullable|required_with:oficina|numeric',
            'veiculo' => 'nullable|string',
            'id_veiculo' => 'nullable|required_with:veiculo|numeric',
            'tempo_servico' => 'nullable|numeric',
            'tipo' => 'nullable|numeric',
            'desc_serv' => 'nullable|string',
            'desc_pecas' => 'nullable|string',
            'imagem' => 'nullable|image|mimes:jpeg,png,jpg,pdf|max:2048',
        ];
    }

    public function validarOrdemServico($ordemServico){

        $validatorOS = Validator::make($ordemServico, $this->ordemServicoRules(), $this->messages());

        if($validatorOS->errors()->toArray()){
            $error = $validatorOS->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
