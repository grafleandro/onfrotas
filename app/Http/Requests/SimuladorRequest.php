<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class SimuladorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    /**
     * @return array
     */
    public function simuladorRule(){
        return [
            'razao_social' => 'required|regex:/^[\pL\s\-]+$/u',
            'email' => 'required|email',
            'contato-cel' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|size:15',
            'cnpj' => 'required|regex:/^(\d{2}).(\d{3}).(\d{3})\/(\d{4})-(\d{2})$/',
            'qtd_veiculo' => 'required|numeric',
            'tipo' => 'required|string'
        ];
    }

    /**
     * @param array $dados
     * @return array
     */
    public function validarSimulador(array $dados){
        $validator = Validator::make($dados, $this->simuladorRule(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
