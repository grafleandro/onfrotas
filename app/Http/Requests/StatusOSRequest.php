<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class StatusOSRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function statusRules(){
        return [
            'nome' => 'required|regex:/^[\pL\s\-]+$/u',
            'abreviacao' => 'required|regex:/^[\pL\s\-]+$/u',
            'descricao' => 'required|string',
        ];
    }

    public function validarStatus($status){
        $validator = Validator::make($status, $this->statusRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
