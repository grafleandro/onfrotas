<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class UnidadeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function unidadeRules(){
        return [
            'titulo' => 'required|regex:/^[\pL\s\-]+$/u',
            'razao_social' => 'required|regex:/^[\pL\s\-]+$/u',
            'insc_estadual' => 'required|numeric',
            'responsavel' => 'nullable|string',
            'id_pessoa' => 'required|required_with:responsavel|numeric',
            'phone_fixo' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|size:14',
            'telefone_cel' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|size:15',
            'email' => 'required|email',
            'obs' => 'max:200',
            'tipo_logradouro' => 'required|numeric',
            'logradouro' => 'required|string',
            'numero' => 'required|numeric',
            'bairro' => 'required|string',
            'cep' => 'required|numeric',
            'cidade' => 'required|string',
            'estado' => 'required|string',
            'complemento' => 'string',
        ];
    }

    public function validarUnidade($unidade){
        $validator = Validator::make($unidade, $this->unidadeRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
