<?php

namespace App\Http\Requests;

use App\Utils\VeiculoUtils;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class VeiculosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return[
            //
        ];
    }

    public function veiculoRules(){
        return [
            'tipo_veiculo' => 'required|numeric',
            'combustivel' => 'required|numeric',
            'cor' => 'required|numeric',
            'uf_emplacamento' => 'required|string|size:2',
            'modelo' => 'required|string',
            'placa' => 'required|string',
            'renavam' => 'required|numeric',
            'ano' => 'required|numeric|between:1950,' . (Carbon::now()->year+1),
            'obs' => 'max:200',
        ];
    }

    public function validarVeiculo($veiculo){
        $validator = Validator::make($veiculo, $this->veiculoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
