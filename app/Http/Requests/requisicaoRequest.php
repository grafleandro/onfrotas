<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Validator;

class requisicaoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function requisicaoRules(){
        return [
            'tipo_requisicao' => 'required|numeric|between:1,2',
            'id_unidade' => 'required|numeric|min:1',
            'unidade' => 'required|string',
            'veiculo' => 'required|string',
            'id_veiculo' => 'required|numeric|min:1',
            'descricao' => 'required|string',
            'valor' => 'nullable',
            'quantidade' => 'required|numeric|min:1',
        ];
    }

    public function validarRequisicao($colaborador){
        $validator = Validator::make($colaborador, $this->requisicaoRules(), $this->messages());

        if($validator->errors()->toArray()){
            $error = $validator->errors()->all()[0];

            return ['alert' => $error];
        }

        return [];
    }
}
