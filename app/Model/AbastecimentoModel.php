<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AbastecimentoModel extends Model
{
    protected $table = 'abastecimento';

    protected $primaryKey = 'abas_id';

    protected $fillable = [
        'abas_posto',
        'abas_km',
        'abas_tipo',
        'abas_quantidade',
        'abas_valor',
        'abas_observacao',
        'veic_id',
        'created_at',
        'updated_at',
    ];
}
