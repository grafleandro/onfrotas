<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ConfiguracaoClienteModel extends Model
{
    protected $table = 'configuracao_cliente';

    protected $primaryKey = 'cocl_id';

    protected $fillable = [
        'cocl_id',
        'cocl_num_ordem_servico',
        'cocl_num_requisicao',
        'cocl_num_requisicao',
        'created_at',
        'updated_at',
    ];
}
