<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContatoModel extends Model
{
    protected $table = 'contato';

    protected $primaryKey = 'cont_id';

    protected $fillable = [
        'cont_cel_1',
        'cont_cel_2',
        'cont_tel_fixo',
        'cont_email' ,
        'created_at',
        'updated_at',
    ];
}
