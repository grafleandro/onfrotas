<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmpresaModel extends Model
{
    protected $table = 'cliente_empresa';

    protected $primaryKey = 'clem_id';

    protected $fillable = [
        'clem_razao_social',
        'clem_nome_fantasia',
        'clem_cnpj',
        'clem_insc_estadual',
        'clem_observacao',
        'ende_id',
        'cont_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresaEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresaContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }
}
