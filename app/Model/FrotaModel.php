<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FrotaModel extends Model
{
    protected $table = 'frota';

    protected $primaryKey = 'frot_id';

    protected $fillable = [
        'frot_cod_interno',
        'unid_id',
        'pess_id',
        'clem_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function frotaUnidade(){
        return $this->hasOne(UnidadeModel::class, 'unid_id', 'unid_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function frotaVeiculo(){
        return $this->hasManyThrough(VeiculosModel::class, FrotaVeiculoModel::class, 'frot_id', 'veic_id', 'frot_id', 'veic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function frotaPessoa(){
        return $this->hasOne(PessoaModel::class, 'pess_id', 'pess_id');
    }
}
