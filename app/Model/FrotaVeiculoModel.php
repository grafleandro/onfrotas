<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FrotaVeiculoModel extends Model
{
    protected $table = 'frota_veiculo';

    protected $primaryKey = 'frve_id';

    public $timestamps = false;

    protected $fillable = [
        'frot_id',
        'veic_id',
        'clem_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function veiculo(){
        return $this->hasOne(VeiculosModel::class, 'veic_id', 'veic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function frotaVeiculoFrota(){
        return $this->hasOne(FrotaModel::class, 'frot_id', 'frot_id');
    }
}
