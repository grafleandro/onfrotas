<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ManutencaoPrioridadeModel extends Model
{
    protected $table = 'manutencao_prioridade';

    protected $primaryKey = 'mapr_id';
}
