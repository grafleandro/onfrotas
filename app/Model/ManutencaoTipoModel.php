<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ManutencaoTipoModel extends Model
{
    protected $table = 'manutencao_tipo';

    protected $primaryKey = 'mati_id';
}
