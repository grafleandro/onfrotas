<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MecanicasModel extends Model
{
    protected $table = 'oficina';

    protected $primaryKey = 'ofic_id';

    protected $fillable = [
        'ofic_titulo',
        'ofic_responsavel',
        'ofic_especializacao',
        'ofic_interna_externa',
        'pess_id',
        'cont_id',
        'clem_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mecanicaContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function mecanicaResponsavel(){
        return $this->hasOne(PessoaModel::class, 'pess_id', 'pess_id');
    }
}
