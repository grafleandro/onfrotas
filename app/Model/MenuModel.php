<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 11/07/18
 * Time: 15:30
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class MenuModel extends Model
{
    protected $table = 'menu';

    protected $primaryKey = 'menu_id';
}