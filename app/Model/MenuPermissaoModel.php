<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MenuPermissaoModel extends Model
{
    protected $table = 'menu_permissao';

    protected $primaryKey = 'mepe_id';

    protected $fillable = [
        'us_id',
        'menu_id'
    ];
}
