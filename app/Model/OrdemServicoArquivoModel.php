<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdemServicoArquivoModel extends Model
{
    protected $table = 'ordem_servico_arquivo';

    protected $primaryKey = 'orsa_id';

    protected $fillable = [
        'orsa_titulo',
        'orsa_url',
        'orse_id',
    ];
}
