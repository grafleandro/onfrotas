<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdemServicoModel extends Model
{
    protected $table = 'ordem_servico';

    protected $primaryKey = 'orse_id';

    protected $fillable = [
        'orse_desc_servico',
        'orse_desc_pecas',
        'orse_tempo_estimado',
        'orse_executar',
        'orse_valor',
        'unid_id',
        'veic_id',
        'mati_id',
        'mapr_id',
        'ofic_id',
        'clem_id',
        'sose_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osVeiculo(){
        return $this->hasOne(VeiculosModel::class, 'veic_id', 'veic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osUnidade(){
        return $this->hasOne(UnidadeModel::class, 'unid_id', 'unid_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osManutencaoTipo(){
        return $this->hasOne(ManutencaoTipoModel::class, 'mati_id', 'mati_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osManutencaoPrioridade(){
        return $this->hasOne(ManutencaoPrioridadeModel::class, 'mapr_id', 'mapr_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osOficina(){
        return $this->hasOne( MecanicasModel::class, 'ofic_id', 'ofic_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function osArquivo(){
        return $this->hasMany( OrdemServicoArquivoModel::class, 'orse_id', 'orse_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osStatus(){
        return $this->hasOne(StatusOrdemServicoModel::class, 'sose_id', 'sose_id');
    }
}
