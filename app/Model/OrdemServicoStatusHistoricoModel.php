<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrdemServicoStatusHistoricoModel extends Model
{
    protected $table = 'ordem_servico_status_historico';

    protected $primaryKey = 'ossh_id';

    protected $fillable = [
        'ossh_anterior',
        'ossh_atual',
        'ossh_motivo',
        'orse_id',
        'pess_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function osStatus(){
        return $this->hasOne(StatusOrdemServicoModel::class, 'sose_id', 'ossh_atual');
    }
}
