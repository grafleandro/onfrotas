<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PessoaModel extends Model
{
    protected $table = 'pessoa';

    protected $primaryKey = 'pess_id';

    protected $fillable = [
        'pess_nome',
        'pess_data_nasc',
        'pess_habilitacao',
        'pess_categoria_cnh',
        'pess_vencimento_cnh',
        'pess_cpf',
        'pess_rg',
        'ende_id',
        'clem_id',
        'cont_id',
        'created_at',
        'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pessoaContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }

}
