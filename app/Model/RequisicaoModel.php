<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RequisicaoModel extends Model
{
    protected $table = 'requisicao';

    protected $primaryKey = 'requ_id';

    protected $fillable = [
        'requ_id',
        'requ_tipo_requisicao',
        'requ_quantidade',
        'requ_descricao',
        'requ_valor',
        'clem_id',
        'unid_id',
        'veic_id',
        'created_at',
        'updated_at',
    ];

    public function requisicaoVeiculo(){
        return $this->hasOne(VeiculosModel::class, 'veic_id', 'veic_id');
    }

    public function requisicaoUnidade(){
        return $this->hasOne(UnidadeModel::class, 'unid_id', 'unid_id');
    }

    public function requisicaoConfiguracaoCliente(){
        return $this->hasOne(ConfiguracaoClienteModel::class, 'clem_id', 'clem_id');
    }
}
