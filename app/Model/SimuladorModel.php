<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SimuladorModel extends Model
{
    protected $table = 'cliente_acesso';

    protected $primaryKey = 'clac_id';

    protected $fillable = [
        'clac_razao_social',
        'clac_email',
        'clac_contato',
        'clac_cnpj',
        'clac_qtd_veiculo',
        'clac_tipo',
    ];
}
