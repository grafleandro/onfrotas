<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class StatusOrdemServicoModel extends Model
{
    protected $table = 'ordem_servico_status';

    protected $primaryKey = 'sose_id';

    protected $fillable = [
        'sose_id',
        'sose_titulo',
        'sose_abreviacao',
        'sose_descricao',
        'created_at',
        'updated_at',
    ];
}
