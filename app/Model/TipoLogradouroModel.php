<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TipoLogradouroModel extends Model
{
    protected $table = 'endereco_logradouro';

    protected $primaryKey = 'enlo_id';
}
