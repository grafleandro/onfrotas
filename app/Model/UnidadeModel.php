<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UnidadeModel extends Model
{
    protected $table = 'unidade';

    protected $primaryKey = 'unid_id';

    protected $fillable = [
        'unid_titulo',
        'unid_razao_social',
        'unid_cnpj',
        'unid_insc_estadual',
        'unid_observacoes',
        'ende_id',
        'cont_id',
        'clem_id',
        'pess_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unidadeContato(){
        return $this->hasOne(ContatoModel::class, 'cont_id', 'cont_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unidadeEndereco(){
        return $this->hasOne(EnderecoModel::class, 'ende_id', 'ende_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unidadeEmpresa(){
        return $this->hasOne(EmpresaModel::class, 'clem_id', 'clem_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unidadeResponsavel(){
        return $this->hasOne(PessoaModel::class, 'pess_id', 'pess_id');
    }
}
