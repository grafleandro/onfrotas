<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VeiculoCorModel extends Model
{
    protected $table = 'veiculo_cor';

    protected $primaryKey = 'veco_id';

    protected $fillable = [
        'veco_titulo',
        'veco_desc'
    ];
}
