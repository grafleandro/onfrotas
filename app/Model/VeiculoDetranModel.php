<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VeiculoDetranModel extends Model
{
    protected $table = 'veiculo_detran';

    protected $primaryKey = 'vede_id';

    protected $fillable = [
        'vede_licenciado_ate_mes',
        'vede_licenciado_ate_ano',
        'vede_licenciamento_valor',
        'veic_id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function veiculo(){
        return $this->hasOne(VeiculosModel::class, 'veic_id', 'veic_id');
    }
}
