<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VeiculoTipoModel extends Model
{
    protected $table = 'veiculo_tipo';

    protected $primaryKey = 'veti_id';

    protected $fillable = [
        'veti_titulo',
        'veti_desc'
    ];
}
