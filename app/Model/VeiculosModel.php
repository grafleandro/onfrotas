<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VeiculosModel extends Model
{
    protected $table = 'veiculo';

    protected $primaryKey = 'veic_id';

    protected $fillable = [
        'veic_modelo',
        'veic_placa',
        'veic_renavam',
        'veic_combustivel',
        'veic_ano',
        'veic_km',
        'veic_uf_emplacamento',
        'veti_id',
        'veco_id',
        'clem_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function veiculoTipo(){
        return $this->hasOne(VeiculoTipoModel::class, 'veti_id', 'veti_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function veiculoCor(){
        return $this->hasOne(VeiculoCorModel::class, 'veco_id', 'veco_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function veiculoFrota(){
        return $this->hasOne(FrotaVeiculoModel::class, 'veic_id', 'veic_id');
    }
}
