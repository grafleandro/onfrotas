<?php

namespace App\Policies;

use App\User;
use App\Model\OrdemServicoModel;
use App\Utils\UsuarioUtils;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrdemServicoPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the ordem servico model.
     *
     * @param  \App\User  $user
     * @param  \App\Model\OrdemServicoModel  $ordemServicoModel
     * @return mixed
     */
    public function view(User $user, OrdemServicoModel $ordemServicoModel)
    {
        //
    }

    /**
     * @param User $user
     * @return bool
     */
    public function menuTabelaEditar(User $user){
        return $user->perfil == UsuarioUtils::_US_GESTOR || $user->perfil == UsuarioUtils::_US_MASTER;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function menuTabelaExcluir(User $user){
        return $user->perfil == UsuarioUtils::_US_GESTOR || $user->perfil == UsuarioUtils::_US_MASTER;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function tabelaAlterarStatus(User $user){
        return $user->perfil == UsuarioUtils::_US_GESTOR || $user->perfil == UsuarioUtils::_US_MASTER;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function visualizarDadosGestor(User $user){
        return $user->perfil == UsuarioUtils::_US_GESTOR || $user->perfil == UsuarioUtils::_US_MASTER;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function menuTabelaImprimir(User $user){
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function menuTabelaVisulizar(User $user){
        return true;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function menuTabelaInformacao(User $user){
        return true;
    }

    /**
     * Determine whether the user can create ordem servico models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the ordem servico model.
     *
     * @param  \App\User  $user
     * @param  \App\Model\OrdemServicoModel  $ordemServicoModel
     * @return mixed
     */
    public function update(User $user, OrdemServicoModel $ordemServicoModel)
    {
        //
    }

    /**
     * Determine whether the user can delete the ordem servico model.
     *
     * @param  \App\User  $user
     * @param  \App\Model\OrdemServicoModel  $ordemServicoModel
     * @return mixed
     */
    public function delete(User $user, OrdemServicoModel $ordemServicoModel)
    {
        //
    }

    /**
     * Determine whether the user can restore the ordem servico model.
     *
     * @param  \App\User  $user
     * @param  \App\Model\OrdemServicoModel  $ordemServicoModel
     * @return mixed
     */
    public function restore(User $user, OrdemServicoModel $ordemServicoModel)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the ordem servico model.
     *
     * @param  \App\User  $user
     * @param  \App\Model\OrdemServicoModel  $ordemServicoModel
     * @return mixed
     */
    public function forceDelete(User $user, OrdemServicoModel $ordemServicoModel)
    {
        //
    }
}
