<?php

namespace App\Providers;

use App\Model\MenuModel;
use App\Utils\Common;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @param Dispatcher $events
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        /* Evento para Criacao do MenuController em tempo de Execucao */
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $event->menu->add('MENU DE NAVEGAÇÃO');

            if(!Session::has('menu')){
                $items = $this->selecionarMenus();

                $event->menu->add(...$items);

                Session::put('menu', $items);
            }else{
                $items = Session::get('menu');
            }

            $event->menu->add(...$items);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * @return array
     */
    private function selecionarMenus(){
        $idUser = Auth::user()->getAuthIdentifier();

        $menus = (new MenuModel())
            ->join('menu_permissao as mepe', 'mepe.menu_id', '=', 'menu.menu_id')
            ->where('mepe.us_id', '=', $idUser)
            ->orderBy('menu.menu_sequencia', 'desc')
            ->get()->toArray();

        $menus = $this->montarEstruturaMenu($menus, true);

        return $this->agruparMenus($menus);
    }

    /**
     * @param array $menus
     * @param bool $mapear
     * @return array
     */
    private function montarEstruturaMenu($menus = [], $mapear = true){
        if($mapear){
            $menus = $this->mapearArrayMenu($menus);
        }

        foreach ($menus as $index => $menu){
            /* Seleciona os ancestrais de um determinado NODE */
//            if(empty($menu['menu_pai'])){
//                /* Seleciona um filho de um determinado NODE */
//                $menuSelecionar = MenuModel::where('menu_sequencia', 'like', $menu['menu_sequencia'] . '.%')
//                    ->orderBy('menu.menu_sequencia', 'desc')
//                    ->get()->toArray();
//
//                foreach ($menuSelecionar as $indexSubmenu => $submenu){
//                    $menus[$submenu['menu_id']] = $submenu;
//                }
//            } else if(!isset($menus[ $menu['menu_pai'] ])){
//                $menuSelecionar = MenuModel::where('menu_id', '=', $menu['menu_pai'])->get()->toArray()[0];
//
//                $menus[$menuSelecionar['menu_id']] = $menuSelecionar;
//
//                return $this->montarEstruturaMenu($menus, false);
//            }

            if(!empty($menu['menu_pai']) && !array_key_exists($menu['menu_pai'], $menus)){
                $menuSelecionar = (new MenuModel())->where('menu_id', '=', $menu['menu_pai'])->get()->toArray()[0];

                $menus[$menuSelecionar['menu_id']] = $menuSelecionar;

                return $this->montarEstruturaMenu($menus, false);
            }else if(is_null($menu['menu_pai'])){
                /* Seleciona um filho de um determinado NODE */
                $menuSelecionar = (new MenuModel())
                    ->where('menu_sequencia', 'like', $menu['menu_sequencia'] . '.%')
                    ->orderBy('menu.menu_sequencia', 'desc')
                    ->get()->toArray();

                foreach ($menuSelecionar as $indexSubmenu => $submenu){
                    $menus[$submenu['menu_id']] = $submenu;
                }
            }
        }

        return $menus;
    }

    /**
     * @param array $menus
     * @return array
     */
    private function mapearArrayMenu($menus = []){
        $arrayAux = [];

        foreach ($menus as $index => $menu){
            $arrayAux[$menu['menu_id']] = $menu;
        }

        return $arrayAux;
    }

    /**
     * @param array $menus
     * @return array
     */
    private function agruparMenus($menus = []){
        /* Ordena de forma decrescente */
        uasort($menus, function($a, $b){
            if($a['menu_id'] == $b['menu_id']){
                return 0;
            }

            return $a['menu_id'] > $b['menu_id'] ? -1 : 1;
        });

        $menuRetorno = [];

        foreach ($menus as $index => $item){
            /* Monta um objeto do Item que serah adicionado */
            $objeto = [
                'text' => $item['menu_titulo'],
                'icon' => $item['menu_classe'],
                'url' => $item['menu_href']
            ];

            /* Verifica se eh um menu PAI ou menu FILHO */
            if(!is_null($item['menu_pai'])){

                /* Verifica se existe a estrutura para os submenus. Caso nao exista, serah criada */
                if(!isset($menuRetorno[$item['menu_pai']]['submenu'])){
                    /* Objeto do item PAI */
                    $objPai = $menus[$item['menu_pai']];

                    /* Verifica se o item ja possui filhos no vetor, caso sim, eh carregado seu filhos, caso contrario atribui um novo filho */
                    $objFilho = ($menuRetorno[$index]) ?? $objeto;

                    /* Prepara a estrutura do PAI */
                    $menuRetorno[$item['menu_pai']] = [
                        'text' => $objPai['menu_titulo'],
                        'icon' => $objPai['menu_classe'],
                        'url' => $objPai['menu_href'],
                        'submenu' => [ $item['menu_id'] => $objFilho ]
                    ];
                } else {
                    /* Caso ja exista a estrutura de subitens, apenas carrega o novo FILHO */
                    $menuRetorno[$item['menu_pai']]['submenu'][$item['menu_id']] = (isset($menuRetorno[ $index ])) ? $menuRetorno[ $index ] : $objeto;
                }

                /* Ordena os Filhos de acordo com o ID */
                ksort($menuRetorno[$item['menu_pai']]['submenu']);

                /* Caso seja necessario, apos inserir o filhos soltos existentes, eh removido da pilha deixando apenas a estrutura PAI */
                unset($menuRetorno[ $index ]);
            }else if(!isset($menuRetorno[$index])){
                /* Caso seja um pai sem filhos */
                $menuRetorno[$item['menu_id']] = $objeto;
            }
        }

        /* Retorna o vetor em sua forma original, do MENOR para o MAIOR */
        return array_reverse($menuRetorno, true);
    }
}
