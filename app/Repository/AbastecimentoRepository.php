<?php


namespace App\Repository;


use App\Model\AbastecimentoModel;
use App\Utils\Common;
use App\Utils\MoneyUtils;
use Illuminate\Http\Request;

class AbastecimentoRepository
{
    public function salvarDados(Request $request)
    {
        return $this->salvar(new AbastecimentoModel(), $request);
    }

//    public function atualizarDados(array $dadosBimestre, int $idBimestre) {
//        $bimestre = $this->findById($idBimestre);
//        return $this->salvar($bimestre, $dadosBimestre);
//    }

    public function salvar(AbastecimentoModel $abastecimentoModel, Request $request)
    {

        $abastecimentoModel->abas_posto = $request->posto;
        $abastecimentoModel->abas_km = $request->km;
        $abastecimentoModel->abas_tipo = $request->tipo_combustivel;
        $abastecimentoModel->abas_quantidade = $request->quantidade;
        $abastecimentoModel->abas_valor = MoneyUtils::removeCaracters($request->valor_abastecimento);
        $abastecimentoModel->abas_observacao = $request->observacao;
        $abastecimentoModel->veic_id = $request->id_veiculo;

        if (!$abastecimentoModel->save()) {
            Common::setError('Erro ao salvar os dados do Abastecimento!');
        }

        return ['success' => 1];
    }

//    /**
//     * @param $idBimestre
//     * @return BimestreModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
//     */
//    public function findById($idBimestre){
//        return BimestreModel::where('bime_id', $idBimestre)->first();
//    }
//
//    /**
//     * @return \Illuminate\Http\JsonResponse
//     * @throws \Exception
//     */
//    public function tabela(){
//        $queryBime = BimestreModel::query();
//
//        return Datatables::eloquent($queryBime)
//            ->editColumn('bime_nivel', function($bimestre){
//                return $bimestre->bime_apelido;
//            })
//            ->addColumn('action', function ($bimestre){
//                $href = url('nivel') . "/{$bimestre->bime_id}/edit";
//
//                $editar = '<a href="'.$href.'"
//                              title="Editar Nível"
//                              data-bimestre="'.$bimestre->bime_id.'"
//                              class="btn btn-default btn-circle margin-right-5">
//                                  <i class="fa fa-edit"></i>
//                            </a>';
//
//                $excluir = '<button title="Excluir Nível"
//                                    data-bimestre="'.$bimestre->bime_id.'"
//                                    class="btn btn-default btn-circle"
//                                    onclick="jQueryBimestre.deletarBimestre($(this))">
//                                        <i class="fa fa-trash-o"></i>
//                             </button>';
//
//                return "<div class='text-center'>{$editar}{$excluir}</div>";
//            })
//            ->make(true);
//    }
//
//    /**
//     * @param $id
//     * @return bool|mixed|null
//     * @throws \Exception
//     */
//    public function deletarDados($id) {
//        $bimestre = $this->findById($id);
//        return $bimestre->delete();
//    }
}