<?php


namespace App\Repository;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class AparelhoRepository
{
    public function registrarArquivo(Request $request, $origem)
    {
        $pathFile = "aparelho\\$origem.json";

        if(File::exists(storage_path("app\{$pathFile}"))){
            $jsonString = file_get_contents(storage_path("app\{$pathFile}"));
            $data = json_decode($jsonString, true);

            array_push($data, $request->toArray());

            Storage::put($pathFile, json_encode($data));
        } else {
            Storage::put($pathFile, json_encode([$request->toArray()]));
        }
    }
}