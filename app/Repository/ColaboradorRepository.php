<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 31/07/18
 * Time: 11:15
 */

namespace App\Repository;


use App\Mail\EnviarNotificacaoMail;
use App\Model\ContatoModel;
use App\Model\MenuModel;
use App\Model\MenuPermissaoModel;
use App\Model\PessoaModel;
use App\User;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\MenuUtils;
use App\Utils\UsuarioUtils;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class ColaboradorRepository
{
    /**
     * @param $dadosUsuario
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function salvarDados($dadosUsuario){

        $resp = User::where('email', $dadosUsuario['email'])->get()->toArray();

        if(!isset($resp[0])) {

            $endereco = null;

            $contato = ContatoModel::create(array(
                'cont_email' => (isset($dadosUsuario['email'])) ? $dadosUsuario['email'] : null,
            ));

            $pessoa = PessoaModel::create(array(
                'pess_nome' => ($dadosUsuario['nome']) ?? null,
                'pess_data_nasc' => (isset($dadosUsuario['dt_nascimento'])) ? Carbon::createFromFormat('d/m/Y', $dadosUsuario['dt_nascimento'])->format('Y-m-d H:i:s') : null,
                'pess_habilitacao'  => ($dadosUsuario['habilitacao']) ?? null,
                'pess_categoria_cnh' => ($dadosUsuario['categoria']) ?? null,
                'pess_vencimento_cnh' => (isset($dadosUsuario['dt_venc_cnh'])) ? Carbon::createFromFormat('d/m/Y', $dadosUsuario['dt_venc_cnh'])->format('Y-m-d H:i:s') : null,
                'pess_cpf'  => ($dadosUsuario['cpf']) ? Mask::removerMascara($dadosUsuario['cpf']) : null,
                'ende_id' => $endereco,
                'cont_id' => $contato->cont_id,
                'clem_id' => ($dadosUsuario['clem_id']) ?? Session::get('clem_id')
            ));

            /* Verifica se foi informado uma senha, caso nao tenha, eh gerado uma nova */
            $password = (isset($dadosUsuario['password']) && !empty($dadosUsuario['password'])) ? $dadosUsuario['password'] : UsuarioUtils::generatePassword();

            $usuario = User::create(array(
                'name' => ($dadosUsuario['nome']) ?? null,
                'email' => ($dadosUsuario['email']) ?? null,
                'password' => Hash::make($password),
                'perfil' => ($dadosUsuario['perfil']) ?? null,
                'pess_id' => $pessoa->pess_id,
            ));

            /* Salvando as permissoes */
            if(isset($dadosUsuario['permissao']) && count($dadosUsuario['permissao']) > 0){
                foreach ($dadosUsuario['permissao'] as $index => $value){
                    $respostaMenu = MenuPermissaoModel::create([
                        'us_id' => $usuario->id,
                        'menu_id' => $value
                    ]);

                    if(!$respostaMenu){
                        Common::setError('Houve um erro ao salvar os menus!');
                    }
                }
            }

            (new EnviarNotificacaoMail())->enviarNotificacao(
                'onFrotas',
                'Cadastro de Usuário',
                $dadosUsuario['email'],
                'email.novo_usuario',
                ['nome' => explode(' ', $dadosUsuario['nome'])[0], 'email' => $dadosUsuario['email'], 'senha' => $password]
            );

            return $usuario->id;
        }else{
            Common::setError("Ja existe Colaborador Cadastrado com este Email!!");

        }
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){


        $empresa = Session::get('clem_id');

        $dadosUsuario = User::with('pessoa')->get()->toArray();

        $tableUsuario = [];

        foreach ($dadosUsuario as $user){
            if($user['pessoa']['clem_id'] == $empresa) {
                $user['pessoa']['pess_cpf'] = Mask::cpf($user['pessoa']['pess_cpf']);
                $tableUsuario[] = $user;
            }
        }

        return Datatables::of($tableUsuario)
            ->addColumn('action', function ($usuario){
                return '<div style="text-align: center">
                                <button title="Editar Usuário" data-usuario="'.$usuario['id'].'" style="margin: 4px" class="btn btn-primary" onclick="jQueryColaborador.editarUsuario($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir" data-usuario="'.$usuario['id'].'" style="margin: 4px" class="btn btn-danger" onclick="jQueryColaborador.deletarUsuario($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->editColumn('cpf', '{{ $pessoa["pess_cpf"] }}')
            ->make(true);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletarUsuario($id){
        try{
            $usuario = User::with('pessoa')->get()->find($id);

            $resp = $usuario->pessoa->delete();

            return $resp;

        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * @param $dadosUsuario
     * @param $id
     * @return mixed
     */
    public function atualizarUsuario($dadosUsuario, $id){

        $usuario = User::find($id);

        $pessoa = PessoaModel::find($usuario->pess_id)->firstOrFail();

        $contato = ContatoModel::find($pessoa->cont_id)->firstOrFail();

        $usuario->name = ($dadosUsuario['nome']) ? $dadosUsuario['nome'] : null;
        $usuario->email = ($dadosUsuario['email']) ? $dadosUsuario['email'] : null;
        $usuario->perfil = ($dadosUsuario['perfil']) ? $dadosUsuario['perfil'] : null;
        if(!empty($dadosUsuario['password'])) {
            $usuario->password = ($dadosUsuario['password']) ? Hash::make($dadosUsuario['password']) : null;
        }

        $pessoa->pess_nome = ($dadosUsuario['nome']) ? $dadosUsuario['nome'] : null;
        $pessoa->pess_data_nasc = (isset($dadosUsuario['dt_nascimento'])) ? Carbon::createFromFormat('d/m/Y', $dadosUsuario['dt_nascimento'])->format('Y-m-d H:i:s') : null;
        $pessoa->pess_habilitacao  = ($dadosUsuario['habilitacao']) ? $dadosUsuario['habilitacao'] : null;
        $pessoa->pess_categoria_cnh = ($dadosUsuario['categoria']) ? $dadosUsuario['categoria'] : null;
        $pessoa->pess_vencimento_cnh = (isset($dadosUsuario['dt_venc_cnh'])) ? Carbon::createFromFormat('d/m/Y', $dadosUsuario['dt_venc_cnh'])->format('Y-m-d H:i:s') : null;
        $pessoa->pess_cpf  = ($dadosUsuario['cpf']) ? str_replace(array('-', ' ', '(', ')', '.'), "", $dadosUsuario['cpf']) : null;

        $contato->cont_email = (isset($dadosUsuario['email'])) ? $dadosUsuario['email'] : null;

        $pessoa->save();

        $contato->save();

        return $usuario->save();
    }


    /**
     * @param $id
     * @return array
     */
    public function findById($id){
        return User::with('pessoa')->get()->find($id)->toArray();
    }

    /**
     * @param $id
     * @return array
     */
    public function findEditUsuario($id){
        return User::with(['pessoa'])->get()->find($id)->toArray();
    }

    /**
     * @param $valor
     * @return mixed
     */
    public function autocomplete($valor){
        return PessoaModel::where('clem_id', Session::get('clem_id'))->where('pess_nome', 'like', $valor . '%')
                ->get()->toArray();
    }

    /**
     * @return array|string
     */
    public function selecionarMenus(){
        $menus = MenuModel::orderBy('menu.menu_sequencia', 'desc')
            ->get()->toArray();

        $menus = (new MenuUtils)->montarEstruturaMenu($menus, true);

        $menus = (new MenuUtils())->montarArvoreMenu($menus);

        $menus = (new MenuUtils())->montarEstruturaMenuHtml($menus) . '</ul>';

        return $menus;
    }


}