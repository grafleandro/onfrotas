<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 18/08/18
 * Time: 09:37
 */

namespace App\Repository;


use App\Model\OrdemServicoModel;
use App\Model\PessoaModel;
use App\Model\VeiculoDetranModel;
use App\Utils\CalendarioUtils;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\MoneyUtils;
use App\Utils\OrdemServicoUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class DashboardRepository
{
    /** Retornar todos os aniversariantes do Dia
     * @return array
     */
    public function aniversariantes(){
        $diaAtual = (strlen(Carbon::now()->day) == 1) ? '0' . Carbon::now()->day : Carbon::now()->day;
        $mesAtual = (strlen(Carbon::now()->month) == 1) ? '0' . Carbon::now()->month : Carbon::now()->month;

        $aniversariante = PessoaModel::with(['pessoaContato'])
            ->where('pess_data_nasc', 'like', '%' . $mesAtual . '-' . $diaAtual)
            ->where('clem_id', Session::get('clem_id'))
            ->get()
            ->toArray();

        return $aniversariante;
    }

    public function graficoOS(){
            $dadosOrdemServico = OrdemServicoModel::select(DB::raw('count(orse_id) as qtd'), 'sose_id', DB::raw('date(created_at) as date'))
            ->where('clem_id', 1)
            ->whereIn('sose_id', OrdemServicoUtils::ordemServicoExecucao())
            ->groupBy(DB::raw('date(created_at)'), 'sose_id')
            ->get()
            ->toArray();

            $dadosRetorno = [];

            /* Prepara o vetor com o retorno das informacoes */
            foreach ($dadosOrdemServico as $index => $value){
                $dadosRetorno[] = [
                    'date' => Carbon::createFromFormat('Y-m-d', $value['date'])->format('d/m/Y'),
                    'amount' => $value['qtd']
                ];
            }

        return $dadosRetorno;
    }

    public function veiculo_doc_vencido(){
        $data = Carbon::now()->addMonth();

        $mes = (strlen($data->month) == 1) ? '0' . $data->month : $data->month;
        $ano = $data->year;

        $dadosDocVencidos= VeiculoDetranModel::with('veiculo')->where('vede_licenciado_ate_mes', $mes)
            ->where('vede_licenciado_ate_ano', $ano)
            ->where('vede_status', 0)
            ->get()->toArray();

        $tableVeiculos = [];

        foreach ($dadosDocVencidos as $veiculo){
            $veiculo['vede_licenciamento_valor'] = 'R$ '.$veiculo['vede_licenciamento_valor'];
            $veiculo['vede_vencimento'] = CalendarioUtils::mesTitulo()[$veiculo['vede_licenciado_ate_mes']]. '/' . $veiculo['vede_licenciado_ate_ano'] ;

            $tableVeiculos[] = $veiculo;
        }

        return Datatables::of($tableVeiculos)
            ->addColumn('action', function ($veiculo){
                return '<div style="text-align: center">
                               <button title="Pago" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryDocVeiculoVencido.pago($(this))"><i class="fa fa-check-square-o"></i></button> 
                        </div>';
            })
            ->editColumn('veic_modelo', '{{ $veiculo["veic_modelo"] }}')
            ->editColumn('veic_placa', '{{ $veiculo["veic_placa"] }}')
            ->make(true);

    }

    public function docPago($id){

        $veiculo = VeiculoDetranModel::where('veic_id', $id)->firstOrFail();

        if(isset($veiculo->vede_id)){
            $veiculo->vede_status = 1;

            $veiculo->save();

            return $veiculo->vede_status;
        }
        return 0;
    }

    public function cnh_vencida(){

        $data =Carbon::now()->format('Y-m-d');

        $dadosCnhVencidas = PessoaModel::with('pessoaContato')->where('clem_id', Session::get('clem_id'))
                            ->where('pess_vencimento_cnh', $data)
                            ->get()->toArray();

        $tableCnhVencidas = [];

        foreach ($dadosCnhVencidas as $pessoa){
            $pessoa['pess_vencimento_cnh'] = Carbon::createFromFormat('Y-m-d', $pessoa['pess_vencimento_cnh'])->format('d/m/Y');
            $pessoa['pessoa_contato']['cont_cel_1'] = Mask::telCelular($pessoa['pessoa_contato']['cont_cel_1']);

            $tableCnhVencidas[] = $pessoa;
        }

        return Datatables::of($tableCnhVencidas)
            ->addColumn('action', function ($pessoa){
                return '<div style="text-align: center">
                               <button title="Pago" data-colaborador="' . $pessoa['pess_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryCnhVencida.pago($(this))" disabled><i class="fa fa-check-square-o"></i></button> 
                        </div>';
            })
            ->editColumn('cont_id', '{{ $pessoa_contato["cont_cel_1"] }}')
            ->make(true);
    }
}