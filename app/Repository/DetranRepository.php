<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 16/08/18
 * Time: 10:00
 */

namespace App\Repository;


use App\Utils\Common;
use App\Utils\DetranUtils;
use DOMDocument;

class DetranRepository
{
    /**
     * @param string $placas
     * @param $renavam
     * @param array $opcoes
     * @return array
     * @throws \Exception
     */
    public function extrairInformacoes($placas = '', $renavam = '0', $opcoes = []){
        $dadosVeiculo = $this->informacoesDoVeiculo($placas, $renavam);
        $retornoDados = [];

        if(count($opcoes)){
            foreach ($opcoes as $value){
                switch ($value){
                    case DetranUtils::_LICENCIADO_ATE:
                        $resposta = $this->licenciadoAte($dadosVeiculo['success']);

                        array_merge($retornoDados, $resposta);
                        break;
                    case DetranUtils::_VALOR_DOCUMENTO:
                        $resposta = $this->valorDocumento($dadosVeiculo['success']);

                        array_merge($retornoDados, $resposta);
                        break;
                    case DetranUtils::_TODOS:
                        $resposta = $this->licenciadoAte($dadosVeiculo['success']);

                        $retornoDados = array_merge($retornoDados, $resposta);

                        $resposta = $this->valorDocumento($dadosVeiculo['success']);

                        $retornoDados = array_merge($retornoDados, $resposta);
                        break;
                }
            }
        }


        return $retornoDados;
    }

    /**
     * @param $dadosVeiculo
     * @return array
     */
    private function licenciadoAte($dadosVeiculo){
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->loadHTML($dadosVeiculo);
        $tables = $dom->getElementsByTagName('table');

        $subject = $tables[0]->nodeValue;

        $pattern = '/(\w+)\/(\d+)/';
        if(preg_match( $pattern, $subject , $matches)){
            return [
                'licenciado_ate' => $matches[0],
                'licenciado_mes' => $matches[1],
                'licenciado_ano' => $matches[2]
            ];
        }

        return [];
    }

    /**
     * @param $dadosVeiculo
     * @return array
     */
    private function valorDocumento($dadosVeiculo){
        libxml_use_internal_errors(true);
        $dom = new \DOMDocument();
        $dom->loadHTML($dadosVeiculo);
        $tables = $dom->getElementsByTagName('table');

        $subject = $tables[0]->nodeValue;

        $pattern = '/([Tt]otal\s*[Gg]eral:\s*(\d*,\d*))/';
        if(preg_match( $pattern, $subject , $matches)){
            return [
                'total_geral' => trim(preg_replace('/\s\s+/', ' ', $matches[0])),
                'valor' => $matches[2]
            ];
        }

        return [];
    }

    /**
     * @param string $placa
     * @param $renavam
     * @return array
     * @throws \Exception
     */
    private function informacoesDoVeiculo($placa = '', $renavam = '0'){
        $cookie = $this->coletarCookiesDaSession();

        if(!$cookie['success']){
            Common::setError('Erro ao se conectar com o site!');
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => DetranUtils::_INFO_URL_MS,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => http_build_query(['placa' => $placa, 'renavam' => $renavam]),
            CURLOPT_HTTPHEADER => array(
                "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "accept-encoding: gzip, deflate",
                "accept-language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7",
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "cookie: {$cookie['success']}",
                "origin: http://www2.detran.ms.gov.br",
                "postman-token: be4c6af9-4f05-9868-7b33-6586e0dc85d1",
                "referer: http://www2.detran.ms.gov.br/detranet/nsite/veiculo/veiculos/cVeic.asp",
                "upgrade-insecure-requests: 1",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36",
                "x-devtools-emulate-network-conditions-client-id: B52DF6AA835F232D2D3147B0D162CADA"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Common::setError($err);
        }

        return ['success' => $response];
    }

    /**
     * @return array|string
     * @throws \Exception
     */
    private function coletarCookiesDaSession(){
        $cookie = storage_path('cookies/cookie.txt');;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => DetranUtils::_COOKIE_URL_MS,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HEADER => 1,
            CURLOPT_HTTPHEADER => array(
                "accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "accept-encoding: gzip, deflate",
                "accept-language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7",
                "cache-control: no-cache",
                "postman-token: 4f7372ca-1e09-3ea3-42b6-6e54a1add7cd",
                "referer: http://www.detran.ms.gov.br/veiculos-2/consulta-debitos-de-veiculos/",
                "upgrade-insecure-requests: 1",
                "user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36",
                "x-devtools-emulate-network-conditions-client-id: B52DF6AA835F232D2D3147B0D162CADA"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            Common::setError($err);
        }

        /* Trabalhando na String de Retorno para separa apenas os Cookies */
        preg_match_all('/([Ss]et-[Cc]ookie:\s?.*?;)/', $response, $cookies);
        $cookies = $cookies[0];
        $cookieFull = '';

        foreach ($cookies as $value){
            $cookieFull .= (preg_match('/([Ss]et-[Cc]ookie:\s)(.*)/', $value, $cookie)) ? $cookie[2] : '';
        }

        return ['success' => $cookieFull];
    }
}