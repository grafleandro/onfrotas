<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 04/08/18
 * Time: 18:30
 */

namespace App\Repository;


use App\Model\ContatoModel;
use App\Model\EmpresaModel;
use App\Model\EnderecoModel;
use App\Model\VeiculosModel;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class EmpresaRepository
{
    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function salvarDados(Request $request){
        $dadosEmpresa = $request->all();

        $respostaEndereco = EnderecoModel::create([
            'ende_logradouro_titulo' => $dadosEmpresa['logradouro'],
            'ende_bairro' => $dadosEmpresa['bairro'],
            'ende_cidade'=> $dadosEmpresa['cidade'],
            'ende_estado'=> $dadosEmpresa['estado'],
            'ende_complemento'=> $dadosEmpresa['complemento'],
            'ende_numero'=> $dadosEmpresa['numero'],
            'ende_cep' => $dadosEmpresa['cep'],
            'enlo_id' => $dadosEmpresa['tipo_logradouro'],
        ]);

        if(!$respostaEndereco){
            Common::setError('Houve um erro ao salvar o Endereço!');
        }

        $respostaContato = ContatoModel::create([
            'cont_cel_1' => Mask::removerMascara($dadosEmpresa['telefone_cel_1']),
            'cont_cel_2' => Mask::removerMascara($dadosEmpresa['telefone_cel_2']),
            'cont_tel_fixo' => Mask::removerMascara($dadosEmpresa['phone_fixo']),
            'cont_email' => $dadosEmpresa['email'],
        ]);

        if(!$respostaContato){
            Common::setError('Houve um erro ao salvar os Contatos!');
        }

        $responsEmpresa = EmpresaModel::create([
            'clem_razao_social' => $dadosEmpresa['razao_social'],
            'clem_nome_fantasia' => $dadosEmpresa['nome_fantasia'],
            'clem_cnpj' => Mask::removerMascara($dadosEmpresa['cnpj']),
            'clem_insc_estadual' => $dadosEmpresa['inscricao_estadual'],
            'clem_observacao' => null,
            'ende_id' => $respostaEndereco->ende_id,
            'cont_id' => $respostaContato->cont_id,
        ]);

        if(!$responsEmpresa){
            Common::setError('Houve um erro ao salvar os dados da Empresa!');
        }

        return ['success' => 1];
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){
        $dadosEmpresa = EmpresaModel::with(['empresaEndereco', 'empresaContato'])->get()->toArray();

        foreach ($dadosEmpresa as &$empresa){
            $empresa['empresa_contato']['cont_cel_1'] = Mask::telCelular($empresa['empresa_contato']['cont_cel_1']);
            $empresa['empresa_contato']['cont_cel_2'] = Mask::telCelular($empresa['empresa_contato']['cont_cel_2']);
            $empresa['contato']     = $empresa['empresa_contato']['cont_cel_1'] . ' / ' . $empresa['empresa_contato']['cont_cel_2'];
            $empresa['clem_cnpj']   = Mask::cnpj($empresa['clem_cnpj']);
            $empresa['qtd_veiculo'] = VeiculosModel::where('clem_id', $empresa['clem_id'])->count();
        }

        $dadosTabela = Datatables::of($dadosEmpresa)
            ->addColumn('action', function ($empresa){
                return '<div style="text-align: center">
                                <button title="Editar Mecanica" data-empresa="'. $empresa['clem_id'] .'" style="margin: 4px" class="btn btn-default" onclick="jQueryMecanica.editarMecanica($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir" data-empresa="'.$empresa['clem_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryMecanica.deletarMecanica($(this))"><i class="fa fa-trash-o"></i></button>
                                <a href="'. url("colaboradores/cadastrar_empresa_usuario?empresa={$empresa['clem_id']}") .'" title="Adicionar Usuário Primário" data-empresa="'.$empresa['clem_id'].'" style="margin: 4px" class="btn btn-default"><i class="fa fa-user"></i></a>
                                <button title="Adicionar Mensalidade" data-empresa="'.$empresa['clem_id'].'" class="btn btn-default" onclick="jQueryFinanceiro.cadastrarMensalidade($(this))"><i class="fa fa-dollar"></i></button>
                        </div>';
            })
            ->editColumn('nome_fantazia', '{{ $clem_nome_fantasia }}')
            ->editColumn('clem_cnpj', '{{ $clem_cnpj }}')
            ->editColumn('qtd_veiculo', '{{ $qtd_veiculo }}')
            ->editColumn('mensalidade', '0')
            ->make(true);

        return $dadosTabela;
    }

    /**
     * @return mixed
     */
    public static function viewDashboard(){
//        return EmpresaModel::where('clem_id', Session::get('clem_id'))->first()->toArray();
        return [];
    }
}