<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 28/07/18
 * Time: 00:52
 */

namespace App\Repository;


use App\Http\Requests\FrotasRequest;
use App\Model\FrotaModel;
use App\Model\FrotaVeiculoModel;
use App\Utils\Common;
use App\Utils\VeiculoUtils;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class FrotasRepository
{
    private $veiculosRepository;

    public function __construct(VeiculosRepository $veiculosRepository)
    {
        $this->veiculosRepository = $veiculosRepository;
    }

    /**
     * @param $frota
     * @return array
     * @throws \Exception
     */
    public function salvarDados($frota){

        if(is_null($frota['frota']) && count($frota['veiculos']) > 0){
            /* Salvando os dados da Frota */
            $resposta = $this->salvarFrota($frota);

            /* Salvando os dados dos Veiculos da Frota */
            foreach ($frota['veiculos'] as $index => $veiculo){
                $frotaVeiculo = FrotaVeiculoModel::create([
                    'frot_id' => $resposta['frota'],
                    'veic_id' => $veiculo['id'],
                    'clem_id' => Session::get('clem_id'),
                ]);

                if(!$frotaVeiculo){
                    Common::setError('Houve um erro ao salvar os dados Veículo na Frota!');
                }
            }

            return ['success' => 1];
        }else if(is_null($frota['frota']) && count($frota['veiculos']) == 0){
            /* Salvando os dados da Frota */
            return $this->salvarFrota($frota);
        }
    }

    /**
     * @param null $frota
     * @return array
     * @throws \Exception
     */
    public function tabela($frota = null){

        if(isset($frota['frota']) && !empty($frota['frota'])){
            $tabelaVeiculo = $this->veiculosRepository->tabela([
                'tipo' => VeiculoUtils::_FROTA,
                'frota' => $frota['frota']
            ]);

        } else {
            $dadosFrota = FrotaModel::with(['frotaVeiculo', 'frotaUnidade', 'frotaPessoa'])->where('clem_id', Session::get('clem_id'))->get();

            $tabelaVeiculo = DataTables::of($dadosFrota->toArray())
                ->addColumn('action', function ($frota) {
                    return '<div style="text-align: center">
                                <button title="Editar Frota" data-frota="' . $frota['frot_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryFrota.editarFrota($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir Frota" data-frota="' . $frota['frot_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryFrota.deletarFrota($(this))"><i class="fa fa-trash-o"></i></button>                                  
                            </div>';
                })
                ->editColumn('pess_id', '{{ $frota_pessoa["pess_nome"] }}')
                ->editColumn('unid_id', '{{ $frota_unidade["unid_titulo"] }}')
                ->editColumn('qtd_veiculo', '{{ count($frota_veiculo) }}')
                ->editColumn('frot_id', '{{ $frot_id }}')
                ->make(true);
        }

        return $tabelaVeiculo;
    }

    /**
     * @param FrotasRequest $frotasRequest
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function atualizarFrota(FrotasRequest $frotasRequest, int $id){
        $dadosFrotaPost = $frotasRequest->all();
        $dadosFrota = $this->findById($id);

        $dadosFrota->frot_cod_interno = $dadosFrotaPost['cod_frota'];
        $dadosFrota->unid_id = $dadosFrotaPost['id_unidade'];
        $dadosFrota->pess_id = $dadosFrotaPost['id_pessoa'];

        if(!$dadosFrota->save()){
            Common::setError('Erro ao atualizar os dados da Frota!');
        }

        if($this->deletarVeiculoFrota(null, $id)){
            /* Salvando os dados dos Veiculos da Frota */
            foreach ($dadosFrotaPost['veiculos'] as $index => $veiculo){
                $frotaVeiculo = FrotaVeiculoModel::create([
                    'frot_id' => $id,
                    'veic_id' => $veiculo['id'],
                    'clem_id' => Session::get('clem_id'),
                ]);

                if(!$frotaVeiculo){
                    Common::setError('Houve um erro ao salvar os dados Veículo na Frota!');
                }
            }
        }else{
            Common::setError('Erro ao atualizar a lista de veículos da Frota!');
        }

        return ['success' => 1];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id){
        return FrotaModel::where('frot_id', '=', $id)->with(['frotaVeiculo', 'frotaUnidade', 'frotaPessoa'])->firstOrFail();
    }

    /**
     * @param $id
     * @return array
     */
    public function editarFrota($id){
        $dadosFrota = $this->findById($id);

        $retorno = [
            'frota' => [
                'id_frota' => $id,
                'unidade' => $dadosFrota->frotaUnidade->unid_titulo,
                'id_unidade' => $dadosFrota->frotaUnidade->unid_id,
                'cod_frota' => $dadosFrota->frot_cod_interno
            ],
            'pessoa' => $dadosFrota->frotaPessoa->toArray(),
        ];

        return $retorno;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletarFrota($id){
        return FrotaModel::where('frot_id', $id)->where('clem_id', Session::get('clem_id'))->delete();
    }

    /**
     * @param $veiculos
     * @return array
     */
    public function selecionarVeiculosFrota($veiculos){
        $respostaVeiculo = [];

        foreach ($veiculos as $index => $veiculo){
            $respostaVeiculo[] = ['id' => $veiculo->veic_id];
        }

        return $respostaVeiculo;
    }

    /**
     * @param $frota
     * @return array
     * @throws \Exception
     */
    private function salvarFrota($frota){
        /* Salvar dados da Frota */
        $resposta = FrotaModel::create([
            'frot_cod_interno' => $frota['cod_frota'],
            'unid_id' => $frota['id_unidade'],
            'pess_id' => $frota['id_pessoa'],
            'clem_id' => Session::get('clem_id'),
        ]);

        if(!$resposta){
            Common::setError('Erro ao salvar os dados!');
        }

        return ['success' => 1, 'frota' => $resposta->frot_id];
    }

    /**
     * @param null $idVeiculo
     * @param null $idFrota
     * @return bool
     */
    public function deletarVeiculoFrota($idVeiculo = null, $idFrota = null){
        if(!is_null($idVeiculo)){
            return FrotaVeiculoModel::where('veic_id', $idVeiculo)->where('clem_id', Session::get('clem_id'))->delete();
        }else if(!is_null($idFrota)){
            return FrotaVeiculoModel::where('frot_id', $idFrota)->where('clem_id', Session::get('clem_id'))->delete();
        }

        return false;
    }
}