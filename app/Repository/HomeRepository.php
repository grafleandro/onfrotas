<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 18/08/18
 * Time: 09:00
 */

namespace App\Repository;


use App\Model\OrdemServicoModel;
use App\Model\PessoaModel;
use App\Model\VeiculoDetranModel;
use App\Utils\Common;
use App\Utils\OrdemServicoUtils;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class HomeRepository
{
    /**
     * @return mixed
     */
    public function aniversariantes(){
        $diaAtual = (strlen(Carbon::now()->day) == 1) ? '0' . Carbon::now()->day : Carbon::now()->day;
        $mesAtual = (strlen(Carbon::now()->month) == 1) ? '0' . Carbon::now()->month : Carbon::now()->month;

        return PessoaModel::where('pess_data_nasc', 'like', '%' . $mesAtual . '-' . $diaAtual)
            ->where('clem_id', Session::get('clem_id'))
            ->get()
            ->count();
    }

    /** Quantidade de Ordem de Servicos aguardando Solicitacao
     * @return mixed
     */
    public function ordemServicoSolicitacao(){
        return OrdemServicoModel::where('sose_id', OrdemServicoUtils::_SOLICITACAO)
            ->where('clem_id', Session::get('clem_id'))
            ->get()
            ->count();
    }

    /** Verifica se possui algum veiculo com documento proximo ao venciemnto - (mes_atual + 1)
     * @return mixed
     */
    public function documentoVeiculoVencimento(){
        $data = Carbon::now()->addMonth();

        $mes = (strlen($data->month) == 1) ? '0' . $data->month : $data->month;
        $ano = $data->year;

        return VeiculoDetranModel::where('vede_licenciado_ate_mes', $mes)
            ->where('vede_licenciado_ate_ano', $ano)
            ->where('vede_status', 0)
            ->get()
            ->count();
    }

    public function cnhMotoristaVencida(){
        $data =Carbon::now()->format('Y-m-d');

        $colaboradores = PessoaModel::where('clem_id', Session::get('clem_id'))
                                        ->where('pess_vencimento_cnh', $data)
                                        ->get()->toArray();

        return $colaboradores;
    }
}