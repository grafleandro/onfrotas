<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 29/07/18
 * Time: 02:38
 */

namespace App\Repository;


use App\Model\ContatoModel;
use App\Model\MecanicasModel;
use App\Model\PessoaModel;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\OrdemServicoUtils;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class MecanicaRepository
{
    public function __construct()
    {

    }

    /**
     * @param $dadosMecanica
     * @return mixed
     * @throws \Exception
     */
    public function salvarDados($dadosMecanica){

        $respostaMecanica = MecanicasModel::where('clem_id', Session::get('clem_id'))->where('ofic_titulo', $dadosMecanica['nome'])->get();

        if($respostaMecanica->count()){
            Common::setError('Já existe uma Oficina cadastrada com esse nome!');
        }

        $contatoResponsavel = ContatoModel::create(array(
            'cont_cel_1' => (isset($dadosMecanica['phone'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosMecanica['phone']) : null,
        ));

        $mecanica = MecanicasModel::create(array(
            'ofic_titulo' => ($dadosMecanica['nome']) ?? null,
            'ofic_responsavel' => ($dadosMecanica['responsavel']) ?? null,
            'ofic_especializacao' => ($dadosMecanica['especializacao']) ?? null,
            'ofic_interna_externa' => ($dadosMecanica['oficina_int_ext']) ?? null,
            'pess_id' => ($dadosMecanica['id_pessoa']) ?? null,
            'cont_id' => $contatoResponsavel->cont_id,
            'clem_id' => Session::get('clem_id')
        ));

        return $mecanica->ofic_id;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){

        $dadosMecanicas = MecanicasModel::with(['mecanicaContato'])->where('clem_id', Session::get('clem_id'))->get()->toArray();

        $tableMecanica = [];

        foreach ($dadosMecanicas as $mecanica){
            $mecanica['mecanica_contato']['cont_cel_1'] = Mask::telCelular($mecanica['mecanica_contato']['cont_cel_1']);
            $mecanica['ofic_interna_externa'] = OrdemServicoUtils::getTitulo($mecanica['ofic_interna_externa']);

            $tableMecanica[] = $mecanica;
        }

        return Datatables::of($tableMecanica)
            ->addColumn('action', function ($mecanica){
                return '<div style="text-align: center">
                                <button title="Editar Mecanica" data-mecanica="'.$mecanica['ofic_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryMecanica.editarMecanica($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir" data-mecanica="'.$mecanica['ofic_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryMecanica.deletarMecanica($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->editColumn('cont_id', '{{ $mecanica_contato["cont_cel_1"] }}')
            ->make(true);

    }

    public function deletarMecanica($id){
        try{
            $mecanica = MecanicasModel::where('ofic_id', $id)->with(['mecanicaContato'])->find($id);

            $mecanica->mecanicaContato->delete();
            $resp = $mecanica->delete();

            return $resp;

        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }

    public function atualizarMecanica($dadosMecanica, $id){

        $mecanica = MecanicasModel::with(['mecanicaContato', 'mecanicaResponsavel'])->find($id);

        /* Dados Contato */
        $mecanica->mecanicaContato->cont_cel_1 = (isset($dadosMecanica['phone'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosMecanica['phone']) : null;
        $mecanica->mecanicaContato->save();

        /* Dados Mecanica */
        $mecanica->ofic_titulo = ($dadosMecanica['nome']) ?? null;
        $mecanica->ofic_responsavel = ($dadosMecanica['responsavel']) ?? null;
        $mecanica->ofic_responsavel = ($dadosMecanica['id_pessoa']) ?? null;
        $mecanica->ofic_especializacao = ($dadosMecanica['especializacao']) ?? null;
        $mecanica->ofic_interna_externa = ($dadosMecanica['oficina_int_ext']) ?? null;

        return $mecanica->save();
    }

    public function findById($id){
        return MecanicasModel::with(['mecanicaContato', 'mecanicaResponsavel'])->where('ofic_id', $id)->firstOrFail()->toArray();
    }

    public function autocomplete($valor){
        return MecanicasModel::where('clem_id', Session::get('clem_id'))->where('ofic_titulo', 'like', $valor . '%')->get()->toArray();
    }
}