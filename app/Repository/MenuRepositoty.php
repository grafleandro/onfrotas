<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 11/07/18
 * Time: 15:32
 */

namespace App\Repository;


use App\Model\MenuModel;
use Illuminate\Http\Response;

class MenuRepositoty
{
    private $menuModel;

    public function __construct(MenuModel $menuModel)
    {
        $this->menuModel = $menuModel;
    }

    public function all(){
        try{
            $menus = $this->menuModel->get()->toArray();;

            $this->menuModel->titulo = 'Tasdasdsa';
            $this->menuModel->desc = 'Tasdasdsa';

            if($this->menuModel->save()){
                $this->menuModel->menu_id;
                return ['success' => 1];
            }

            return $menus;
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }
}