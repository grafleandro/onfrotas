<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 04/08/18
 * Time: 14:30
 */

namespace App\Repository;


use App\Model\OrdemServicoArquivoModel;
use App\Utils\Common;
use App\Utils\FilesUtils;
use Illuminate\Http\Request;
use Folklore\Image\Facades\Image;

class OrdemServicoArquivoRepository
{
    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function salvarArquivo(Request $request){
        $dadosArquivo = $request->all();

        foreach ($request->file() as $index => $file){
            $path = $file->store('storage/ordem_servico');

            if(FilesUtils::isPdf($file->getMimeType())){
                $thumbnail = 'storage/img/pdf.png';
            }else{
                $thumbnail = $path;
            }

            $respostaArquivo = OrdemServicoArquivoModel::create([
                'orsa_url' => $path,
                'orsa_thumbnail' => $thumbnail,
                'orsa_titulo' => $dadosArquivo['titulo_arquivos'],
                'orse_id' => $dadosArquivo['referencia'],
            ]);

            if(!$respostaArquivo){
                Common::setError('Houve erro ao salvar os dados!');
            }
        }

        return ['success' => 1];
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deletarArquivo($id){
        return OrdemServicoArquivoModel::where('orsa_id', $id)->delete();
    }
}