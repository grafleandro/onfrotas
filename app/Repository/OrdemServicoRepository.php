<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 26/07/18
 * Time: 01:09
 */

namespace App\Repository;

use App\Http\Requests\OrdemServicoRequest;
use App\Model\EnderecoModel;
use App\Model\ManutencaoPrioridadeModel;
use App\Model\ManutencaoTipoModel;
use App\Model\OrdemServicoArquivoModel;
use App\Model\OrdemServicoModel;
use App\Model\OrdemServicoStatusHistoricoModel;
use App\Model\PessoaModel;
use App\Model\StatusOrdemServicoModel;
use App\Model\TipoLogradouroModel;
use App\Model\VeiculoTipoModel;
use App\User;
use App\Utils\Common;
use App\Utils\FilesUtils;
use App\Utils\FormUtils;
use App\Utils\Mask;
use App\Utils\Money;
use App\Utils\MoneyUtils;
use App\Utils\OrdemServicoUtils;
use App\Utils\PDF;
use App\Utils\UsuarioUtils;
use App\Utils\VeiculoUtils;
use Carbon\Carbon;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Psy\TabCompletion\Matcher\CommandsMatcher;
use Yajra\DataTables\Facades\DataTables;

class OrdemServicoRepository
{
    /**
     * @param OrdemServicoRequest $ordemServicoRequest
     * @return array
     * @throws \Exception
     */
    public function salvarDados(OrdemServicoRequest $ordemServicoRequest){

        $dadosOrdemServico = $ordemServicoRequest->all();

        $resposta = OrdemServicoModel::create([
            'orse_desc_servico' => ($dadosOrdemServico['desc_serv']) ?? null,
            'orse_desc_pecas' => ($dadosOrdemServico['desc_pecas']) ?? null,
            'orse_tempo_estimado' => ($dadosOrdemServico['tempo_estimado']) ?? null,
            'orse_valor' => (isset($dadosOrdemServico['ordem_servico_valor'])) ? MoneyUtils::removeCaracters($dadosOrdemServico['ordem_servico_valor']) : '',
            'clem_id' => Session::get('clem_id'),
            'unid_id' => ($dadosOrdemServico['id_unidade']) ?? null,
            'veic_id' => ($dadosOrdemServico['id_veiculo']) ?? null,
            'mati_id' => ($dadosOrdemServico['tipo_manutencao']) ?? null,
            'mapr_id' => ($dadosOrdemServico['prioridade_manutencao']) ?? null,
            'ofic_id' => ($dadosOrdemServico['id_oficina']) ?? null,
            'sose_id' => ($dadosOrdemServico['status_os']) ?? null,
        ]);

        if(!$resposta){
            Common::setError('Houve erro ao salvar os dados!');
        }

        /* Salvando imagens */
        foreach ($ordemServicoRequest->file() as $index => $file){
            $path = $file->store('public/ordem_servico');

            if(FilesUtils::isPdf($file->getMimeType())){
                $thumbnail = 'public/img/pdf.png';
            }else{
                $thumbnail = $path;
            }

            $respostaArquivo = OrdemServicoArquivoModel::create([
                'orsa_url' => $path,
                'orsa_thumbnail' => $thumbnail,
                'orsa_titulo' => '',
                'orse_id' => $resposta->orse_id,
                'pess_id' => Auth::user()->pess_id,
            ]);

            if(!$respostaArquivo){
                Common::setError('Houve erro ao salvar os dados!');
            }
        }

        return ['success' => 1];
    }

    /**
     * @param OrdemServicoRequest $ordemServicoRequest
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function atualizarDados(OrdemServicoRequest $ordemServicoRequest, int $id){
        /* Dados que serao atualizados vindos do Formulario */
        $ordemServico = $ordemServicoRequest->all();

        /* Dados da OS vindos do BD */
        $dadosOrdemServico = $this->findById($id);

        /* Guarda o Status anterior da OS */
        $statusOsAnterior = $dadosOrdemServico->sose_id;

        $dadosOrdemServico->unid_id = ($ordemServico['id_unidade']) ?? null;
        $dadosOrdemServico->ofic_id = ($ordemServico['id_oficina']) ?? null;
        $dadosOrdemServico->mati_id     = ($ordemServico['tipo_manutencao']) ?? null;
        $dadosOrdemServico->mapr_id     = ($ordemServico['prioridade_manutencao']) ?? null;
        $dadosOrdemServico->sose_id     = ($ordemServico['status_os']) ?? null;
        $dadosOrdemServico->orse_executar   = ($ordemServico['ordem_servico_executar']) ?? null;
        $dadosOrdemServico->orse_valor      = (isset($ordemServico['ordem_servico_valor'])) ? MoneyUtils::removeCaracters($ordemServico['ordem_servico_valor']) : null;
        $dadosOrdemServico->orse_desc_servico   = ($ordemServico['desc_serv']) ?? null;
        $dadosOrdemServico->orse_desc_pecas     = ($ordemServico['desc_pecas']) ?? null;

        if(!$dadosOrdemServico->save()){
            Common::setError('Houve um erro ao atualizar os dados!');
        }

        /* Grava Historico de Status da OS */
        $respostaOSSH = OrdemServicoStatusHistoricoModel::create([
            'ossh_anterior' => $statusOsAnterior,
            'ossh_atual' => ($ordemServico['status_os']) ?? null,
            'ossh_motivo' => (empty($statusOs['motivo'])) ? null : $statusOs['motivo'],
            'orse_id' => $id,
            'pess_id' => Auth::user()->pess_id,
        ]);

        if(!$respostaOSSH){
            Common::setError('Erro ao salvar os histórico do Status da OS!');
        }

        return ['success' => 1];
    }

    /**
     * @param $status
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function atualizarStatus($statusOs, $id){
        if($statusOs['status'] == OrdemServicoUtils::_CANCELADA || $statusOs['status'] == OrdemServicoUtils::_AGUARDANDO  && empty($statusOs['motivo'])){
            Common::setError('Você precisa informar o Motivo para este tipo de ação!');
        }

        /* Dados da OS vindos do BD */
        $dadosOrdemServico = $this->findById($id);

        /* Guarda o Status anterior da OS */
        $statusOsAnterior = $dadosOrdemServico->sose_id;

        $dadosOrdemServico->sose_id = $statusOs['status'];

        if(!$dadosOrdemServico->save()){
            Common::setError('Houve um erro ao atualizar os dados!');
        }

        /* Grava Historico de Status da OS */
        $respostaOSSH = OrdemServicoStatusHistoricoModel::create([
            'ossh_anterior' => $statusOsAnterior,
            'ossh_atual' => $statusOs['status'],
            'ossh_motivo' => (empty($statusOs['motivo'])) ? null : $statusOs['motivo'],
            'orse_id' => $id,
            'pess_id' => Auth::user()->pess_id,
        ]);

        if(!$respostaOSSH){
            Common::setError('Erro ao salvar os histórico do Status da OS!');
        }

        return ['success' => 1];
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Exception
     */
    public function tabela(Request $request){

        $dadosOrdemServico = $this->montarFiltro($request->all());

            $ordemServico = [];

            foreach ($dadosOrdemServico as $index => $value) {
                $ordemServico[] = [
                    'orse_id' => $value['orse_id'],
                    'unid_id' => $value['unid_id'],
                    'unidade' => $value['os_unidade']['unid_titulo'],
                    'veic_id' => $value['veic_id'],
                    'veiculo' => $value['os_veiculo']['veic_modelo'] . ' - ' . Mask::placaVeiculo($value['os_veiculo']['veic_placa']),
                    'mati_id' => $value['mati_id'],
                    'manutencao_tipo' => $value['os_manutencao_tipo']['mati_titulo'],
                    'mapr_id' => $value['mapr_id'],
                    'manutencao_prioriddade' => $value['os_manutencao_prioridade']['mapr_titulo'],
                    'status' => $value['os_status']
                ];
            }

            /* Selecionar os Status de OS */
            $ordemServicoStatus = StatusOrdemServicoModel::get()->toArray();

            $tabelaOrdemServico = DataTables::of($ordemServico)
                ->addColumn('action', function ($os) use ($request){
                    return $this->menusListarOS($request, $os);
                })
                ->addColumn('status', function ($os) use ($request, $ordemServicoStatus){
                    $dadosOrdemServico = $this->findById($os['orse_id']);

                    if($request->user()->can('tabelaAlterarStatus', $dadosOrdemServico)){
                        return view('tabela/status', ['osStatus' => $ordemServicoStatus, 'sose_id' => $os["status"]["sose_id"], 'orse_id' => $os['orse_id']]);
                    }else{
                        return '<span class="badge bg-'. $os["status"]["sose_btn_cor"] .'">'. $os["status"]["sose_titulo"] .'</span>';
                    }
                })
                ->rawColumns(['action', 'status'])
                ->make(true);


        return $tabelaOrdemServico;
    }

    /**
     * @param $dados
     * @return array
     */
    private function montarFiltro($dados){
        $filtro = [
            ['clem_id', '=', Session::get('clem_id')]
        ];

        if($dados['status'] != 0 && !empty($dados['status'])){
            $filtro[] = ['sose_id', '=', $dados['status'] ];
        }

        return OrdemServicoModel::with(['osVeiculo', 'osUnidade', 'osManutencaoTipo', 'osManutencaoPrioridade', 'osOficina', 'osArquivo', 'osStatus'])
            ->where($filtro)
            ->get()->toArray();
    }

    /**
     * @param $id
     * @return OrdemServicoModel|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findById($id){
        return OrdemServicoModel::with(['osVeiculo', 'osUnidade', 'osManutencaoTipo', 'osManutencaoPrioridade', 'osOficina', 'osArquivo', 'osStatus'])
            ->where('orse_id', '=', $id)
            ->firstOrFail();
    }

    public function editarOS(Request $request, $id){
        $dadosOS = $this->findById($id)->toArray();
        $placaVeiculo = Mask::placaVeiculo($dadosOS['os_veiculo']['veic_placa']);

        $retorno = [
            'ordem_servico' => [
                'id_os' => $id,
                'id_unidade' => $dadosOS['os_unidade']['unid_id'],
                'id_veiculo' => $dadosOS['os_veiculo']['veic_id'],
                'id_oficina' => $dadosOS['os_oficina']['ofic_id'],
                'id_mati' => $dadosOS['mati_id'],
                'id_mapr' => $dadosOS['mapr_id'],
                'unidade' => $dadosOS['os_unidade']['unid_titulo'],
                'oficina' => $dadosOS['os_oficina']['ofic_titulo'],
                'veiculo' => $dadosOS['os_veiculo']['veic_modelo'] . ' - ' . $placaVeiculo,
                'valor' => MoneyUtils::addPrefixPrecision($dadosOS['orse_valor']),
                'executar' => (is_null($dadosOS['orse_executar'])) ? OrdemServicoUtils::_INTERNA : $dadosOS['orse_executar'],
                'os_status' => $dadosOS['sose_id'],
                'desc_serv' => $dadosOS['orse_desc_servico'],
                'desc_pecas' => $dadosOS['orse_desc_pecas'],
            ],
            'prioridade' => $this->getPrioridadeManutencao(),
            'tipo_manutencao' => $this->getTipoManutencao(),
            'status_os' => $this->getOrdemServicoStatus($request),
            'arquivos' => $dadosOS['os_arquivo']
        ];

        return $retorno;
    }

    /**
     * @param Request $request
     * @param null $dadosOS
     * @return mixed
     */
    public function menusListarOS(Request $request, $dadosOS = null){
        $dadosOrdemServico = $this->findById($dadosOS['orse_id']);

        $buttons = '';

        if($request->user()->can('menuTabelaEditar', $dadosOrdemServico)){
            $buttons .= '<button title="Editar OS" data-os="' . $dadosOrdemServico->orse_id . '" class="btn btn-default margin-right" onclick="jQueryOrdemServico.editarOrdemServico($(this))"><i class="fa fa-pencil"></i></button>';
        }

        if($request->user()->can('menuTabelaExcluir', $dadosOrdemServico)){
            $buttons .= '<button title="Excluir OS" data-os="' . $dadosOrdemServico->orse_id . '" class="btn btn-default margin-right" onclick="jQueryOrdemServico.deletarOrdemServico($(this))"><i class="fa fa-trash"></i></button>';
        }

        if($request->user()->can('menuTabelaImprimir', $dadosOrdemServico)){
            $buttons .= '<button title="Imprimir OS" data-url="' .url('ordem_servico'). '/imprimir" data-imprimi="' . $dadosOrdemServico->orse_id . '" class="btn btn-default margin-right" onclick="jQueryOrdemServico.imprimir_os($(this))"><i class="fa fa-print"></i></button>';
        }

        if($request->user()->can('menuTabelaVisulizar', $dadosOrdemServico)){
            $buttons .= '<button title="Visulizar OS" data-os="' .$dadosOrdemServico->orse_id . '" class="btn btn-default margin-right" onclick="jQueryOrdemServico.visualizarOrdemServico($(this))"><i class="fa fa-eye"></i></button>';
        }

        if($request->user()->can('menuTabelaVisulizar', $dadosOrdemServico) && OrdemServicoUtils::ordemServicoStatusInfo($dadosOS['status']['sose_id'])){
            $buttons .= '<button title="Informação da OS" data-os="' .$dadosOrdemServico->orse_id . '" class="btn btn-default margin-right" onclick="jQueryOrdemServico.visualizarInformacao($(this))"><i class="fa fa-exclamation"></i></button>';
        }

        return '<div class="pull-center">'. $buttons .'</div>';
    }

    public function deletarOrdemServico($id){
        return OrdemServicoModel::where('orse_id', $id)->delete();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function imprimir($id){

        $file = new PDF();
        $file->SetCreator('LMS', true);
        $file->SetFont('Times','B',12);
        $os = OrdemServicoModel::with(['osVeiculo', 'osUnidade', 'osManutencaoTipo', 'osManutencaoPrioridade', 'osOficina', 'osArquivo'])
            ->where('orse_id', '=', $id)
            ->firstOrFail()->toArray();
        $this->printUnidade($file, $os['os_unidade']);
        $this->printVeiculo($file, $os['os_veiculo']);
        //inicio de linha no arquivo
        $this->printMecanica($file, $os['os_oficina']);
        $this->printTipoManutecao($file, $os['os_manutencao_tipo']);
        $this->printPrioridadeManutecao($file, $os['os_manutencao_prioridade']);
        $this->printServico($file, $os['orse_desc_servico']);
        $this->printPecas($file, $os['orse_desc_pecas']);

            $file->Output();
            exit;
//        return response()->json(['success'=>1, 'pdf' => $file->Output()]);

    }

    public function printPecas(Fpdf $file, $dados){
        $file->SetFont('Times','B',10);
        $file->Cell(190,7,'PEÇAS',1,0,'C');$file->ln(0);
        $file->SetFont('Times','',10);
        $file->ln();
        //desricao do pecas
        $file->MultiCell(190, 6, $dados,0,'L');$file->ln(7);
    }

    public function printServico(Fpdf $file, $dados){
        $file->SetFont('Times','B',10);
        $file->Cell(190,7,'SERVIÇO',1,0,'C');$file->ln(0);
        $file->SetFont('Times','',10);
        $file->ln();
        //desricao do servico
        $file->MultiCell(190, 6,$dados,0,'L');$file->ln(7);
    }

    public function printMecanica(Fpdf $file, $dados){
        $file->Cell(64, 6,'Mecânica: '.$dados['ofic_titulo'], 0,0); $file->SetX(64);
    }

    public function printTipoManutecao(Fpdf $file, $dados){
        $file->Cell(63, 6,'Tipo da Manutenção: '.$dados['mati_titulo'], 0,0); $file->SetX(127);
    }

    public function printPrioridadeManutecao(Fpdf $file, $dados){
        $file->Cell(63, 6,'Prioridade da Manutenção: '.$dados['mapr_titulo'], 0,0);$file->ln(7);
    }

    /**
     * @param Fpdf $file
     * @param $dados
     */
    public function printVeiculo(Fpdf $file, $dados){
        $file->SetFont('Times','B',10);
        $file->Cell(190,7,'Dados da Ordem de Serviço',1,0,'C');$file->ln(0);
        $file->SetFont('Times','',10);
        $file->ln();
        //linha no arquivo
        $file->Cell(64, 6,'Veículo: '.$dados['veic_modelo'], 0,0); $file->SetX(64);
        $file->Cell(63, 6,'Placa: '.$dados['veic_placa'], 0,0); $file->SetX(127);
        $file->Cell(63, 6,'Tipo do Veículo: '. VeiculoTipoModel::find($dados['veti_id'])->firstOrFail()->toArray()['veti_titulo'], 0,0);$file->ln(7);
        //linha no arquivo
        $file->Cell(64, 6,'Ano do Veículo: : '.$dados['veic_ano'], 0,0); $file->SetX(64);
        $file->Cell(63, 6,'Combustível: '. VeiculoUtils::tituloCombustivel($dados['veti_id']), 0,0); $file->SetX(127);
        $file->Cell(63, 6,'KM do Veículo: '. $dados['veic_km'], 0,0);$file->ln(7);

    }
    /**
     * @param $file
     * @param $dados
     */
    public function printUnidade(Fpdf $file, $dados){
        $endereco = EnderecoModel::where('ende_id', '=', $dados['ende_id'])->firstOrFail()->toArray();
        $file->AddPage();
        $file->SetFont('Times','B',10);
        $file->Cell(190,7,'UNIDADE',1,0,'C');$file->ln(0);
        $file->SetFont('Times','',10);
        $file->ln();
        $file->Cell(95, 6,'Nome: '.$dados['unid_titulo'], 0,0); $file->SetX(95); $file->Cell(95, 6,'CNPJ: '. Mask::cnpj($dados['unid_cnpj']), 0,0);$file->ln(7);
        $file->Cell(95, 6,TipoLogradouroModel::find($endereco['enlo_id'])->firstOrFail()->toArray()['enlo_titulo'].', '.
            $endereco['ende_logradouro_titulo'].', nº'.$endereco['ende_numero'], 0,0); $file->SetX(95);
        $file->Cell(95, 6,'Bairro '. $endereco['ende_bairro'].'    -   '.
            $endereco['ende_cidade'].' / '. $endereco['ende_estado'], 0,0);$file->ln(7);
    }

    /**
     * @return mixed
     */
    public function getPrioridadeManutencao(){
        return ManutencaoPrioridadeModel::get()->toArray();
    }

    /**
     * @return mixed
     */
    public function getTipoManutencao(){
        return ManutencaoTipoModel::get()->toArray();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getOrdemServicoStatus(Request $request){

        if(UsuarioUtils::usuarioEhGestor($request->user()->perfil)){
            $osStatus = StatusOrdemServicoModel::get()->toArray();
        }else{
            $osStatus = StatusOrdemServicoModel::where('sose_id', OrdemServicoUtils::_SOLICITACAO)->get()->toArray();
        }

        foreach ($osStatus as $index => &$value){
            $value['sose_titulo'] = $value['sose_abreviacao'] . ' - ' . $value['sose_titulo'];
        }

        return $osStatus;
    }

    /**
     * @param array $dadosOsInfo
     * @return mixed
     */
    public function ordemServicoInfo(array $dadosOsInfo){
        $ordemServicoHist = OrdemServicoStatusHistoricoModel::with(['osStatus'])
            ->where('orse_id', $dadosOsInfo['orse'])
            ->whereIn('ossh_atual', [OrdemServicoUtils::_CANCELADA, OrdemServicoUtils::_AGUARDANDO])
            ->get()->toArray();

        foreach ($ordemServicoHist as $index => &$historico){
            $historico['created_at'] = Carbon::parse($historico['created_at'])->format('d/m/Y H:i:s');
        }

        return $ordemServicoHist;
    }
}