<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 07/08/18
 * Time: 00:41
 */

namespace App\Repository;


use App\Model\OrdemServicoModel;
use App\Utils\Common;
use App\Utils\Mask;
use App\Utils\MoneyUtils;
use App\Utils\PDF;
use Carbon\Carbon;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class RelatorioRepository
{
    public function relatorioVeiculo($id){

        $dadosOrdemServico = OrdemServicoModel::with(['osVeiculo', 'osUnidade', 'osManutencaoTipo', 'osManutencaoPrioridade'])->where('veic_id', $id)->get()->toArray();

        $tabelaOrdemServico = [];

            foreach ($dadosOrdemServico as $veiculo_os){
                $veiculo_os['orse_valor'] = ($veiculo_os['orse_valor']) ? MoneyUtils::addPrefixPrecision($veiculo_os['orse_valor']) : "";
                $veiculo_os['created_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $veiculo_os['created_at'])->format('d/m/Y');
                $tabelaOrdemServico[] = $veiculo_os;
            }


            $tabelaOrdemServico = DataTables::of($tabelaOrdemServico)->make(true);



        return $tabelaOrdemServico;
    }

    private function montarFiltro($dados){
        $filtro = [
            ['clem_id', '=', Session::get('clem_id')]
        ];

        if($dados['tipo_os'] != 0 && !empty($dados['tipo_os'])){
            $filtro[] = ['orse_executar', '=', $dados['tipo_os'] ];
        }

        if(isset($dados['oficina']) && !empty($dados['oficina'])){
            $filtro[] = ['ofic_id', '=', $dados['oficina'] ];
        }
        if(isset($dados['dt_inicio']) && !empty($dados['dt_inicio'])){
            $filtro[] = ['created_at', '>=', Carbon::createFromFormat('d/m/Y', $dados['dt_inicio'])->format('Y-m-d 00:00:00')];
        }
        if(isset($dados['dt_final']) && !empty($dados['dt_final'])){
            $filtro[] = ['created_at', '<=', Carbon::createFromFormat('d/m/Y', $dados['dt_final'])->format('Y-m-d 23:59:59') ];
        }

        return OrdemServicoModel::with(['osVeiculo', 'osUnidade', 'osManutencaoTipo', 'osManutencaoPrioridade', 'osOficina', 'osArquivo'])
            ->where($filtro)
            ->get()->toArray();
    }

    public function relatorioManutencao($dados) {

        $dadosOrdemServico = $this->montarFiltro($dados);

        $tabelaOrdemServico = [];

        foreach ($dadosOrdemServico as $veiculo_os){
            $veiculo_os['orse_valor'] = ($veiculo_os['orse_valor']) ? MoneyUtils::addPrefixPrecision($veiculo_os['orse_valor']) : "";
            $veiculo_os['created_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $veiculo_os['created_at'])->format('d/m/Y');
            $tabelaOrdemServico[] = $veiculo_os;
        }

        $tabelaOrdemServico = DataTables::of($tabelaOrdemServico)->make(true);

        return $tabelaOrdemServico;
    }

    public function imprimir($id){

        $os = OrdemServicoModel::with(['osVeiculo', 'osUnidade', 'osManutencaoTipo', 'osManutencaoPrioridade', 'osOficina', 'osArquivo'])
            ->where('veic_id', '=', $id)
            ->get()->toArray();

        $file = new PDF();
        $file->SetCreator('LMS', true);
        $file->SetFont('Times','B',12);

        $this->relatorio($file, $os);

        $file->Output();

        exit;
    }

    public function imprimirManutencao($dados){

        $dadosOrdemServico = $this->montarFiltro($dados);

        $file = new PDF();
        $file->SetCreator('OutPut Web - Onfrotas', true);
        $file->SetFont('Times','B',12);

        $this->relatorio($file, $dadosOrdemServico);

        $file->Output();

        exit;
    }

    private function relatorio(Fpdf $file, $dados){
        $file->AddPage();
        $file->SetFont('Times','B',10);
        $file->Cell(190,7,'RELATÓRIO DE MANUTENÇÃO',1,0,'C');$file->ln(7);
        $file->SetFont('Times','B',9);
        $file->Cell(10,4,'#',1,0,'C');//$file->SetX(15);
        $file->Cell(27,4,'Veículo',1,0,'C');//$file->SetX(42);
        $file->Cell(27,4,'Oficina',1,0,'C');//$file->SetX(69);
        $file->Cell(40,4,'Serviço',1,0,'C');//$file->SetX(108);
        $file->Cell(40,4,'Peças',1,0,'C');//$file->SetX(135);
        $file->Cell(27,4,'Valor',1,0,'C');//$file->SetX(162);
        $file->Cell(19,4,'Data',1,1,'C');

        $file->SetFont('Times','',9);

        foreach ($dados as $os) {
            $file->Cell(10, 4, $os['orse_id'], 1, 0, 'C');//$file->SetX(15);
            $file->Cell(27, 4, substr($os['os_veiculo']['veic_modelo'],0, 27) . ' / ' . Mask::placaVeiculo($os['os_veiculo']['veic_placa']), 1, 0, 'C');//$file->SetX(42);
            $file->Cell(27, 4, substr($os['os_oficina']['ofic_titulo'],0, 27), 1, 0, 'C');//$file->SetX(69);
            $file->Cell(40, 4, substr($os['orse_desc_servico'],0,27), 1, 0, 'C');//$file->SetX(108);
            $file->Cell(40, 4, substr($os['orse_desc_pecas'],0,27), 1, 0, 'C');//$file->SetX(135);
            $file->Cell(27, 4, ($os['orse_valor']) ? MoneyUtils::addPrefixPrecision($os['orse_valor']) : "", 1, 0, 'C');//$file->SetX(162);
            $file->Cell(19, 4, Carbon::createFromFormat('Y-m-d H:i:s', $os['created_at'])->format('d/m/Y'), 1, 1, 'C');
        }

    }
}