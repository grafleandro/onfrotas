<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 10/08/18
 * Time: 15:06
 */

namespace App\Repository;


use App\Http\Requests\requisicaoRequest;
use App\Model\ConfiguracaoClienteModel;
use App\Model\RequisicaoModel;
use App\Model\UnidadeModel;
use App\Utils\Mask;
use App\Utils\PDF;
use App\Utils\MoneyUtils;
use App\Utils\TipoRequisicao;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;
use Codedge\Fpdf\Fpdf\Fpdf;

class RequisicaoRepository
{
    public function __construct()
    {

    }

    public function salvarDados($dadosRequisicao){
//        Buscando as configurações da empresa
        $config_cliente = ConfiguracaoClienteModel::where('clem_id', Session::get('clem_id'))->get()->first();

        // Não iniciado o contador de requisicao ele é iniciado
        if(!isset($config_cliente->cocl_num_requisicao) ) {
            $config = new ConfiguracaoClienteModel;

            $config->cocl_num_requisicao = 1;

            $config->clem_id = Session::get('clem_id');
        }else{
//            Existindo o contador de requisicao ele é incrementado
            $config = $config_cliente;

            $config->cocl_num_requisicao = $config_cliente->cocl_num_requisicao + 1;
        }
        $config->save();
        //Salvando uma nova requisicao
        $requisicao = new RequisicaoModel;

        $requisicao->requ_tipo_requisicao = $dadosRequisicao['tipo_requisicao'];
        $requisicao->requ_quantidade = $dadosRequisicao['quantidade'];
        $requisicao->requ_descricao = $dadosRequisicao['descricao'];
        $requisicao->requ_valor = (isset($dadosRequisicao['valor'])) ? MoneyUtils::removeCaracters($dadosRequisicao['valor']) : null;
        $requisicao->clem_id = Session::get('clem_id');
        $requisicao->unid_id = $dadosRequisicao['id_unidade'];
        $requisicao->veic_id = $dadosRequisicao['id_veiculo'];

        $requisicao->save();

        return $requisicao->requ_id;

    }

    public function tabela($dados){

        $dadosRequisicao = $this->montarFiltro($dados);

        $tableRequisicao = [];

        foreach ($dadosRequisicao as $requisicao){
            $requisicao['requ_valor'] = ($requisicao['requ_valor']) ? MoneyUtils::addPrefixPrecision($requisicao['requ_valor']) : '';
            $requisicao['requ_tipo_requisicao'] = TipoRequisicao::tituloTipoRequisacao($requisicao['requ_tipo_requisicao']);
            $requisicao['created_at'] = Carbon::createFromFormat('Y-m-d H:i:s', $requisicao['created_at'])->format('d/m/Y');

            $tableRequisicao[] = $requisicao;
        }

        return Datatables::of($tableRequisicao)
            ->addColumn('action', function ($requisicao){
                return '<div style="text-align: center">
                                <button title="Editar Requisicao" data-requisicao="'.$requisicao['requ_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryRequisicao.editarRequisicao($(this))"><i class="fa fa-pencil"></i></button>
                                <button title="Excluir" data-requisicao="'.$requisicao['requ_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryRequisicao.deletarRequisicao($(this))"><i class="fa fa-trash-o"></i></button>
                                <button title="Imprimir Requisição" data-url="'.url('requisicao').'/imprimir" data-imprimi="'.$requisicao['requ_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryForm.imprimir($(this))"><i class="fa fa-print"></i></button>
                        </div>';
            })
            ->editColumn('veic_id', '{{ $requisicao_veiculo["veic_modelo"] }}')
            ->make(true);

    }

    private function montarFiltro($dados){
        $filtro = [
            ['clem_id', '=', Session::get('clem_id')]
        ];

        if($dados['veiculo'] != 0 && !empty($dados['veiculo'])){
            $filtro[] = ['veic_id', '=', $dados['veiculo'] ];
        }

        if(isset($dados['tipo_requisicao']) && !empty($dados['tipo_requisicao'])){
            $filtro[] = ['requ_tipo_requisicao', '=', $dados['tipo_requisicao'] ];
        }
        if(isset($dados['dt_inicio']) && !empty($dados['dt_inicio'])){
            $filtro[] = ['created_at', '>=', Carbon::createFromFormat('d/m/Y', $dados['dt_inicio'])->format('Y-m-d 00:00:00')];
        }
        if(isset($dados['dt_final']) && !empty($dados['dt_final'])){
            $filtro[] = ['created_at', '<=', Carbon::createFromFormat('d/m/Y', $dados['dt_final'])->format('Y-m-d 23:59:59') ];
        }

        return RequisicaoModel::with(['requisicaoVeiculo'])
            ->where($filtro)
            ->get()->toArray();
    }


    public function deletarRequisicao($id){
        try{
            $requisicao = RequisicaoModel::find($id);

            $resp = $requisicao->delete();

            return $resp;

        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }

    public function atualizarRequisicao($dadosRequisicao, $id){

        $requisicao = RequisicaoModel::find($id);

        $requisicao->requ_tipo_requisicao = $dadosRequisicao['tipo_requisicao'];
        $requisicao->requ_quantidade = $dadosRequisicao['quantidade'];
        $requisicao->requ_descricao = $dadosRequisicao['descricao'];
        $requisicao->requ_valor = (isset($dadosRequisicao['valor'])) ? MoneyUtils::removeCaracters($dadosRequisicao['valor']) : null;
        $requisicao->clem_id = Session::get('clem_id');
        $requisicao->unid_id = $dadosRequisicao['id_unidade'];
        $requisicao->veic_id = $dadosRequisicao['id_veiculo'];
        $requisicao->updated_at = Carbon::now()->format('Y-m-d H:i:s');

        return $requisicao->save();

    }

    public function imprimir($id){

        $requisicao = RequisicaoModel::with(['requisicaoVeiculo', 'requisicaoUnidade', 'requisicaoConfiguracaoCliente'])->where('requ_id', $id)->firstOrFail()->toArray();

        $file = new PDF();
        $file->SetCreator('OutPut Web / onFrotas', true);
        $file->SetFont('Times','B',12);

        $this->requisicao($file, $requisicao);

        $file->Output();

        exit;
    }

    private function requisicao(Fpdf $file, $dados){
        $file->AddPage();
        $file->SetFont('Times','B',10);
        $file->Cell(190,7,'REQUISIÇÃO Nº' . $dados['requisicao_configuracao_cliente']['cocl_num_requisicao'],1,0,'C');$file->ln(0);
        $file->SetFont('Times','',10);
        $file->ln();

        //linha no arquivo
        $file->Cell(64, 6,'Data: '.Carbon::createFromFormat('Y-m-d H:i:s', $dados['created_at'])->format('d/m/Y'), 0,0); $file->SetX(64);
        $file->Cell(63, 6,'Tipo de Requisição: '. TipoRequisicao::tituloTipoRequisacao($dados['requ_tipo_requisicao']), 0,0); $file->SetX(127);
        $file->Cell(63, 6,'Unidade: '. $dados['requisicao_unidade']['unid_titulo'], 0,0);$file->ln(7);
        //linha no arquivo
        $file->Cell(64, 6,'Veículo: '.$dados['requisicao_veiculo']['veic_modelo'], 0,0); $file->SetX(64);
        $file->Cell(63, 6,'Placa: '. strtoupper(Mask::placaVeiculo($dados['requisicao_veiculo']['veic_placa'])), 0,0); $file->SetX(127);
        $file->Cell(63, 6,'Ano: '. $dados['requisicao_veiculo']['veic_ano'], 0,0);$file->ln(7);

        $file->MultiCell(190, 6,'Descrição: ' . $dados['requ_descricao'],0,'L');$file->ln(7);

        $file->ln(30);

        $file->Cell(190,7,'__________________________________',0,0,'C');$file->ln(7);
        $file->Cell(190,4,'Assinatura do Resposável',0,0,'C');$file->ln(4);

    }

    public function findById($id){
        return RequisicaoModel::with(['requisicaoVeiculo', 'requisicaoUnidade'])->where('requ_id', $id)->firstOrFail()->toArray();
    }
}