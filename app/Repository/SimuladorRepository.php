<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 30/08/18
 * Time: 19:46
 */

namespace App\Repository;


use App\Mail\EnviarNotificacaoMail;
use App\Model\SimuladorModel;
use App\Utils\Common;
use App\Utils\Mask;

class SimuladorRepository
{
    /**
     * @param array $dadosSimulador
     * @return array
     * @throws \PHPMailer\PHPMailer\Exception
     * @throws \Throwable
     */
    public function salvarDados(array $dadosSimulador){
        $respostaSimulador = SimuladorModel::create([
            'clac_razao_social' => $dadosSimulador['razao_social'],
            'clac_email' => $dadosSimulador['email'],
            'clac_contato' => Mask::removerMascara($dadosSimulador['contato-cel']),
            'clac_cnpj' => Mask::removerMascara($dadosSimulador['cnpj']),
            'clac_qtd_veiculo' => $dadosSimulador['qtd_veiculo'],
            'clac_tipo' => $dadosSimulador['tipo'],
        ]);

        if(!$respostaSimulador->save()){
            Common::setError('Erro ao salvar os dados do Simulador!');
        }

        /* Enviar Email */
        (new EnviarNotificacaoMail())->enviarNotificacao(
            'Simulador',
            'Novo Cliente',
            'contato@onfrotas.com.br',
            'email.novo_cliente',
            [
                'razao_social' => $dadosSimulador['razao_social'],
                'email' => $dadosSimulador['email'],
                'contato' => $dadosSimulador['contato-cel'],
                'cnpj' => $dadosSimulador['cnpj'],
                'qtd_veiculo' => $dadosSimulador['qtd_veiculo'],
                'tipo' => $dadosSimulador['tipo'],
            ]
        );

        return ['success' => 1];
    }
}