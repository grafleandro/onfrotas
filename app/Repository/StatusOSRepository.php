<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 31/07/18
 * Time: 11:15
 */

namespace App\Repository;


use App\Model\StatusOrdemServicoModel;
use App\User;
use App\Utils\Common;
use App\Utils\Mask;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class StatusOSRepository
{
    public function __construct()
    {

    }

    public function salvarDados($dadosStatus){

        $resp = StatusOrdemServicoModel::where('sose_titulo', $dadosStatus['nome'])->where('sose_abreviacao', $dadosStatus['abreviacao'])->get()->toArray();

        if(!isset($resp[0])) {

            $status = StatusOrdemServicoModel::create(array(

                'sose_titulo'  => ($dadosStatus['nome']) ? $dadosStatus['nome'] : null,
                'sose_abreviacao' => ($dadosStatus['abreviacao']) ? $dadosStatus['abreviacao'] : null,
                'sose_descricao' => ($dadosStatus['descricao']) ? $dadosStatus['descricao'] : null,
            ));

            return $status->sose_id;
        }else{
            Common::setError("Ja existe este tipo de Status!!");

        }
    }

    public function tabela(){

        $dadosStatus = StatusOrdemServicoModel::get()->toArray();

        return Datatables::of($dadosStatus)
            ->addColumn('action', function ($status){
                return '<div style="text-align: center">
                                <button title="Editar Usuário" data-status="'.$status['sose_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryStatus.editarStatus($(this))"><i class="fa fa-pencil"></i></button>
                                <button title="Excluir" data-status="'.$status['sose_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryStatus.deletarStatus($(this))"><i class="fa fa-trash-o"></i></button>
                        </div>';
            })
            ->make(true);

    }

    public function deletarUsuario($id){
        try{
            $status = StatusOrdemServicoModel::get()->find($id);

            $resp = $status->delete();

            return $resp;

        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }

    public function atualizarUsuario($dadosStatus, $id){

        $status = StatusOrdemServicoModel::find($id);

        $status->sose_titulo  = ($dadosStatus['nome']) ? $dadosStatus['nome'] : null;
        $status->sose_abreviacao = ($dadosStatus['abreviacao']) ? $dadosStatus['abreviacao'] : null;
        $status->sose_descricao = ($dadosStatus['descricao']) ? $dadosStatus['descricao'] : null;

        return $status->save();

    }


    public function findById($id){
        return StatusOrdemServicoModel::get()->find($id)->toArray();
    }
}