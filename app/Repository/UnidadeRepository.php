<?php
/**
 * Created by PhpStorm.
<<<<<<< Updated upstream
 * User: julio
 * Date: 28/07/18
 * Time: 01:20
=======
 * User: lms
 * Date: 27/07/18
 * Time: 00:14
>>>>>>> Stashed changes
 */

namespace App\Repository;

use App\Model\ContatoModel;
use App\Model\EnderecoModel;
use App\Model\TipoLogradouroModel;
use App\Model\UnidadeModel;
use App\Utils\Common;
use App\Utils\Mask;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\Facades\DataTables;

class UnidadeRepository
{
    public function __construct()
    {

    }

    /**
     * @param array $dadosUnidade
     * @return int
     * @throws \Exception
     */
    public function salvarDados(array $dadosUnidade){

        $respostaUnidade = UnidadeModel::where('unid_cnpj', str_replace(array('-', ' ', '(', ')', '.', '/'), "", $dadosUnidade['cnpj']))->get();

        if($respostaUnidade->count()){
            Common::setError('Já possui uma Unidade cadastrada com esse CNPJ: ' . $dadosUnidade['cnpj']);
        }

        $contato_pes = ContatoModel::create(array(
            'cont_cel_1' => (isset($dadosUnidade['telefone_cel'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosUnidade['telefone_cel']) : null,
            'cont_cel_2' => (isset($dadosUnidade['telefone_cel1'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosUnidade['telefone_cel1']) : null,
            'cont_tel_fixo' => (isset($dadosUnidade['phone_fixo'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosUnidade['phone_fixo']) : null,
            'cont_email' => (isset($dadosUnidade['email'])) ? $dadosUnidade['email'] : null,
        ));

        $endereco = EnderecoModel::create(array(
            'enlo_id' => ($dadosUnidade['tipo_logradouro']) ? $dadosUnidade['tipo_logradouro'] : null,
            'ende_logradouro_titulo' => (isset($dadosUnidade['logradouro'])) ? $dadosUnidade['logradouro'] : null,
            'ende_bairro' => (isset($dadosUnidade['bairro'])) ? $dadosUnidade['bairro'] : null,
            'ende_cidade' => (isset($dadosUnidade['cidade'])) ? $dadosUnidade['cidade'] : null,
            'ende_estado' => (isset($dadosUnidade['estado'])) ? $dadosUnidade['estado'] : null,
            'ende_complemento' => (isset($dadosUnidade['complemento'])) ? $dadosUnidade['complemento'] : null,
            'ende_numero' => (isset($dadosUnidade['numero'])) ? $dadosUnidade['numero'] : null,
            'ende_cep' => (isset($dadosUnidade['cep'])) ? $dadosUnidade['cep'] : null,
        ));

        $unidade = UnidadeModel::create(array(
            'unid_titulo' => ($dadosUnidade['titulo']) ? $dadosUnidade['titulo'] : null,
            'unid_razao_social' => ($dadosUnidade['razao_social']) ? $dadosUnidade['razao_social'] : null,
            'unid_cnpj' => ($dadosUnidade['cnpj']) ? str_replace(array('-', ' ', '(', ')', '.', '/'), "", $dadosUnidade['cnpj']) : null,
            'unid_insc_estadual' => ($dadosUnidade['insc_estadual']) ? $dadosUnidade['insc_estadual'] : null,
            'unid_observacoes' => ($dadosUnidade['obs']) ? $dadosUnidade['obs'] : null,
            'ende_id' => $endereco->ende_id,
            'cont_id' => $contato_pes->cont_id,
            'clem_id' => Session::get('clem_id'),
            'pess_id' => ($dadosUnidade['id_pessoa']) ?? null,
        ));

        return $unidade->unid_id;
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function tabela(){

        $dadosUnidades = UnidadeModel::with(['unidadeContato','unidadeEndereco'])->where('clem_id', Session::get('clem_id'))->get()->toArray();

        $tableUnidade = [];

        foreach ($dadosUnidades as $unidade){
            $unidade['unid_cnpj'] = Mask::cnpj($unidade['unid_cnpj']);

            $tableUnidade[] = $unidade;
        }

        return Datatables::of($tableUnidade)
            ->addColumn('action', function ($unidade){
                return '<div style="text-align: center">
                                <button title="Editar Unidade" data-unidade="'.$unidade['unid_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryUnidade.editarUnidade($(this))"><i class="fa fa-edit"></i></button>
                                <button title="Excluir" data-unidade="'.$unidade['unid_id'].'" style="margin: 4px" class="btn btn-default" onclick="jQueryUnidade.deletarUnidade($(this))"><i class="fa fa-trash-o"></i></button>                                  
                        </div>';
            })
            ->make(true);

    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletarUnidade($id){
        try{

            $unidade = UnidadeModel::where('unid_id', $id)->with(['unidadeContato','unidadeEndereco'])->find($id);

            $unidade->unidadeContato->delete();

            $unidade->unidadeEndereco->delete();

            $resp = $unidade->delete();

            return $resp;

        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * @param $dadosUnidade
     * @param $id
     * @return bool
     */
    public function atualizarUnidade($dadosUnidade, $id){
        $unidade = UnidadeModel::with(['unidadeContato','unidadeEndereco'])->find($id);

        //Dados Unidade
        $unidade->unid_titulo = ($dadosUnidade['titulo']) ? $dadosUnidade['titulo'] : null;
        $unidade->unid_razao_social = ($dadosUnidade['razao_social']) ? $dadosUnidade['razao_social'] : null;
        $unidade->unid_cnpj = ($dadosUnidade['cnpj']) ? str_replace(array('-', ' ', '(', ')', '.', '/'), "", $dadosUnidade['cnpj']) : null;
        $unidade->unid_insc_estadual = ($dadosUnidade['insc_estadual']) ? $dadosUnidade['insc_estadual'] : null;
        $unidade->unid_observacoes = ($dadosUnidade['obs']) ? $dadosUnidade['obs'] : null;

        //Dados Contato
        $unidade->unidadeContato->cont_cel_1 = (isset($dadosUnidade['telefone_cel'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosUnidade['telefone_cel']) : null;
        $unidade->unidadeContato->cont_cel_2 = (isset($dadosUnidade['telefone_cel1'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosUnidade['telefone_cel1']) : null;
        $unidade->unidadeContato->cont_tel_fixo = (isset($dadosUnidade['phone_fixo'])) ? str_replace(array('-', ' ', '(', ')'), "", $dadosUnidade['phone_fixo']) : null;
        $unidade->unidadeContato->cont_email = (isset($dadosUnidade['email'])) ? $dadosUnidade['email'] : null;
        $unidade->unidadeContato->save();

        //Dados Endereco
        $unidade->unidadeEndereco->enlo_id = ($dadosUnidade['tipo_logradouro']) ? $dadosUnidade['tipo_logradouro'] : null;
        $unidade->unidadeEndereco->ende_logradouro_titulo = (isset($dadosUnidade['logradouro'])) ? $dadosUnidade['logradouro'] : null;
        $unidade->unidadeEndereco->ende_bairro = (isset($dadosUnidade['bairro'])) ? $dadosUnidade['bairro'] : null;
        $unidade->unidadeEndereco->ende_cidade = (isset($dadosUnidade['cidade'])) ? $dadosUnidade['cidade'] : null;
        $unidade->unidadeEndereco->ende_estado = (isset($dadosUnidade['estado'])) ? $dadosUnidade['estado'] : null;
        $unidade->unidadeEndereco->ende_complemento = (isset($dadosUnidade['complemento'])) ? $dadosUnidade['complemento'] : null;
        $unidade->unidadeEndereco->ende_numero = (isset($dadosUnidade['numero'])) ? $dadosUnidade['numero'] : null;
        $unidade->unidadeEndereco->ende_cep = (isset($dadosUnidade['cep'])) ? $dadosUnidade['cep'] : null;
        $unidade->unidadeEndereco->save();

        return $unidade->save();
    }

    /**
     * @return mixed
     */
    public function getTipoLogradouro(){
        return TipoLogradouroModel::get()->toArray();
    }

    /**
     * @param $valor
     * @return mixed
     */
    public function autocomplete($valor){
        return UnidadeModel::where('unid_titulo', 'like', $valor . '%')->where('clem_id', Session::get('clem_id'))->get()->toArray();
    }

    /**
     * @param $id
     * @return array
     */
    public function findById($id){
        return UnidadeModel::with(['unidadeContato','unidadeEndereco', 'unidadeResponsavel'])->where('unid_id', $id)->firstOrFail()->toArray();
    }
}