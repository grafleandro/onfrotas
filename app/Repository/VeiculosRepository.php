<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 26/07/18
 * Time: 10:01
 */

namespace App\Repository;


use App\Model\FrotaVeiculoModel;
use App\Model\PessoaModel;
use App\Model\VeiculoDetranModel;
use App\Model\VeiculosModel;
use App\Utils\CalendarioUtils;
use App\Utils\Common;
use App\Utils\DetranUtils;
use App\Utils\Mask;
use App\Utils\MoneyUtils;
use App\Utils\VeiculoUtils;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Laracasts\Utilities\JavaScript\JavaScriptFacade;
use Yajra\DataTables\Facades\DataTables;

class VeiculosRepository
{
    private $detranRepository;

    public function __construct(DetranRepository $detranRepository)
    {
        $this->detranRepository = $detranRepository;
    }

    /**
     * @param $dadosVeiculo
     * @return array
     * @throws \Exception
     */
    public function salvarDados($dadosVeiculo){
        $placa = str_replace('-', '', $dadosVeiculo['placa']);

        $veiculo = VeiculosModel::create([
            'veic_modelo' => $dadosVeiculo['modelo'],
            'veic_placa' => str_replace('-', '', $dadosVeiculo['placa']),
            'veic_renavam' => $dadosVeiculo['renavam'],
            'veic_combustivel' => $dadosVeiculo['combustivel'],
            'veic_ano' => $dadosVeiculo['ano'],
            'veic_km' => $dadosVeiculo['km'],
            'veic_uf_emplacamento' => $dadosVeiculo['uf_emplacamento'],
            'veti_id' => $dadosVeiculo['tipo_veiculo'],
            'veco_id' => $dadosVeiculo['cor'],
            'clem_id' => Session::get('clem_id'),
        ]);

        if(!$veiculo){
            Common::setError('Erro ao salvar os dados!');
        }

        /* Salvando as Informacoes do Veiculo */
        $veiculoInfo = $this->detranRepository->extrairInformacoes($placa, $dadosVeiculo['renavam'], [DetranUtils::_TODOS]);

        /* Seleciona o Indice do Mes do Licenciamento */
        if(in_array($dadosVeiculo['uf_emplacamento'], DetranUtils::_UF_HABILITADA)){
            $veiculoInfo['licenciado_mes'] = CalendarioUtils::indiceMes($veiculoInfo['licenciado_mes']);

            $veiculoInfo = VeiculoDetranModel::create([
                'vede_licenciado_ate_mes' => $veiculoInfo['licenciado_mes'],
                'vede_licenciado_ate_ano' => $veiculoInfo['licenciado_ano'],
                'vede_licenciamento_valor' => MoneyUtils::removeCaracters($veiculoInfo['valor']),
                'veic_id' => $veiculo->veic_id,
            ]);

            if(!$veiculoInfo){
                Common::setError('Erro ao salvar as informações do veículo!');
            }
        }

        return ['success' => 1];
    }

    /**
     * @param $dados
     * @param $id
     * @return mixed
     * @throws \Exception
     */
    public function atualizarVeiculo($dados, $id){
        $veiculo = VeiculosModel::find($id);

        if(!isset($dados['odometro'])) {
            $veiculo->veic_modelo = $dados['modelo'];
            $veiculo->veic_placa = $dados['placa'];
            $veiculo->veic_combustivel = $dados['combustivel'];
            $veiculo->veic_ano = $dados['ano'];
            $veiculo->veic_km = $dados['km'];
            $veiculo->veic_km = $dados['uf_emplacamento'];
            $veiculo->veti_id = $dados['tipo_veiculo'];
            $veiculo->veco_id = $dados['cor'];
        }else{
            if($veiculo->veic_km > $dados['odometro']){
                Common::setError('O valor do Odómetro está a baixo do KM registrado anteriormente!');
            }

            $veiculo->veic_km = $dados['odometro'];
        }

        return $veiculo->save();
    }

    /**
     * @param null $dados
     * @return array
     * @throws \Exception
     */
    public function tabela($dados = null)
    {
        $dadosVeiculo = $this->selecionarVeiculo($dados);

        foreach ($dadosVeiculo as $index => &$value){
            $value['veic_combustivel'] = VeiculoUtils::tituloCombustivel($value['veic_combustivel']);
        }

        $tabelaVeiculo = DataTables::of($dadosVeiculo)
            ->addColumn('action', function ($veiculo) use ($dados) {
                return $this->actionsButtons($dados, $veiculo);
            })
            ->editColumn('veic_placa', function($veiculo){
                return Mask::placaVeiculo($veiculo['veic_placa']);
            })
            ->editColumn('veti_id', '{{ $veiculo_tipo["veti_titulo"] }}')
            ->editColumn('id', '{{ $veic_id }}')
            ->make(true);

        return $tabelaVeiculo;
    }

    public function findById($id){
        return VeiculosModel::where('veic_id', $id)->firstOrFail()->toArray();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deletarVeiculo($id){
        try{
            return VeiculosModel::where('veic_id', $id)->delete();
        } catch (\Exception $e){
            return response()->json(['success' => 0, 'alert' => $e->getMessage()], Response::HTTP_NO_CONTENT);
        }
    }

    /**
     * @param $valor
     * @return mixed
     */
    public function autocomplete($valor){
        return VeiculosModel::where('veic_modelo', 'like', $valor . '%')
            ->orWhere('veic_placa', 'like', $valor . '%')
            ->where('clem_id', Session::get('clem_id'))
            ->get()
            ->toArray();
    }

    /**
     * @param $dados
     * @param array $dadosAuxiliar
     * @return array
     */
    private function selecionarVeiculo($dados, $dadosAuxiliar = []){

        switch ($dados['tipo']){
            case VeiculoUtils::_VEICULO:
                return VeiculosModel::with(['veiculoTipo','veiculoCor'])
                    ->where('clem_id', Session::get('clem_id'))
                    ->get()
                    ->toArray();
                break;
            case VeiculoUtils::_FROTA:
                if(isset($dados['frota']) && !empty($dados['frota'])){
                    $frotaVeiculo = FrotaVeiculoModel::where('frot_id', $dados['frota'])
                        ->where('clem_id', Session::get('clem_id'))
                        ->get()
                        ->toArray();

                    $frotaVeiculo = array_column($frotaVeiculo, 'veic_id');

                    $retornoVeiculo = VeiculosModel::with(['veiculoTipo','veiculoCor'])
                        ->where('clem_id', Session::get('clem_id'))
                        ->whereIn('veic_id', $frotaVeiculo)
                        ->get()
                        ->toArray();

                    foreach ($retornoVeiculo as $index => &$veiculo){
                        $veiculo['selecionar'] = true;
                    }

                    return $this->selecionarVeiculo(['tipo' => $dados['tipo']], $retornoVeiculo);
                } else {
                    $frotaVeiculo = FrotaVeiculoModel::where('clem_id', Session::get('clem_id'))->get()->toArray();

                    $frotaVeiculo = array_column($frotaVeiculo, 'veic_id');

                    $retornoVeiculo = VeiculosModel::with(['veiculoTipo','veiculoCor'])
                        ->where('clem_id', Session::get('clem_id'))
                        ->whereNotIn('veic_id', $frotaVeiculo)
                        ->get()
                        ->toArray();

                    if(count($dadosAuxiliar)){
                        foreach ($retornoVeiculo as $index => &$veiculo){
                            $veiculo['selecionar'] = false;
                        }

                        return array_merge($dadosAuxiliar, $retornoVeiculo);
                    }

                    return $retornoVeiculo;
                }
                break;
        }
    }

    /**
     * @param $tipo
     * @param $veiculo
     * @return string
     */
    private function actionsButtons($dados, $veiculo){
        switch ($dados['tipo']){
            case VeiculoUtils::_VEICULO:
                return '<div style="text-align: center">
                            <button title="Editar Cadastro" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryVeiculo.editarVeiculo($(this))"><i class="fa fa-edit"></i></button>
                            <button title="Excluir" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryVeiculo.deletarVeiculo($(this))"><i class="fa fa-trash-o"></i></button>
                            <button title="Adicionar Odómetro" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryVeiculo.adicionarOdometro($(this))"><img width="18px" src="/icon/speedometer.svg"></i></button>                                  
                        </div>';
                break;
            case VeiculoUtils::_FROTA:
                $classBtnSelecionar = (isset($veiculo['selecionar']) && $veiculo["selecionar"]) ? "fa-check-square-o" : "fa-square-o";

                return '<div style="text-align: center">
                            <button title="Selecionar Veículo" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default selecionar" onclick="jQueryFrota.selecionarVeiculo($(this))"><i class="fa '. $classBtnSelecionar .'"></i></button>
                            <button title="Adicionar Número do Veículo" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryFrota.addNumeroVeiculo($(this))"><img width="18px" src="/icon/numbering-signs.svg"></button>
                            <button title="Adicionar Motorista" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryFrota.addMotoristaVeiculo($(this))"><img width="18px" src="/icon/steering-weel.svg"></i></button>
                        </div>';
                break;
            case VeiculoUtils::_ORDEM_SERVICO:
                return '<div style="text-align: center">
                            <button title="Selecionar Veículo" data-veiculo="' . $veiculo['veic_id'] . '" style="margin: 4px" class="btn btn-default" onclick="jQueryOrdemServico.selecionarVeiculo($(this))"><i class="fa fa-square-o"></i></button>                                  
                        </div>';
                break;
        }
    }
}