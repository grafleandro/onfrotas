<?php

namespace App;

use App\Model\PessoaModel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'categoria',
        'perfil',
        'habilitacao',
        'cpf',
        'dt_nascimento',
        'vencimento_cnh',
        'pess_id',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function pessoa(){
        return $this->hasOne(PessoaModel::class, 'pess_id', 'pess_id');
    }
}
