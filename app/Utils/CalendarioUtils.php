<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 16/08/18
 * Time: 20:52
 */

namespace App\Utils;


class CalendarioUtils
{
    /** MESES */
    const _JAN = 1;
    const _FEV = 2;
    const _MAC = 3;
    const _ABR = 4;
    const _MAI = 5;
    const _JUN = 6;
    const _JUL = 7;
    const _AGO = 8;
    const _SET = 9;
    const _OUT = 10;
    const _NOV = 11;
    const _DEZ = 12;

    /**Selecionar os meses com seus respectivos titulos
     * @return array
     */
    public static function mesTitulo(){
        return [
            self::_JAN => 'Janeiro',
            self::_FEV => 'Feiveireio',
            self::_MAC => 'Março',
            self::_ABR => 'Abril',
            self::_MAI => 'Maio',
            self::_JUN => 'Junho',
            self::_JUL => 'Julho',
            self::_AGO => 'Agosto',
            self::_SET => 'Setembro',
            self::_OUT => 'Outubro',
            self::_NOV => 'Novembro',
            self::_DEZ => 'Dezembro',
        ];
    }

    /**
     * @param string $titulo
     * @return int|null|string
     */
    public static function indiceMes(string $titulo){
        foreach (self::mesTitulo() as $index => $value){
            similar_text(strtoupper($value), $titulo, $percent);

            if($percent > 95){
                return $index;
            }
        }

        return null;
    }
}