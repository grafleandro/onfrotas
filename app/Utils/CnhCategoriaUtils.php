<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 03/08/18
 * Time: 15:15
 */

namespace App\Utils;


class CnhCategoriaUtils
{
    /**
     * CATEGORIA CNH
     */
    const A  = 1;
    const B  = 2;
    const C  = 3;
    const D  = 4;
    const E  = 5;
    const AB  = 6;
    const AC  = 7;
    const AD  = 8;
    const AE  = 9;
    const ACC  = 10;


    public static function CnhCategoria(){
        return [
            self::A => [
                'titulo' => 'A',
                'id' => self::A
            ],
            self::B => [
                'titulo' => 'B',
                'id' => self::B
            ],
            self::C => [
                'titulo' => 'C',
                'id' => self::C
            ],
            self::D => [
                'titulo' => 'D',
                'id' => self::D
            ],
            self::E => [
                'titulo' => 'E',
                'id' => self::E
            ],
            self::AB => [
                'titulo' => 'AB',
                'id' => self::AB
            ],
            self::AC => [
                'titulo' => 'AC',
                'id' => self::AC
            ],
            self::AD => [
                'titulo' => 'AD',
                'id' => self::AD
            ],
            self::AE => [
                'titulo' => 'AE',
                'id' => self::AE
            ],
            self::ACC => [
                'titulo' => 'ACC',
                'id' => self::ACC
            ],

        ];
    }

    public static function tituloCategoriaCNH($id){
        return self::CnhCategoria()[$id]['titulo'];
    }
}