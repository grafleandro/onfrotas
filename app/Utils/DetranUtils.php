<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 16/08/18
 * Time: 13:39
 */

namespace App\Utils;


class DetranUtils
{
    const _LICENCIADO_ATE   = 1;
    const _VALOR_DOCUMENTO  = 2;
    const _TODOS = 3;

    /**
     * DETRANS URL's COOKIES
     */
    const _COOKIE_URL_MS = "http://www2.detran.ms.gov.br/detranet/nsite/veiculo/veiculos/cVeic.asp";

    /**
     * DETRANS URL's INFORMACOES
     */
    const _INFO_URL_MS = "http://www2.detran.ms.gov.br/detranet/nsite/veiculo/veiculos/retornooooveiculos.asp";

    /** UF's Com Sistema de Busca Habilitado */
    const _UF_HABILITADA = [
        'MS'
    ];

    /**
     * Tipos de Combustiveis
     */
    const COMB_GASOLINA    = 1;
    const COMB_ALCOOL     = 2;
    const COMB_DIESEL    = 3;
    const COMB_DIESEL_S10    = 4;

    public static function combustiveis(){
        return [
            self::COMB_ALCOOL => [
                'titulo' => 'Álcool',
                'id' => self::COMB_ALCOOL
            ],
            self::COMB_DIESEL => [
                'titulo' => 'Diesel',
                'id' => self::COMB_DIESEL
            ],
            self::COMB_DIESEL_S10 => [
                'titulo' => 'Diesel S10',
                'id' => self::COMB_DIESEL_S10
            ],
            self::COMB_GASOLINA => [
                'titulo' => 'Gasolina',
                'id' => self::COMB_GASOLINA
            ],
        ];
    }
}