<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 04/08/18
 * Time: 11:56
 */

namespace App\Utils;


class FilesUtils
{
    const _PATH_DEFAULT = 'http://localhost/onfrotas/';

    const _MIME_TYPE_PDF = 'application/pdf';

    public static function getUrl($file){
        return self::_PATH_DEFAULT . $file;
    }

    public static function isPdf($mimeType){
        return $mimeType == self::_MIME_TYPE_PDF;
    }
}