<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 11/08/18
 * Time: 10:55
 */

namespace App\Utils;


class FinanceiroUtils
{
    /** TIPOS DE PLANOS */
    const _FROTA_BAIXA  = 1;
    const _FROTA_MEDIA  = 2;
    const _FROTA_ALTA   = 3;

    public static function tiposDePlanos(){
        return [
            self::_FROTA_BAIXA => [
                'titulo' => 'Frota Baixa - R$ 9,00/Veículo',
                'id' => self::_FROTA_BAIXA
            ],
            self::_FROTA_MEDIA => [
                'titulo' => 'Frota Média - R$ 7,50/Veículo',
                'id' => self::_FROTA_MEDIA
            ],
            self::_FROTA_ALTA => [
                'titulo' => 'Frota Alta - R$ 375,00 + (Qtd. Veic. * R$ 7,50)',
                'id' => self::_FROTA_ALTA
            ]
        ];
    }
}