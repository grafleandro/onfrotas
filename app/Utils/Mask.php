<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 13/06/18
 * Time: 18:34
 */

namespace App\Utils;

class Mask
{
    public static function removerMascara($string){
        return str_replace(['_', '.', '/', '-', ',', '(', ')', ' '], "", $string);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function cnpj($val = "", $mask = '##.###.###/####-##'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function cpf($val = "", $mask = '###.###.###-##'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function telComercial($val = "", $mask = '(##) #### - ####'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function telCelular($val = "", $mask = '(##) # #### - ####'){
        return self::setMask($val, $mask);
    }

    /**
     * @param string $val
     * @param string $mask
     * @return string
     */
    public static function placaVeiculo($val = "", $mask = '###-####'){
        return self::setMask(strtoupper($val), $mask);
    }

    /**
     * @param $val
     * @param $mask
     * @return string
     */
    private static function setMask($val, $mask){
        $maskared = '';
        $k = 0;

        if(empty($val)){
            return "";
        }

        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}