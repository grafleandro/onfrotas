<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 13/08/18
 * Time: 08:42
 */

namespace App\Utils;


use App\Model\MenuModel;

class MenuUtils
{
    /**
     * @param array $menus
     * @param bool $mapear
     * @return array
     */
    public function montarEstruturaMenu($menus = [], $mapear = true){
        if($mapear){
            $menus = $this->mapearArrayMenu($menus);
        }

        foreach ($menus as $index => $menu){
            /* Seleciona os ancestrais de um determinado NODE */

            if(!empty($menu['menu_pai']) && !array_key_exists($menu['menu_pai'], $menus)){
                $menuSelecionar = MenuModel::where('menu_id', '=', $menu['menu_pai'])->get()->toArray()[0];

                $menus[$menuSelecionar['menu_id']] = $menuSelecionar;

                return $this->montarEstruturaMenu($menus, false);
            }else if(is_null($menu['menu_pai'])){
                /* Seleciona um filho de um determinado NODE */

                $menuSelecionar = MenuModel::where('menu_sequencia', 'like', $menu['menu_sequencia'] . '.%')
                    ->orderBy('menu.menu_sequencia', 'desc')
                    ->get()->toArray();

                foreach ($menuSelecionar as $indexSubmenu => $submenu){
                    $menus[$submenu['menu_id']] = $submenu;
                }
            }
        }

        return $menus;
    }

    /**
     * @param array $menus
     * @return array
     */
    public function mapearArrayMenu($menus = []){
        $arrayAux = [];

        foreach ($menus as $index => $menu){
            $arrayAux[$menu['menu_id']] = $menu;
        }

        return $arrayAux;
    }

    /**
     * @param array $menus
     * @return array
     */
    public function montarArvoreMenu($menus = []){
        foreach ($menus as $index => &$item){
            if(!empty($item['menu_pai'])){
                $menus[ $item['menu_pai'] ]['submenu'][] = $item;
                unset($menus[ $index ]);
            }
        }

        return array_reverse($menus);
    }

    /**
     * @param array $menus
     * @return array
     */
    public function agruparMenus($menus = []){
        /* Ordena de forma decrescente */
        uasort($menus, function($a, $b){
            if($a['menu_id'] == $b['menu_id']){
                return 0;
            }

            return $a['menu_id'] > $b['menu_id'] ? -1 : 1;
        });

        $menuRetorno = [];

        foreach ($menus as $index => $item){
            /* Monta um objeto do Item que serah adicionado */
            $objeto = [
                'text' => $item['menu_titulo'],
                'icon' => $item['menu_classe'],
                'url' => $item['menu_href']
            ];

            /* Verifica se eh um menu PAI ou menu FILHO */
            if(!is_null($item['menu_pai'])){

                /* Verifica se existe a estrutura para os submenus. Caso nao exista, serah criada */
                if(!isset($menuRetorno[$item['menu_pai']]['submenu'])){
                    /* Objeto do item PAI */
                    $objPai = $menus[$item['menu_pai']];

                    /* Verifica se o item ja possui filhos no vetor, caso sim, eh carregado seu filhos, caso contrario atribui um novo filho */
                    $objFilho = ($menuRetorno[$index]) ?? $objeto;

                    /* Prepara a estrutura do PAI */
                    $menuRetorno[$item['menu_pai']] = [
                        'text' => $objPai['menu_titulo'],
                        'icon' => $objPai['menu_classe'],
                        'url' => $objPai['menu_href'],
                        'submenu' => [ $item['menu_id'] => $objFilho ]
                    ];
                } else {
                    /* Caso ja exista a estrutura de subitens, apenas carrega o novo FILHO */
                    $menuRetorno[$item['menu_pai']]['submenu'][$item['menu_id']] = $objeto;
                }

                /* Ordena os Filhos de acordo com o ID */
                ksort($menuRetorno[$item['menu_pai']]['submenu']);

                /* Caso seja necessario, apos inserir o filhos soltos existentes, eh removido da pilha deixando apenas a estrutura PAI */
                unset($menuRetorno[ $index ]);
            }else if(!isset($menuRetorno[$index])){
                /* Caso seja um pai sem filhos */
                $menuRetorno[$item['menu_id']] = $objeto;
            }
        }

        /* Retorna o vetor em sua forma original, do MENOR para o MAIOR */
        return array_reverse($menuRetorno, true);
    }

    public function montarEstruturaMenuHtml($menus = [], $inicio = true, $idMenusSelecionado = []){
        $html = ($inicio) ? '<ul class="checktree">' : '';

        foreach($menus as $m){
            $checked = ( in_array($m['menu_id'], $idMenusSelecionado)) ? 'checked' : '';

            if(isset($m['submenu'])){
                $html .= '<li><input type="checkbox" '. $checked .' id="'. $m["menu_id"] .'" name="permissao[]" value="'. $m["menu_id"] .'" /><label for="'. $m["menu_id"] .'">'. $m["menu_titulo"] .'</label>' . PHP_EOL;
                $html .= '<ul>';

                uasort($m['submenu'], function($a, $b){
                    if ($a['menu_titulo'] == $b['menu_titulo']){
                        return 0;
                    }

                    return ($a['menu_titulo'] < $b['menu_titulo']) ? -1 : 1;
                });

                $html .= $this->montarEstruturaMenuHtml($m['submenu'], false, $idMenusSelecionado);

                $html .= '</ul></li>';
            }else{
                $html .= '<li><input type="checkbox" '. $checked .' id="'. $m["menu_id"] .'" name="permissao[]" value="'. $m["menu_id"] .'" /><label for="'. $m["menu_id"] .'">'. $m["menu_titulo"] .'</label></li>' . PHP_EOL;
            }
        }

        return $html;
    }
}