<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 14/06/18
 * Time: 12:31
 */

namespace App\Utils;

class MoneyUtils
{
    private $value;
    private $status;

    /**
     * @param $value_1
     * @param $value_2
     * @param string $prefix
     * @return object
     */
    public static function subtraction($value_1, $value_2, $prefix = "R$"){
        /* Remove Carecteres Desnecessario */
        $value_1 = self::removeCaracters($value_1);
        $value_2 = self::removeCaracters($value_2);

        /* Verifica se o valor e um MILHAR */
        if(self::isThousand($value_1)){
            $value_1 = str_replace(',', '', $value_1);
        }

        if(self::isThousand($value_2)){
            $value_2 = str_replace(',', '', $value_2);
        }

        /* Realiza o Calculo da Subtracao */
        $result = $value_1 - $value_2;

        /* Inserir precisao decimal de 2 casas */
        $setPrecisionValue = self::setPrecision($result);

        /* Adicionar Prefixo */
        $addPrefix = self::addPrefix($setPrecisionValue);

        return (object) ['value' => $addPrefix, 'status' => ($result > 0)];
    }

    public static function adition($value_1 = 0, $value_2 = 0){
        /* Remove Carecteres Desnecessario */
        $value_1 = self::removeCaracters($value_1);
        $value_2 = self::removeCaracters($value_2);

        return ($value_1 + $value_2);
    }

    public static function division($money = 0, $amount = 0, $dateBD = false){
        $money = self::removeCaracters($money);

        /* Realiza o calculo do valor dividido pela quantidade informada */
        $valuePerPlot = ($money/$amount);

        if($dateBD){
            /* Prepara o valor para ser salvo no BD */
            $valuePerPlot = str_replace([','], '.', $valuePerPlot);

            return $valuePerPlot;
        }

        return self::addPrefix($valuePerPlot);
    }

    /**
     * @param $value
     * @param string $prefix
     * @return string
     */
    public static function addPrefixPrecision($value, $prefix = "R$"){
        if(empty($value)){
            return '';
        }

        return ($value > 0) ? $prefix . " " . self::setPrecision($value) : $prefix . self::setPrecision($value);
    }

    /**
     * @param $value
     * @param $prefix
     * @return string
     */
    public static function addPrefix($value, $prefix = "R$"){
        return ($value > 0) ? $prefix . " " . $value : $prefix . $value;
    }

    /**
     * @param $value
     * @return mixed
     */
    public static function removeCaracters($value){
        return str_replace(',', '.', str_replace(['.', 'R$', ' '], "", $value));
    }

    /**
     * @param $value
     * @param int $precision
     * @return string
     */
    public static function setPrecision($value, $precision = 2){
        return number_format($value, $precision,',','.');
    }

    /**
     * @param $value
     * @return bool
     */
    public static function isThousand($value){
        return (strlen($value) > 6);
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}