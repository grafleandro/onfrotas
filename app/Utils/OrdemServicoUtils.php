<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 06/08/18
 * Time: 19:38
 */

namespace App\Utils;


class OrdemServicoUtils
{
    /** Tipos de Status de Ordem de Servico */
    const _INICIALIZADA = 1;
    const _SOLICITACAO  = 2;
    const _CANCELADA    = 3;
    const _FINALIZADA   = 4;
    const _AGUARDANDO   = 5;
    const _EXECUTANDO   = 6;

    /** Tipos de Ambiente onde serah executada as OS */
    const _EXTERNA = 1;
    const _INTERNA = 2;

    /**
     * @return array
     */
    public static function getIntExt(){
        return [
            self::_EXTERNA => [
                'titulo' => 'Externa',
                'id' => self::_EXTERNA
            ],
            self::_INTERNA => [
                'titulo' => 'Interna',
                'id' => self::_INTERNA
            ],
        ];
    }

    /**
     * @param $index
     * @return mixed
     */
    public static function getTitulo($index){
        return self::getIntExt()[$index]['titulo'];
    }

    /**
     * @return array
     */
    public static function ordemServicoExecucao(){
        return [
            self::_INICIALIZADA,
            self::_AGUARDANDO,
        ];
    }

    /**
     * @return array
     */
    public static function ordemServicoUsComum(){
        return [
            self::_INICIALIZADA => [
                'sose_titulo' => 'Inicializada',
                'sose_id' => self::_INICIALIZADA
            ],
            self::_AGUARDANDO => [
                'sose_titulo' => 'Aguardando',
                'sose_id' => self::_AGUARDANDO
            ],
            self::_EXECUTANDO => [
                'sose_titulo' => 'Executando',
                'sose_id' => self::_EXECUTANDO
            ],
            self::_FINALIZADA => [
                'sose_titulo' => 'Finalizada',
                'sose_id' => self::_FINALIZADA
            ]
        ];
    }

    /**
     * @param $status
     * @return bool
     */
    public static function ordemServicoStatusInfo($status){
        return ($status == self::_AGUARDANDO || $status == self::_CANCELADA);
    }
}