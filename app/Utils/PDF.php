<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 05/08/18
 * Time: 18:52
 */

namespace App\Utils;


use App\Model\EmpresaModel;
use App\Model\PessoaModel;
use App\Model\TipoLogradouroModel;
use App\User;
use Codedge\Fpdf\Fpdf\Fpdf;
use Illuminate\Support\Facades\Auth;

class Pdf extends Fpdf
{
    public function Header()
    {
        $user = Auth::user();

        $pessoa = PessoaModel::where('pess_id', $user->pess_id)->firstOrFail();

        $empresa = EmpresaModel::with(['empresaEndereco','empresaContato'])
            ->where('clem_id', '=', $pessoa->clem_id)
            ->firstOrFail()->toArray();

        $this->SetFont('Arial','B',14);
        $this->Cell(190,10,$empresa['clem_nome_fantasia'],0,0,'C');$this->Ln(5);
        $this->SetFont('Arial','',8);
        $this->Cell(190,10,$empresa['clem_razao_social'].' / '.Mask::cnpj($empresa['clem_cnpj']),0,0,'C');$this->Ln(5);
        $this->SetFont('Arial','B',10);
        $this->Cell(190,10,(Mask::telComercial($empresa['empresa_contato']['cont_tel_fixo'])) ?? '',0,0,'C');$this->Ln(5);
        $this->Cell(190,10,TipoLogradouroModel::find($empresa['empresa_endereco']['enlo_id'])->firstOrFail()->toArray()['enlo_titulo'].', '.
            $empresa['empresa_endereco']['ende_logradouro_titulo'].', nº'.$empresa['empresa_endereco']['ende_numero'].', Bairro '. $empresa['empresa_endereco']['ende_bairro'].' - '.
            $empresa['empresa_endereco']['ende_cidade'].' / '. $empresa['empresa_endereco']['ende_estado']
            ,0,0,'C');$this->ln(10);
        $this->SetLineWidth(0.5);
        $this->rect(10,     10, 190,24);
    }

    public function Footer()
    {
        $this->SetY(-15);
        $this->SetFont('Arial','I',8);
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }
}