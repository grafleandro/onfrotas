<?php
/**
 * Created by PhpStorm.
 * User: lms
 * Date: 10/08/18
 * Time: 15:51
 */

namespace App\Utils;


class TipoRequisicao
{
    /**
     * VARIAVEIS DE CONTROLE
     */
    const _COMBUSTIVEL  = 1;
    const _PECAS   = 2;


    public static function tipoRequisicao(){
        return [
            self::_COMBUSTIVEL => [
                'titulo' => 'Combustivel',
                'id' => self::_COMBUSTIVEL
            ],
            self::_PECAS => [
                'titulo' => 'Peças',
                'id' => self::_PECAS
            ],

        ];
    }

    public static function tituloTipoRequisacao($id){
        return self::tipoRequisicao()[$id]['titulo'];
    }
}