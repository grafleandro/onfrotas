<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 03/08/18
 * Time: 02:29
 */

namespace App\Utils;


class UsuarioUtils
{
    /**
     * Tipos de Usuarios
     */
    const _US_MASTER    = 0;
    const _US_ADMIN     = 1;
    const _US_GESTOR    = 2;
    const _US_COMUM     = 3;

    public static function PerfilUsuario(){
        return [
            self::_US_ADMIN => [
                'titulo' => 'Administrador',
                'id' => self::_US_ADMIN
            ],
            self::_US_GESTOR => [
                'titulo' => 'Gestor',
                'id' => self::_US_GESTOR
            ],
            self::_US_COMUM => [
                'titulo' => 'Usuário Comum',
                'id' => self::_US_COMUM
            ],


        ];
    }

    /**
     * @param $perfil
     * @return bool
     */
    public static function usuarioEhGestor($perfil){
        return ($perfil == self::_US_MASTER || $perfil == self::_US_GESTOR);
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function tituloPerfilUsuario($id){
        return self::PerfilUsuario()[$id]['titulo'];
    }

    /**
     * Função para gerar senhas aleatórias
     *
     * @author    Thiago Belem <contato@thiagobelem.net>
     *
     * @param integer $tamanho Tamanho da senha a ser gerada
     * @param boolean $maiusculas Se terá letras maiúsculas
     * @param boolean $numeros Se terá números
     * @param boolean $simbolos Se terá símbolos
     *
     * @return string A senha gerada
     */
    public static function generatePassword($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false){

        $lmin = 'abcdefghijklmnpqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';
        $num = '123456789';
        $simb = '!@#$%*-';

        $retorno = '';
        $caracteres = '';
        $caracteres .= $lmin;

        if ($maiusculas){
            $caracteres .= $lmai;
        }

        if ($numeros){
            $caracteres .= $num;
        }

        if ($simbolos){
            $caracteres .= $simb;
        }

        $len = strlen($caracteres);

        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand-1];
        }

        return $retorno;
    }
}