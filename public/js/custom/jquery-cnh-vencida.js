$(function(){
    jQueryCnhVencida.init();
});

let jQueryCnhVencida = (function(){
    let $table = '<table id="tabela-cnh-vencida" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                        +'<th>Nome</th>'
                        +'<th>Contato</th>'
                        +'<th>Vencimento</th>'
                        // +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-lista-cnh-vencida').length) {
                let $setTable = $('.table-lista-cnh-vencida');

                $setTable.empty();

                $setTable.html($table);

                 this.listarColaborador();
            }
        },
        listarColaborador: function(){

            $("#tabela-cnh-vencida").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                ajax: {
                    url: '/dashboard/tabela_cnh_vencida'
                },
                columns: [
                    { data: "pess_nome" },
                    { data: "cont_id" },
                    { data: "pess_vencimento_cnh" },
                    // { data: "action" },
                ]
            });
        },

        pago: function ($event) {
            $.ajax({
                type: "put",
                dataType: "json",
                url: '/dashboard/' + $event.data('colaborador'),
                data:{
                    cnh_vencida: true,
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Salvando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        swal({
                            title: "Pago",
                            text: 'Dados salvos com sucesso.',
                            icon: "success",
                            type: "success"
                        }).then(function(){
                            location.reload();
                        });
                    }else{
                        swal("Pago", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Pago", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        }
    }
})(jQuery);