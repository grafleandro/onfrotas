$(function(){
    jQueryColaborador.init();
});

let jQueryColaborador = (function(){
    let $table = '<table id="tabela-colaborador" class="table table-bordered table-striped">'
        +'<thead class="table-details-thead">'
        +'<tr>'
        +'<th>#</th>'
        +'<th>Nome</th>'
        +'<th>CPF</th>'
        +'<th>Email</th>'
        +'<th>Ação</th>'
        +'</tr>'
        +'</thead>'
        +'<tbody class="table-details-tbody">'

        +'</tbody>'
        +'</table>';

    return{
        init: function(){
            if($('.table-lista-colaborador').length) {
                let $setTable = $('.table-lista-colaborador');

                $setTable.empty();

                $setTable.html($table);

                this.listarColaborador();
            }
        },
        listarColaborador: function(){

            $("#tabela-colaborador").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                ajax: {
                    url: '/colaboradores/tabela'
                },
                columns: [
                    { data: "id" },
                    { data: "name" },
                    { data: "cpf" },
                    { data: "email" },
                    { data: "action" },
                ]
            });
        },
        editarUsuario: function($event){

            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-title').empty();
            $modal.find('.modal-dialog').addClass('modal-lg');

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/colaboradores/' + $event.data('usuario') +'/edit',
                data:{
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Editar Usuário');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form_colaborador" data-title="Atualizar Usuário" data-dismiss="modal" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');


                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/colaboradores/' + $event.data('usuario'));
                    }else{
                        swal("Editar Usuário", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Editar Usuário", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarUsuario: function ($this) {
            if(typeof $this.data('usuario') !== 'undefined' && $this.data('usuario') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/colaboradores/' + $this.data('usuario'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Usuário", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Usuário", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Usuário", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        selecionarColaborador: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/colaboradores",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.colaboradores.length){
                                        response($.map(data.colaboradores, function (item) {
                                            return {
                                                label: item.pess_nome,
                                                value: item.pess_nome,
                                                id: item.pess_id
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item não encontrato',
                                            value: 'Item não encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal($loading_title, data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 5,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#id_pessoa").val(ui.item.id);
                    }
                }
            });
        },

    }
})(jQuery);