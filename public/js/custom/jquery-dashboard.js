let jQueryDashboard = (function(){
    return {
        enviarFelicitacao: function($event){
            if($event.data('contato')){
                let $modal = $("#modal-inf");

                $modal.find('.modal-body').empty();
                $modal.find('.modal-footer').empty();

                $modal.find('.modal-title').append('Enviar Felicitações');

                $modal.find('.modal-body').append('<form id="form-status-os" role="form" action="ordem_servico" method="put">' +
                    '<div class="row">' +
                    '<div class="col-md-12">' +
                    '<label for="obs">Texto</label>' +
                    '<textarea rows="3" class="form-control" id="motivo" name="motivo" required>Ola, nós da NOME_DA_EMPRESA, desejamos Filicidades neste dia. Feliz Aniversário</textarea>'+
                    '</div>'+
                    '<input type="hidden" name="status" id="status" value="'+ $event.val() +'">' +
                    '<input type="hidden" name="orse" id="orse" value="'+ $event.data('orse') +'">' +
                    '<input type="hidden" name="_token" id="_token" value="'+ csrfToken +'">' +
                    '</div>'+
                    '</form>');

                $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>');
                $modal.find('.modal-footer').append('<button type="button" class="btn btn-primary" data-content="#form-status-os" data-title="Alterando Status" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-paper-plane-o"></i> Enviar SMS</button>');

                $modal.modal('show');
            }
        }
    }
})(jQuery);