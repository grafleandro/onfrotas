$(function(){
    jQueryDocVeiculoVencido.init();
});

let jQueryDocVeiculoVencido = (function(){
    let $table = '<table id="tabela-doc-veiculo-vencido" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                        +'<th>Veículo</th>'
                        +'<th>Placa</th>'
                        +'<th>Vencimento</th>'
                        +'<th>Valor</th>'
                        +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-lista-doc-veiculo').length) {
                let $setTable = $('.table-lista-doc-veiculo');

                $setTable.empty();

                $setTable.html($table);

                 this.listarVeiculo();
            }
        },
        listarVeiculo: function(){

            $("#tabela-doc-veiculo-vencido").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                ajax: {
                    url: '/dashboard/tabela_veiculo_doc_vencido'
                },
                columns: [
                    { data: "veic_modelo" },
                    { data: "veic_placa" },
                    { data: "vede_vencimento" },
                    { data: "vede_licenciamento_valor" },
                    { data: "action" },
                ]
            });
        },

        pago: function ($event) {
            $.ajax({
                type: "put",
                dataType: "json",
                url: '/dashboard/' + $event.data('veiculo'),
                data:{
                    doc_veiculo: true,
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Salvando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        swal({
                            title: "Pago",
                            text: 'Dados salvos com sucesso.',
                            icon: "success",
                            type: "success"
                        }).then(function(){
                            location.reload();
                        });
                    }else{
                        swal("Pago", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Pago", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        }
    }
})(jQuery);