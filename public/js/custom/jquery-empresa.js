$(function(){
    jQueryEmpresa.init();
});

let jQueryEmpresa = (function(){
    let $table = '<table id="tabela-empresa" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                        +'<th>Nome</th>'
                        +'<th>Contato</th>'
                        +'<th>CNPJ</th>'
                        +'<th>Qtd. Veículo</th>'
                        +'<th>Mensalidade</th>'
                        +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-lista-empresa').length) {
                let $setTable = $('.table-lista-empresa');

                $setTable.empty();

                $setTable.html($table);

                this.listarEmpresa();
            }
        },
        listarEmpresa: function(){
            $("#tabela-empresa").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                language: {
                    processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                },
                ajax: {
                    url: '/empresa/tabela'
                },
                columns: [
                    { data: "nome_fantazia" },
                    { data: "contato" },
                    { data: "clem_cnpj" },
                    { data: "qtd_veiculo" },
                    { data: "mensalidade" },
                    { data: "action" },
                ]
            });
        }

    }
})(jQuery);