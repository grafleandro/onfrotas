
let jQueryForm = (function () {

    // url base
    let base_url = $("body").data('url');
    
    // constroller
    let $control;
    
    // metodo da controller
    let $method;
    
    // area para informação de carregamento
    let $loading;
    
    // texto de carregamento
    let $loading_text;
    
    // area onde será inserido o conteudo
    let $target_content;
    
    // url completa para a requisicao
    let $url;
    
    let $form;
    /* Passa um referencia dos dados que deveram ser salvos */
    let $content = null;
    /* Variaveis de comparacoes */
    let _KEYWORD    = "keyword";
    let _FORM_ADV   = "form-adv";
    
    return {
        send_form: function($this) {
                        
            let $refresh_content = $this.data('refresh');
            let $text_action = $this.html();
            
            let $loading = $this.data('loading');
            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }
            
            if ($form[0]) {
                
                // dados que serão enviados para o metodo
                let $dados_form = $form.serialize();

                if(jQueryForm.validateInput($($form[0]).find(':input'), $loading_title)){
                    return;
                }

                // action do form
                let $url = $form.attr('action');

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';

                if (typeof $url !== 'undefined') {
                    
                    $.ajax({
                        type: $method, //tipo que esta sendo enviado post ou get
                        data: $dados_form,//caonteudo do form
                        url: $url, //endereço para onde vai
                        dataType: 'json',//tipo do retorno
                        beforeSend: function () {
                        	loading.show($loading_title, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                            $this.html($text_action);

                            loading.hide();

                            if(data.success){
                                swal({
                                    title: $loading_title,
                                    text: 'Dados salvos com sucesso.',
                                    icon: "success",
                                    type: "success"
                                }).then(function(){
                                    location.reload();
                                });
                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                       },
                       error: function (xhr) {
                           loading.hide();

                           swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                       }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "danger"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }
            
        },
        send_form_file: function($this) {                        
            let $refresh_content;
            let $text_action = $this.html();
                        
            let $loading_text = $this.data('loading-text');
            let $loading_title = $this.data('title');

            if(typeof $this.data('content') !== undefined && $this.data('content')){
                let formContent = $this.data('content');

                $form = $(formContent);
            }else{
                // localizar form se tiver
                $form = $this.closest('form');
            }
                        
            if ($form[0]) {

                let $serialize = $form.serializeArray();

                let $inputfile = $form.find('input[type="file"]');
                let $dados_form = new FormData($form);
                
                $refresh_content = $form.find($this.data('refresh'));
                $loading = $form.find($this.data('loading'));
                
                $.each($inputfile, function(k,v){
                    let files_input = $(this);

                    $.each(files_input[0].files, function(key, file) {
                        $dados_form.append(key, file);
                    });
                });

                $.each($serialize, function(k,v){
                    $dados_form.append(v.name, v.value);
                });

                /* Metodo de Envio */
                let $method = (typeof $form.attr('method') !== 'undefined') ? $form.attr('method') : 'POST';
                
                /* action do form */
                let $url = $form.attr('action');
                
                if (typeof $url != 'undefined') {
                    
                    $.ajax({
                        type: $method,
                        data: $dados_form,
                        url: $url,
                        cache: false,
                        dataType: 'json',
                        processData: false, // Don't process the files
                        contentType: false, // Set content type to false as jQuery
                        headers: { 'X-CSRF-TOKEN': csrfToken },
                        beforeSend: function () {
                        	loading.show($loading_text, {dialogSize: 'sm'});
                        },
                        success: function (data) {
                        	 $this.html($text_action);
                             
                             loading.hide();

                            if(data.success){
                                swal({
                                    title: $loading_title,
                                    text: 'Dados salvos com sucesso.',
                                    icon: "success",
                                    type: "success"
                                }).then(function(){
                                    location.reload();
                                });
                            }else{
                                swal($loading_title, data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                        }
                    });
                } else {
                    swal("Erro", "Url não informada", {icon: "danger"});
                }
            } else {
                //console.log('Nenhum form foi encontrado!');
            }
            
        },
        validateInput: function($inputs, $title){
            let status = false;

            $.each($inputs, function(index, input){
                /* Capturando dados do Input */
                let formGroup = $(input).closest('.form-group');
                let label = formGroup.find('label');

                if(input.required && !$(input).val().length){
                    /* Ativando alertas */
                    formGroup.addClass('has-error');

                    if(!formGroup.find('.help-block').length){
                        formGroup.append('<span class="help-block">Campo Obrigatório!</span>');
                    }

                    status = true;
                }else if(input.required && $(input).val().length && formGroup.hasClass('has-error')){
                    label.prepend('<i class="fa fa-check"></i> ');
                    formGroup.removeClass('has-error').addClass('has-success');
                    formGroup.find('.help-block').remove();
                }
            });

            if(status){
                swal($title, 'Você possui campos que precisam ser preenchidos.', {icon: "warning"});
            }

            return status;
        },
        imprimir:function ($this) {

            if(typeof $this.data('imprimi') !== 'undefined' && $this.data('imprimi') !== '' && typeof $this.data('url') !== 'undefined' && $this.data('url') !== '' ){

                $url = $this.data('url');

                let $modal = $("#modal-inf");

                $('.modal-body').empty();
                $('.modal-footer').empty();

                $modal.find('.modal-dialog').addClass('modal-lg');

                $modal.find('.modal-title').text('Visualização');

                $modal.find('.modal-body').append('<embed src="'+$url+'?ordem_servico=' + $this.data('imprimi')+ '&_token='+csrfToken+'"type="application/pdf" width="100%" height="600px">')

                $modal.modal('show');
            }
        },

    };
})(jQuery);