$(function(){
    jQueryFrota.init();
});

let jQueryFrota = (function(){
    let $table = '<table id="tabela-frota" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                            +'<th>#</th>'
                            +'<th>Unidade</th>'
                            +'<th>Gerente Responsável</th>'
                            +'<th>Código Frota</th>'
                            +'<th>Quantidade de Veículos</th>'
                            +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    let _VEICULOS = [];

    return{
        init: function(){
            if($('.table-lista-frota').length) {
                let $setTable = $('.table-lista-frota');

                $setTable.empty();

                $setTable.html($table);

                this.listarFrota($setTable);
            }
        },
        listarFrota: function($setTable){
            $("#tabela-frota").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                language: {
                    processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                },
                ajax: { url: '/frotas/tabela' },
                columns: [
                    { data: "frot_id" },
                    { data: "unid_id" },
                    { data: "pess_id" },
                    { data: "frot_cod_interno" },
                    { data: "qtd_veiculo" },
                    { data: "action" },
                ]
            });
        },
        proximoStapVeiculo: function($event, $activeStap = false){
            let parent = this;

            $("#frota_menu").removeClass('active');
            $("#frota_veiculo_menu").addClass('active');

            $("#frota_view").removeClass('active');
            $("#frota_veiculo_view").addClass('active');
        },
        salvarVeiculo: function($event){

            /* Metodo de Envio */
            let $method = $("#form-frotas").attr('method');
            let $url    = $("#form-frotas").attr('action');

            $.ajax({
                type: $method,
                dataType: 'json',
                url: $url,
                beforeSend: function(){
                    loading.show('Salvando Dados...', {dialogSize: 'sm'});
                },
                data: {
                    frota: $("#id_frota").val(),
                    cod_frota: $("#cod_frota").val(),
                    id_unidade: $("#id_unidade").val(),
                    id_pessoa: $("#id_pessoa").val(),
                    veiculos: _VEICULOS,
                    _token: csrfToken
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        swal({
                            title: 'Cadastro de Frota',
                            text: 'Dados salvos com sucesso.',
                            icon: "success",
                            type: "success"
                        }).then(function(){
                            location.reload();
                        });
                    }else{
                        swal('Cadastro de Frota', data.alert, {icon: "warning"});
                    }
                },
                error: function(xhr){
                    loading.hide();

                    console.log(xhr);
                }
            });
        },
        deletarFrota: function($event){
            if(typeof $event.data('frota') !== 'undefined' && $event.data('frota') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/frotas/' + $event.data('frota'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Frota", "Item excluido com sucesso!", {icon: "success"});

                                    $event.closest('tr').remove();
                                }else{
                                    swal("Excluir Frota", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Frota", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        editarFrota: function($event){
            location.href = '/frotas/' + $event.data('frota') + '/edit';
        },
        addVeiculoVetor: function(idVeiculo){
            _VEICULOS.push({id: idVeiculo});
        },
        selecionarTodosVeiculo: function($event){
            /* Seleciona todas as linhas que possui na tabela */
            $.each($("#tabela-veiculo").find('tbody tr'), function(index, rowTable){
                /* Seleciona o BOTAO 'Selecionar' e simula o click do botao */
                $(rowTable).find('.selecionar').trigger('click');
            });
        },
        selecionarVeiculo: function($event){
            if($event.find('.fa-square-o').length){
                $event.find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square-o');

                _VEICULOS.push({id: $event.data('veiculo')});

                console.log(_VEICULOS);
            }else{
                $event.find('.fa-check-square-o').removeClass('fa-check-square-o').addClass('fa-square-o');

                _VEICULOS = $.grep(_VEICULOS, function(value){
                    return value.id !== $event.data('veiculo');
                });
            }
        },
        addNumeroVeiculo: function($event){
            swal({
                title: "Número do Veículo",
                content: {
                    element: "input",
                    attributes: {
                        placeholder: "Informar Número do Veículo",
                        type: "text",
                    },
                },
            });
        },
        addMotoristaVeiculo: function($event){
            swal({
                title: "Adicionar Motorista",
                content: {
                    element: "input",
                    attributes: [{
                            placeholder: "Adicionar motorista ao Veículo",
                            type: "text",
                        },
                    ],
                },
            }).then((data) => {
                console.log(data, $event.data('veiculo'));
            });
        }
    }
})(jQuery);