$(function(){

});

let jQueryHistoricoManutencao = (function(){
    let $table = '<table id="tabela-historico-veiculo" class="table table-bordered table-striped">'
        +'<thead class="table-details-thead">'
        +'<tr>'
        +'<th>#</th>'
        +'<th>Serviço</th>'
        +'<th>Peças</th>'
        +'<th>Valor</th>'
        +'<th>Data</th>'
        +'</tr>'
        +'</thead>'
        +'<tbody class="table-details-tbody">'

        +'</tbody>'
        +'</table>';

    return{
        init: function(){
            if($('.table-lista-ordem_servico').length) {
                let $setTable = $('.table-lista-ordem_servico');

                $setTable.empty();

                $setTable.html($table);

                this.listarHistoricoManutencao($setTable);

                $('.imprimir').removeAttr('hidden');
            }
        },
        listarHistoricoManutencao: function($setTable){

            $("#tabela-historico-veiculo").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                language: {
                    processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                },
                ajax: {
                    url: '/relatorio/tabela_manutencao',
                    data:{
                        oficina:$("#id_oficina").val(),
                        tipo_os:$("#tipo_ordem_servico").val(),
                        dt_inicio:$("#dt_inicio").val(),
                        dt_final:$("#dt_final").val(),
                    }
                },
                columns: [
                    { data: "orse_id" },
                    { data: "orse_desc_servico" },
                    { data: "orse_desc_pecas" },
                    { data: "orse_valor" },
                    { data: "created_at" },
                ]
            });
        },

        imprimirManutencao:function ($this) {

                $url = $this.data('url');

                let $modal = $("#modal-inf");

                $('.modal-body').empty();
                $('.modal-footer').empty();

                $modal.find('.modal-dialog').addClass('modal-lg');

                $modal.find('.modal-title').text('Visualização');

                $modal.find('.modal-body').append('<embed src="'+$url+'?oficina='
                    + $("#id_oficina").val()+ '&tipo_os='+$("#tipo_ordem_servico").val()+
                    '&dt_inicio='+$("#dt_inicio").val()+ '&dt_final='+$("#dt_final").val()
                    +'&_token='+csrfToken+'"type="application/pdf" width="100%" height="600px">');

                $modal.modal('show');


        },
    }
})(jQuery);