$(function(){

});

let jQueryHistoricoVeiculo = (function(){
    let $table = '<table id="tabela-historico-veiculo" class="table table-bordered table-striped">'
        +'<thead class="table-details-thead">'
        +'<tr>'
        +'<th>#</th>'
        +'<th>Serviço</th>'
        +'<th>Peças</th>'
        +'<th>Valor</th>'
        +'<th>Data</th>'
        +'</tr>'
        +'</thead>'
        +'<tbody class="table-details-tbody">'

        +'</tbody>'
        +'</table>';

    return{
        init: function(){
            if($('.table-lista-ordem_servico').length) {
                let $setTable = $('.table-lista-ordem_servico');

                $setTable.empty();

                $setTable.html($table);

                this.listarHistoricoVeiculo($setTable);

                $('.imprimir').removeAttr('hidden');
                $('#imprimir').attr("data-imprimi", $("#id_veiculo").val())
            }
        },
        listarHistoricoVeiculo: function($setTable){

            $("#tabela-historico-veiculo").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                language: {
                    processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                },
                ajax: {
                    url: '/relatorio/tabela_veiculo',
                    data:{
                        veiculo:$("#id_veiculo").val(),
                    }
                },
                columns: [
                    { data: "orse_id" },
                    { data: "orse_desc_servico" },
                    { data: "orse_desc_pecas" },
                    { data: "orse_valor" },
                    { data: "created_at" },
                ]
            });
        },
    }
})(jQuery);