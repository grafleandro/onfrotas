$(".mask-cpf").focusout(function(){
    var element;
    element = $(this);
    element.unmask();
    cpf_cnpj = element.val().replace(/\D/g, '');

    element.mask("999.999.999-99");

}).trigger('focusout');

$(".mask-cnpj").focusout(function(){
    var element;
    element = $(this);
    element.unmask();
    cpf_cnpj = element.val().replace(/\D/g, '');

    element.mask("99.999.999/99?99-99");

}).trigger('focusout');

$(".telephone").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

$(".cell_phone_1").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

$(".cell_phone_2").focusout(function(){
    var phone, element;
    element = $(this);
    element.unmask();
    phone = element.val().replace(/\D/g, '');
    if(phone.length > 10) {
        element.mask("(99) 99999-999?9");
    } else {
        element.mask("(99) 9999-9999?9");
    }
}).trigger('focusout');

$(".mask-integer").keyup(function(){
    let integer = $(this).val().replace(/[^\d]/,'');
    $(this).val(integer);
});

if(typeof $(".mask-money").maskMoney !== "undefined"){
    $(".mask-money").maskMoney({
        prefix: 'R$ ',
        decimal: ',',
        thousands: '.'
    });
}

