$(function(){
    jQueryMecanica.init();
});

let jQueryMecanica = (function(){
    let $table = '<table id="tabela-mecanica" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                        +'<th>#</th>'
                        +'<th>Nome</th>'
                        +'<th>Telefone</th>'
                        +'<th>Reponsavel</th>'
                        +'<th>Especializada</th>'
                        +'<th>Ext/Int</th>'
                        +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-lista-mecanica').length) {
                let $setTable = $('.table-lista-mecanica');

                $setTable.empty();

                $setTable.html($table);

                 this.listarMecanica();
            }
        },
        listarMecanica: function(){

            $("#tabela-mecanica").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                ajax: {
                    url: '/mecanicas/tabela'
                },
                columns: [
                    { data: "ofic_id" },
                    { data: "ofic_titulo" },
                    { data: "cont_id" },
                    { data: "ofic_responsavel" },
                    { data: "ofic_especializacao" },
                    { data: "ofic_interna_externa" },
                    { data: "action" },
                ]
            });
        },
        editarMecanica: function($event){

            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-title').empty();

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/mecanicas/' + $event.data('mecanica') +'/edit',
                data:{
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Editar Mecânica');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form_mecanica" data-title="Atualizar Mecânica" data-dismiss="modal" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');


                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/mecanicas/' + $event.data('mecanica'));
                    }else{
                        swal("Editar Mecânica", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Editar Mecânica", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarMecanica: function ($this) {
            if(typeof $this.data('mecanica') !== 'undefined' && $this.data('mecanica') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/mecanicas/' + $this.data('mecanica'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Mecânica", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Mecânica", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Mecânica", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        selecionarMecanica: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/mecanicas",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.oficina.length){
                                        response($.map(data.oficina, function (item) {
                                            return {
                                                label: item.ofic_titulo +' - '+ item.ofic_responsavel,
                                                value: item.ofic_titulo,
                                                id: item.ofic_id
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item não encontrato',
                                            value: 'Item não encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal($loading_title, data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 5,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#id_oficina").val(ui.item.id);
                    }
                }
            });
        },
    }
})(jQuery);