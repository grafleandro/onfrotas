$(function(){
    jQueryOrdemServico.init();
});

$('#status').on('change', function() {
    jQueryOrdemServico.init();
});

let jQueryOrdemServico = (function(){
    let _VEICULOS = [];

    let _STATUS_CANCELAR    = 3;
    let _STATUS_AGUARDANDO  = 5;

    let statusAnterior = null;

    let $table = '<table id="tabela-ordem-servico" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                            +'<th>#</th>'
                            +'<th>Veículo</th>'
                            +'<th>Tipo da Manutenção</th>'
                            +'<th>Prioridade</th>'
                            +'<th>Status</th>'
                            +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    return {
        init: function(){
            if($('.table-lista-ordem-servico').length) {
                let $setTable = $('.table-lista-ordem-servico');

                $setTable.empty();

                $setTable.html($table);

                this.listarOrdemServico($setTable);
            }
        },
        listarOrdemServico: function($setTable){

            $("#tabela-ordem-servico").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                language: {
                    processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                },
                ajax: {
                    url: '/ordem_servico/tabela',
                    data:{
                        _token: csrfToken,
                        status: $('#status').val(),
                    },
                },
                columns: [
                    { data: "orse_id" },
                    { data: "veiculo" },
                    { data: "manutencao_tipo" },
                    { data: "manutencao_prioriddade" },
                    { data: "status" },
                    { data: "action" },
                ]
            });
        },
        carregarVeiculos: function($event){
            let $modal = $("#modal-inf");

            $.ajax({
                type: "get",
                dataType: "json",
                url: '/veiculos/tabela',
                data:{
                    _token: csrfToken,
                    tipo: $event.data('tipo')
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Selecionar Veículo');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form-veiculo" data-title="Atualizar Veículo" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');

                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/veiculos/' + $event.data('veiculo'));
                    }else{
                        swal("Selecionar Veículo", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Selecionar Veículo", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        selecionarVeiculo: function($event){
            if($event.find('.fa-square-o').length){
                $event.find('.fa-square-o').removeClass('fa-square-o').addClass('fa-check-square-o');

                _VEICULOS.push({id: $event.data('veiculo')});
            }else{
                $event.find('.fa-check-square-o').removeClass('fa-check-square-o').addClass('fa-square-o');

                _VEICULOS = $.grep(_VEICULOS, function(value){
                    return value.id !== $event.data('veiculo');
                });
            }
        },
        selecionarMenu: function($event){
            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();

            $.ajax({
                type: "get",
                dataType: "json",
                url: '/ordem_servico/menus',
                data:{
                    _token: csrfToken,
                    ordem_servico: $event.data('os')
                },
                beforeSend: function () {
                    loading.show('Selecionando Menus...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Ações');

                        $modal.find('.modal-body').html(data.menus);

                        $modal.modal('show');
                    }else{
                        swal("Selecionar Veículo", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Selecionar Veículo", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        editarOrdemServico: function($event){
            location.href = '/ordem_servico/' + $event.data('os') + '/edit';
        },
        visualizarOrdemServico: function($event){
            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-title').empty();

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/ordem_servico/view',
                data:{
                    _token: csrfToken,
                    ordem_servico: $event.data('os'),
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-dialog').addClass('modal-lg');

                        $modal.find('.modal-title').append('Visualizar Ordem de Serviço');

                        $modal.find('.modal-body').html(data.view);

                        $modal.modal('show');

                        $('.box-title').remove();
                        $('.box-footer').remove();
                        $('textarea').attr('disabled', 'disabled');
                        $('#unidade').attr('disabled', 'disabled');
                        $('#oficina').attr('disabled', 'disabled');
                        $('#veiculo').attr('disabled', 'disabled');
                        $('#ordem_servico_valor').attr('disabled', 'disabled');
                        $('#ordem_servico_executar').attr('disabled', 'disabled');
                        $('#prioridade_manutencao').attr('disabled', 'disabled');
                        $('#tipo_manutencao').attr('disabled', 'disabled');

                    }else{
                        swal("Visualizar Ordem de Serviço", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Visualizar Ordem de Serviço", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarOrdemServico: function($this){
            if(typeof $this.data('os') !== 'undefined' && $this.data('os') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/ordem_servico/' + $this.data('os'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Ordem de Servico", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Ordem de Servico", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Ordem de Servico", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        imprimir_os:function ($this) {

            if(typeof $this.data('imprimi') !== 'undefined' && $this.data('imprimi') !== '' && typeof $this.data('url') !== 'undefined' && $this.data('url') !== '' ){

                $url = $this.data('url');

                let $modal = $("#modal-inf");

                $('.modal-body').empty();
                $('.modal-footer').empty();

                $modal.find('.modal-dialog').addClass('modal-lg');

                $modal.find('.modal-title').text('Visualização');

                $modal.find('.modal-body').append('<embed src="'+$url+'?ordem_servico=' + $this.data('imprimi')+ '&_token='+csrfToken+'"type="application/pdf" width="100%" height="600px">')

                $modal.modal('show');
            }
        },
        alterarStatus: function($event){

            if($event.val() == _STATUS_CANCELAR || $event.val() == _STATUS_AGUARDANDO){
                let $modal = $("#modal-inf");

                $modal.find('.modal-body').empty();
                $modal.find('.modal-footer').empty();

                $modal.find('.modal-title').append('Justificativa');

                if($event.val() == _STATUS_CANCELAR){
                    $modal.find('.modal-body').append('<p>Para o Cancelamento da Ordem de Serviço, é necessário que informe o motivo:</p>');
                } else if($event.val() == _STATUS_AGUARDANDO){
                    $modal.find('.modal-body').append('<p>Para AGUARDANDO, é necessário que informe o motivo:</p>');
                }

                $modal.find('.modal-body').append('<form id="form-status-os" role="form" action="/ordem_servico/'+ $event.data("orse") +'" method="put">' +
                                                    '<div class="row">' +
                                                        '<div class="col-md-12">' +
                                                            '<label for="obs">Motivo</label>' +
                                                            '<textarea rows="3" class="form-control" id="motivo" name="motivo" required></textarea>'+
                                                        '</div>'+
                                                        '<input type="hidden" name="status" id="status" value="'+ $event.val() +'">' +
                                                        '<input type="hidden" name="orse" id="orse" value="'+ $event.data('orse') +'">' +
                                                        '<input type="hidden" name="_token" id="_token" value="'+ csrfToken +'">' +
                                                    '</div>'+
                                                '</form>');

                $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>');
                $modal.find('.modal-footer').append('<button type="button" class="btn btn-primary" data-content="#form-status-os" data-title="Alterando Status" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                $modal.modal('show');

                $modal.on('hide.bs.modal', function () {
                    if(statusAnterior){
                        $event.val(statusAnterior);
                    }
                });
            } else {
                $.ajax({
                    type: 'put',
                    dataType: 'json',
                    url: '/ordem_servico/' + $event.data('orse'),
                    data: {
                        status: $event.val(),
                        orse: $event.data('orse'),
                        _token: csrfToken
                    },
                    success: function(data){
                        if(data.success){
                            swal('Atualizando Status', 'Status atualizado com sucesso.', {icon: "success"});
                        }else{
                            console.log('Erro: Atualizar Status -> ' + data.alert);
                        }
                    },
                    error: function(xhr){
                        console.log(xhr);
                    }
                });
            }
        },
        statusAnterior: function($event){
            statusAnterior = $event.val()
        },
        visualizarInformacao: function($event){
            $.ajax({
                type: 'get',
                dataType: 'json',
                url: '/ordem_servico',
                data: {
                    orse: $event.data('os'),
                    tipo: 'info'
                },
                success: function(data){
                    if(data.success && data.historico.length){
                        let $modal = $("#modal-inf");

                        $modal.find('.modal-body').empty();
                        $modal.find('.modal-footer').empty();

                        $modal.find('.modal-title').text('Histórico');

                        $.each(data.historico, function(index, value){
                            $modal.find('.modal-body').append('<p><strong>Data e Hora: </strong>'+ value.created_at +'</p>');
                            $modal.find('.modal-body').append('<p><strong>Status: </strong><span class="badge bg-'+ value.os_status.sose_btn_cor +'">'+ value.os_status.sose_titulo +'</span></p>');
                            $modal.find('.modal-body').append('<p><strong>Motivo: </strong>'+ value.ossh_motivo+'</p>');
                            $modal.find('.modal-body').append('<hr>');
                        });

                        $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Fechar</button>');

                        $modal.modal('show');
                    }
                },
                error: function (xhr) {
                    console.log(xhr);
                }
            })
        }
    }
})(jQuery);