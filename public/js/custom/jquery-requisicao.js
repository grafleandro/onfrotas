$(function(){
    // jQueryRequisicao.init();
});

let jQueryRequisicao = (function(){
    let $table = '<table id="tabela-requisicao" class="table table-bordered table-striped">'
        +'<thead class="table-details-thead">'
        +'<tr>'
        +'<th>#</th>'
        +'<th>Tipo</th>'
        +'<th>Data</th>'
        +'<th>Veículo</th>'
        +'<th>Descrição</th>'
        +'<th>Qtd.</th>'
        +'<th>Valor</th>'
        +'<th>Ação</th>'
        +'</tr>'
        +'</thead>'
        +'<tbody class="table-details-tbody">'

        +'</tbody>'
        +'</table>';

    return{
        init: function(){
            if($('.table-lista-requisicao').length) {
                let $setTable = $('.table-lista-requisicao');

                $setTable.empty();

                $setTable.html($table);

                 this.listarRequisicao();
            }
        },
        listarRequisicao: function(){

            $("#tabela-requisicao").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                ajax: {
                    url: '/requisicao/tabela',
                    data:{
                        veiculo:$("#id_veiculo").val(),
                        tipo_requisicao:$("#tipo_requisicao").val(),
                        dt_inicio:$("#dt_inicio").val(),
                        dt_final:$("#dt_final").val(),
                    }
                },
                columns: [
                    { data: "requ_id" },
                    { data: "requ_tipo_requisicao" },
                    { data: "created_at" },
                    { data: "veic_id" },
                    { data: "requ_descricao" },
                    { data: "requ_quantidade" },
                    { data: "requ_valor" },
                    { data: "action" },
                ]
            });
        },
        editarRequisicao: function($event){

            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-title').empty();

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/requisicao/' + $event.data('requisicao') +'/edit',
                data:{
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Editar Requisição');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form_requisicao" data-title="Salvando Requisição" data-dismiss="modal" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');


                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/requisicao/' + $event.data('requisicao'));
                    }else{
                        swal("Editar Requisição", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Editar Requisição", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        reset: function ($this) {

            $("#id_veiculo").val('');

            let $setTable = $('.table-lista-requisicao');

            $setTable.empty();

        },
        deletarRequisicao: function ($this) {
            if(typeof $this.data('requisicao') !== 'undefined' && $this.data('requisicao') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/requisicao/' + $this.data('requisicao'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Mecânica", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Mecânica", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Mecânica", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
    }
})(jQuery);