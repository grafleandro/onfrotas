let jQuerySimulador = (function(){
    let _BASICO         = 25;
    let _INTERMEDIARIO  = 49;
    let _AVANCADO       = 50;

    /** VALORES */
    let _VALOR_BASICO   = 9;
    let _VALOR_INTERMEDIARIO = 7.5;

    return{
        simulador: function($event){
            // localizar form se tiver
            let $form = $event.closest('form');

            if(jQueryForm.validateInput($($form[0]).find(':input'), 'Simulador')){
                return;
            }

            /* Informacoes do Usuario */
            let contato = $("#contato").val();
            let qtdVeiculo = $("#qtd-veiculo").val();
            let valorTotal      = 0;

            /* Calcular por Tipo de Plano */
            if(qtdVeiculo <= _BASICO){
                valorTotal = (qtdVeiculo*_VALOR_BASICO)
            } else if(qtdVeiculo > _BASICO || qtdVeiculo <= _INTERMEDIARIO){
                valorTotal = (qtdVeiculo*_VALOR_INTERMEDIARIO)
            } else if(qtdVeiculo >= _AVANCADO){
                valorTotal = ((qtdVeiculo*_AVANCADO)+(qtdVeiculo-_AVANCADO)*_VALOR_INTERMEDIARIO);
            }

            /* Valor por Periodo */
            let valorMensal     = valorTotal;
            let valorTrimestral = (valorTotal-(valorTotal*0.1))*3;
            let valorSemestral  = (valorTotal-(valorTotal*0.15))*6;
            let valorAnual      = (valorTotal-(valorTotal*0.2))*12;


            let valorTriEconomia    = ((valorTotal*3)-valorTrimestral);
            let valorSemEconomia    = ((valorTotal*6)-valorSemestral);
            let valorAnualEconomia  = ((valorTotal*12)-valorAnual);

            /* Inicializando os valores dos planos */
            $("#valor-mensal").text(valorMensal.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
            $("#valor-tri").text(valorTrimestral.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
            $("#valor-sem").text(valorSemestral.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
            $("#valor-anual").text(valorAnual.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));

            $("#economia-tri").text(valorTriEconomia.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
            $("#economia-sem").text(valorSemEconomia.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));
            $("#economia-anual").text(valorAnualEconomia.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }));

            $(".pricing-simulator").removeClass('hidden');
        },
        preencherForm: function ($event) {
            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();

            // $modal.find('.modal-content').addClass('modal-lg');
            $modal.find('.modal-title').text('Solicitar Acesso');

            $modal.find('.modal-body').html('<form id="form-simulador" role="form" action="/simulador" method="post">' +
                                                '<div class="row">' +
                                                    '<div class="col-md-12">' +
                                                        '<label for="razao_social">Razão Social</label>' +
                                                        '<input type="text" class="form-control" name="razao_social" id="razao_social">'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="row">' +
                                                    '<div class="col-md-6">' +
                                                        '<label for="email">Email</label>' +
                                                        '<input type="text" class="form-control" name="email" id="email">'+
                                                    '</div>'+
                                                    '<div class="col-md-6">' +
                                                        '<label for="contato-cel">Contato</label>' +
                                                        '<input type="text" class="form-control cell_phone_1" name="contato-cel" id="contato-cel" value="'+ $("#contato").val() +'">'+
                                                    '</div>'+
                                                    '<input type="hidden" name="_token" id="_token" value="'+ csrfToken +'">' +
                                                '</div>'+
                                                '<div class="row">' +
                                                    '<div class="col-md-6">' +
                                                        '<label for="cnpj">CNPJ</label>' +
                                                        '<input type="text" class="form-control mask-cnpj" name="cnpj" id="cnpj">'+
                                                    '</div>'+
                                                    '<div class="col-md-6">' +
                                                        '<label for="qtd_veiculo">Qtd. Veículos</label>' +
                                                        '<input type="text" class="form-control" name="qtd_veiculo" id="qtd_veiculo" value="'+ $("#qtd-veiculo").val() +'">'+
                                                    '</div>'+
                                                '</div>'+
                                                '<input type="hidden" class="form-control" name="tipo" id="tipo" value="'+ $event.data('tipo') +'">'+
                                            '</form>');

            $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>');
            $modal.find('.modal-footer').append('<button class="btn btn-primary" data-content="#form-simulador" data-title="Solicitar Acesso" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-location-arrow"></i> Enviar</button>');

            $modal.modal('show');

            $(".mask-cnpj").focusout(function(){
                var element;
                element = $(this);
                element.unmask();
                cpf_cnpj = element.val().replace(/\D/g, '');

                element.mask("99.999.999/99?99-99");

            }).trigger('focusout');

            $(".cell_phone_1").focusout(function(){
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if(phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                } else {
                    element.mask("(99) 9999-9999?9");
                }
            }).trigger('focusout');
        }
    }
})(jQuery);