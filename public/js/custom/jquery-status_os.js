$(function(){
    jQueryStatus.init();
});

let jQueryStatus = (function(){
    let $table = '<table id="tabela-status" class="table table-bordered table-striped">'
        +'<thead class="table-details-thead">'
        +'<tr>'
        +'<th>#</th>'
        +'<th>Titulo</th>'
        +'<th>Abreviação</th>'
        +'<th>Descrição</th>'
        +'<th>Ação</th>'
        +'</tr>'
        +'</thead>'
        +'<tbody class="table-details-tbody">'

        +'</tbody>'
        +'</table>';

    return{
        init: function(){
            if($('.table-lista-status').length) {
                let $setTable = $('.table-lista-status');

                $setTable.empty();

                $setTable.html($table);

                this.listarStatus();
            }
        },
        listarStatus: function(){

            $("#tabela-status").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                processing: true,
                language: {
                    processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                },
                ajax: {
                    url: '/ordem_servico_status/tabela'
                },
                columns: [
                    { data: "sose_id" },
                    { data: "sose_titulo" },
                    { data: "sose_abreviacao" },
                    { data: "sose_descricao" },
                    { data: "action" },
                ]
            });
        },
        editarStatus: function($event){

            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-title').empty();
            $modal.find('.modal-dialog').addClass('modal-lg');

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/ordem_servico_status/' + $event.data('status') +'/edit',
                data:{
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Editar Status');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form_status" data-title="Atualizar Status" data-dismiss="modal" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');


                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/ordem_servico_status/' + $event.data('status'));
                    }else{
                        swal("Editar Usuário", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Editar Usuário", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarStatus: function ($this) {
            if(typeof $this.data('status') !== 'undefined' && $this.data('status') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/ordem_servico_status/' + $this.data('status'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Status", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Status", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Status", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },

    }
})(jQuery);