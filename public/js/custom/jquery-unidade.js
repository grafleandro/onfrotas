$(function(){
    jQueryUnidade.init();
});

let jQueryUnidade = (function(){
    let $table = '<table id="tabela-unidade" class="table table-bordered table-striped">'
        +'<thead class="table-details-thead">'
        +'<tr>'
        +'<th>#</th>'
        +'<th>Nome</th>'
        +'<th>Razão Social</th>'
        +'<th>CNPJ</th>'
        +'<th>Ação</th>'
        +'</tr>'
        +'</thead>'
        +'<tbody class="table-details-tbody">'

        +'</tbody>'
        +'</table>';

    return{
        init: function(){
            if($('.table-lista-unidade').length) {
                let $setTable = $('.table-lista-unidade');

                $setTable.empty();

                $setTable.html($table);

                this.listarUnidades();
            }
        },
        listarUnidades: function(){

            $("#tabela-unidade").dataTable({
                destroy: true,
                searching: true,
                serverSide: true,
                ajax: {
                    url: '/unidade/tabela'
                },
                columns: [
                    { data: "unid_id" },
                    { data: "unid_titulo" },
                    { data: "unid_razao_social" },
                    { data: "unid_cnpj" },
                    { data: "action" },
                ]
            });
        },
        editarUnidade: function($event){

            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-title').empty();

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/unidade/' + $event.data('unidade') +'/edit',
                data:{
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-title').append('Editar Unidade');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form_unidade" data-title="Atualizar Unidade" data-dismiss="modal" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');

                        $('#tipo_logradouro').val(data.tipo_logradouro);
                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/unidade/' + $event.data('unidade'));
                    }else{
                        swal("Editar Unidade", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Editar Unidade", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarUnidade: function ($this) {
            if(typeof $this.data('unidade') !== 'undefined' && $this.data('unidade') !== ''){

                $.ajax({
                    type: "delete",
                    dataType: "json",
                    url: '/unidade/' + $this.data('unidade'),
                    data:{
                        _token: csrfToken
                    },
                    beforeSend: function () {
                        loading.show('Excluindo dados...', {dialogSize: 'sm'});
                    },
                    success: function(data){
                        loading.hide();
                        if(data.success){
                            swal({title: 'Exclusão de Unidade', text: 'Unidade excluida com sucesso.', type:
                                "success"}).then(function(){
                                    location.reload();
                                }
                            );
                        }else{
                            swal('Exclusão de Cadastro', data.alert, {icon: "warning"});
                        }
                    },
                    error: function (xhr) {
                        loading.hide();
                        swal('Exclusão de Unidade', data.alert, {icon: "warning"});
                    }
                });
            }
        },
        selecionarUnidade: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/unidade",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.unidades.length){
                                        response($.map(data.unidades, function (item) {
                                            return {
                                                label: item.unid_titulo,
                                                value: item.unid_titulo,
                                                id: item.unid_id
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item não encontrato',
                                            value: 'Item não encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal('Selecionar Unidade', data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal('Selecionar Unidade', "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 5,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id){
                        $("#id_unidade").val(ui.item.id);
                    }
                }
            });
        },

    }
})(jQuery);