var jQueryUploadFile = (function($){
    var $modal = $("#modal-inf");

    return{
        uploadFile: function($this){
            /* Preparando Modal */
            $modal.find('.modal-footer').empty();
            $modal.find('.modal-body').empty();

            $modal.find('.modal-title').text('Anexar Arquivo(s)');

            if($modal.find('.modal-dialog').hasClass('modal-lg')){
                $modal.find('.modal-dialog').removeClass('modal-lg');
            }

            body = '<form id="form-upload-file" action="'+ $this.data('url') +'" method="post">'+
                        '<div class="form-group">'+
                            '<label for="titulo_arquivos">Título</label>'+
                            '<input type="text" class="form-control" id="titulo_arquivos" name="titulo_arquivos">'+
                        '</div>'+
                        '<div class="form-group">'+
                            '<label for="empenho">'+ $this.text() +'</label>'+
                            '<input type="file" class="form-control" id="arquivos" name="arquivos" multiple>'+
                        '</div>'+
                        '<input type="hidden" id="referencia" name="referencia" value="'+ $this.data('referencia') +'">'+
                    '</form>';

            $modal.find('.modal-body').html(body);

            $modal.find('.modal-footer').append('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancelar</button>');
            $modal.find('.modal-footer').append('<button class="btn btn-primary" data-content="#form-upload-file" data-title="Arquivo(s)" data-loading-text="Salvando Arquivo(s)..." data-dismiss="modal" onclick="jQueryForm.send_form_file($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

            $modal.modal('show');
        },
        visualizarArquivo: function($this){
            let $modal = $("#modal-inf");

            $('.modal-body').empty();
            $('.modal-footer').empty();

            $modal.find('.modal-dialog').addClass('modal-lg');

            $modal.find('.modal-title').text('Visualização');

            $modal.find('.modal-body').append('<button class="btn btn-danger" data-dismiss="modal" data-arquivo="'+ $this.data('arquivo') +'" onclick="jQueryUploadFile.excluirArquivo($(this))"><i class="fa fa-trash"></i> Excluir Arquivo</button><hr>');

            let link = $this.data('link').split('.');

            if(link[ link.length-1 ] !== 'pdf'){
                $modal.find('.modal-dialog').removeClass('modal-lg');

                $modal.find('.modal-body').addClass('pull-center').append('<img src="'+ $this.data('link') +'" style="height: auto; width: 100%">')
            }else{
                $modal.find('.modal-body').addClass('pull-center').append('<iframe height="500px" width="100%" src="'+ $this.data('link') +'"></iframe>');
            }

            $modal.modal('show');
        },
        excluirArquivo: function($this){
            let $loading_title = 'Excluir Arquivo';

            $.ajax({
                type: 'delete',
                dataType: 'json',
                url: '/ordem_servico_arquivo/' + $this.data('arquivo'),
                data: { _token: csrfToken },
                beforeSend: function () {
                    loading.show('Excluindo Arquivo...', {dialogSize: 'sm'});
                },
                success: function (data) {
                    loading.hide();

                    if(data.success){
                        /* Removendo arquivo */
                        $("#file-" + $this.data('arquivo')).remove();

                        swal($loading_title, "Item excluido com sucesso!", {icon: "success"});
                    }else{
                        swal($loading_title, data.alert, {icon: "warning"});
                    }
                },
                error: function (xhr) {
                    loading.hide();

                    swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                }
            });
        }
    }
})(jQuery);