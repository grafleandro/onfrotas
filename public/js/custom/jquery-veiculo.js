$(function(){
    jQueryVeiculo.init();
});

let jQueryVeiculo = (function(){
    let $table = '<table id="tabela-veiculo" class="table table-bordered table-striped">'
                    +'<thead class="table-details-thead">'
                        +'<tr>'
                            +'<th>#</th>'
                            +'<th>Tipo do Veículo</th>'
                            +'<th>Combustível</th>'
                            +'<th>Modelo</th>'
                            +'<th>Placa</th>'
                            +'<th>KM</th>'
                            +'<th>Ação</th>'
                        +'</tr>'
                    +'</thead>'
                    +'<tbody class="table-details-tbody">'

                    +'</tbody>'
                +'</table>';

    return{
        init: function(){
            if($('.table-lista-veiculo').length) {
                let $setTable = $('.table-lista-veiculo');

                $setTable.empty();

                $setTable.html($table);

                this.listarVeiculo($setTable);
            }
        },
        listarVeiculo: function($setTable){
            let $url = '/veiculos/tabela';

            if($("#id_frota").val() !== '' && $("#id_frota").closest('form').attr('method') === 'put'){
                $url = '/frotas/tabela';
            }

            let promiseVeiculo = new Promise(function(resolve, reject){
                $("#tabela-veiculo").dataTable({
                    destroy: true,
                    searching: true,
                    serverSide: true,
                    processing: true,
                    language: {
                        processing: "<img style='width:50px; height:50px;' src='../../css/custom/image/loading.gif'>"
                    },
                    ajax: {
                        url: $url,
                        data: {
                            tipo: $setTable.data('tipo'),
                            frota: ($("#id_frota").val()) ? $("#id_frota").val() : 0
                        },
                        dataSrc: function(json){
                            resolve(json.data);

                            return json.data;
                        }
                    },
                    columns: [
                        { data: "veic_id" },
                        { data: "veti_id" },
                        { data: "veic_combustivel" },
                        { data: "veic_modelo" },
                        { data: "veic_placa" },
                        { data: "veic_km" },
                        { data: "action" },
                    ]
                });
            });

            promiseVeiculo.then(function(result){
                $.each(result, function(index, value){
                    if(value.selecionar){
                        jQueryFrota.addVeiculoVetor(parseInt(value.veic_id));
                    }
                });
            }, function (err) {
                console.log(err);
            });
        },
        editarVeiculo: function($event){
            let $modal = $("#modal-inf");

            $modal.find('.modal-body').empty();
            $modal.find('.modal-footer').empty();

            /* Configuracoes do Modal */
            $.ajax({
                type: "get",
                dataType: "json",
                url: '/veiculos/' + $event.data('veiculo') +'/edit',
                data:{
                    _token: csrfToken
                },
                beforeSend: function () {
                    loading.show('Carregando informações...', {dialogSize: 'sm'});
                },
                success: function(data){
                    loading.hide();

                    if(data.success){
                        $modal.find('.modal-dialog ').addClass('modal-lg');

                        $modal.find('.modal-title').append('Editar Veículo');

                        $modal.find('.modal-body').html(data.view);

                        $modal.find('.modal-footer').html('<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Cancelar</button>' +
                            '<button class="btn btn-primary" data-content="#form-veiculo" data-title="Atualizar Veículo" onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>');

                        $modal.modal('show');

                        /* Configurando Formulario */
                        $modal.find('form').attr('method', 'put');
                        $modal.find('form').attr('action', '/veiculos/' + $event.data('veiculo'));
                    }else{
                        swal("Editar Partido", data.alert, {icon: "warning"});
                    }
                },
                error: function(){
                    loading.hide();

                    swal("Editar Partido", xhr.responseJSON.message, {icon: "danger"});
                }
            });
        },
        deletarVeiculo: function($this){
            if(typeof $this.data('veiculo') !== 'undefined' && $this.data('veiculo') !== ''){

                swal({
                    title: "Excluir Item",
                    text: "Deseja realmente excluir este item ?",
                    icon: "warning",
                    buttons: ["Não", "Sim"],
                }).then((isConfirm) => {
                    if (isConfirm) {
                        $.ajax({
                            type: "delete",
                            dataType: "json",
                            url: '/veiculos/' + $this.data('veiculo'),
                            data:{
                                _token: csrfToken
                            },
                            beforeSend: function () {
                                loading.show('Excluindo dados...', {dialogSize: 'sm'});
                            },
                            success: function(data){
                                loading.hide();

                                if(data.success){
                                    swal("Excluir Veículo", "Item excluido com sucesso!", {icon: "success"});

                                    $this.closest('tr').remove();
                                }else{
                                    swal("Excluir Veículo", data.alert, {icon: "warning"});
                                }
                            },
                            error: function (xhr) {
                                loading.hide();

                                swal("Excluir Veículo", xhr.responseJSON.message, {icon: "warning"});
                            }
                        });
                    }
                });
            }
        },
        adicionarOdometro: function($event){
            swal({
                title: "Adicionar Odómetro",
                content: {
                    element: "input",
                    attributes: {
                        placeholder: "Informar Número do Odómetro",
                        type: "text",
                    },
                },
            }).then(function(subject){
                if(subject !== '') {
                    $.ajax({
                        type: "put",
                        dataType: "json",
                        url: '/veiculos/' + $event.data('veiculo'),
                        data: {
                            _token: csrfToken,
                            odometro: subject
                        },
                        beforeSend: function () {
                            loading.show('Atualizando informações...', {dialogSize: 'sm'});
                        },
                        success: function (data) {
                            loading.hide();

                            if (data.success) {
                                swal({
                                    title: 'Atualizando Odómetro',
                                    text: 'Dados salvos com sucesso.',
                                    icon: "success",
                                    type: "success"
                                }).then(function () {
                                    location.reload();
                                });
                            } else {
                                swal('Atualizando Odómetro', data.alert, {icon: "warning"});
                            }
                        },
                        error: function (xhr) {
                            loading.hide();

                            swal('Atualizando Odómetro', "Erro ao validar informação", {icon: "danger"});
                        }
                    });
                }
            });
        },
        selecionarVeiculo: function($event){
            $($event).autocomplete({
                source: function(request, response){
                    if(request.term !== 0){
                        $.ajax({
                            type: "get",
                            dataType: "json",
                            url: "/veiculos",
                            data: {
                                search_value: request.term
                            },
                            success: function(data){

                                if(data.success){
                                    if(data.veiculos.length){
                                        response($.map(data.veiculos, function (item) {
                                            return {
                                                label: item.veic_modelo + ' - ' + item.veic_placa,
                                                value: item.veic_modelo,
                                                id: item.veic_id
                                            };
                                        }));
                                    } else {
                                        response([{
                                            label: 'Item não encontrato',
                                            value: 'Item não encontrato',
                                            id: 0
                                        }]);
                                    }

                                    $event.removeClass('loading');
                                }else{
                                    swal($loading_title, data.alert, {icon: "warning"});
                                }
                            },
                            error: function(xhr){
                                swal($loading_title, "Erro ao validar informação", {icon: "danger"});
                            }
                        });
                    }else{
                        $('.text-alert').text('(*)');
                    }
                },
                search: function(e, u) {
                    $event.addClass('loading');
                },
                max: 5,
                minLength: 2,
                select: function(event, ui){
                    $event.removeClass('loading');

                    if(ui.item.id) {
                        $("#id_veiculo").val(ui.item.id);

                        if ($('.table-lista-ordem_servico').length) {

                            jQueryHistoricoVeiculo.init();
                        }
                    }
                }
            });
        },
    }
})(jQuery);