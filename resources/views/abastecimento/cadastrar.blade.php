@extends('adminlte::page')

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
@endpush

@section('content')
{{--    @switch(\Illuminate\Support\Facades\Auth::user()->perfil)--}}
{{--        @case (\App\Utils\UsuarioUtils::_US_GESTOR)--}}
{{--            @if(isset($ordem_servico['id_os']))--}}
{{--                @include('ordem_servico.editar_os')--}}
{{--            @else--}}
{{--                @include('ordem_servico.formulario_gestor')--}}
{{--            @endif--}}
{{--            @break--}}
{{--        @case (\App\Utils\UsuarioUtils::_US_COMUM)--}}
{{--            @include('ordem_servico.formulario_padrao')--}}
{{--            @break--}}

{{--    @endswitch--}}
    @include('abastecimento.formulario')
@stop

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-unidade.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-ordem-servico.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mecanica.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-veiculo.js') }}></script>
    <script type="text/javascript" src={{ asset('plugins/iCheck/icheck.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>

    <script>
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    </script>
@endpush