<?php
use App\Utils\FormUtils;
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Lançar Abastecimento</h3>
                <div class="box-tools  pull-right center-block">
                    <a href="{{url('abastecimento/listar')}}" class="btn btn-default"><i class="fa fa-list"></i> Listar abastecimentos</a>
                    @if(isset($bimestre['bime_id']))
                        <a href="{{url('nivel/cadastrar')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Novo Registro</a>
                    @endif
                </div>
            </div>

            @if(isset($ordem_servico['id_os']))
                <form role="form" action="{{ url('ordem_servico/' . $ordem_servico['id_os']) }}" method="put">
            @else
                <form role="form" action="{{ route("abastecimento.store") }}" method="post">
            @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade">Posto de Combustivel</label>
                                <input type="text" class="form-control" id="posto" name="posto"  value="{{ ($ordem_servico['unidade']) ?? '' }}" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="veiculo">Selecionar Veículo</label>
                                <input type="text" class="form-control" id="veiculo" name="veiculo" maxlength="50" placeholder="Marca, Modelo ou Placa" onfocus="jQueryVeiculo.selecionarVeiculo($(this))" value="{{ ($ordem_servico['veiculo']) ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="veiculo">KM</label>
                                <input type="text" class="form-control" id="km" name="km" maxlength="50" placeholder="KM 000000" value="{{ ($ordem_servico['veiculo']) ?? '' }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade">Tipo de Combustivel</label>
                                {{App\Utils\FormUtils::select(App\Utils\DetranUtils::combustiveis(), 'tipo_combustivel', 'tipo_combustivel', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'id', 'titulo')}}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade">Quantidade</label>
                                <input type="text" class="form-control" id="quantidade" name="quantidade"  value="{{ ($ordem_servico['unidade']) ?? '' }}" >
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="veiculo">Valor</label>
                                <input type="text" class="form-control mask-money" id="valor_abastecimento" name="valor_abastecimento" maxlength="50" placeholder="R$ 0,00" value="{{ ($ordem_servico['valor']) ?? '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="desc_pecas">Observações</label>
                                <textarea rows="3" class="form-control" id="observacao" name="observacao">{{ ($ordem_servico['desc_pecas']) ?? '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="id_veiculo" name="id_veiculo" value="{{ ($ordem_servico['id_veiculo']) ?? '' }}">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                <div class="box-footer">
                    <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                    <button type="button" class="btn btn-primary" data-title="Salvando Abastecimento" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-files-o"></i> Lançar </button>
                </div>
            </form>
        </div>
    </div>
</div>