@extends('adminlte::page')

@push('css')
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastrar Colaborador</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                @include('colaboradores.formulario')
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
    <script src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
    <script src={{ asset('js/custom/jquery-calendario.js') }}></script>
    <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script src={{ asset('js/custom/jquery-search-cep.js') }}></script>
@endpush