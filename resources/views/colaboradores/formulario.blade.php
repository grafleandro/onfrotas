<?php
use App\Utils\FormUtils;
use App\Utils\UsuarioUtils;
use App\Utils\CnhCategoriaUtils;
?>
@if(isset($colaborador['id']))
    <head>
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    </head>
@endif

<form role="form" id="form_colaborador"  action="/colaboradores" method="post">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" maxlength="50" value="{{ $colaborador['pessoa']['pess_nome'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="cpf">CPF</label>
                    <input type="text" class="form-control mask-cpf" id="cpf" name="cpf" value="{{ $colaborador['pessoa']['pess_cpf'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <label for="dt_nascimento">Data de Nascimento</label>
                <div class='input-group date'>
                    <input name='dt_nascimento' id='dt_nascimento' type='text' class="form-control input-datepicker" value="{{ $colaborador['pessoa']['pess_data_nasc'] ?? '' }}" required/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="habilitacao">Habilitação</label>
                    <input type="text" class="form-control" id="habilitacao" name="habilitacao" value="{{ $colaborador['pessoa']['pess_habilitacao'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="categoria">Categoria</label>
                    <?php FormUtils::select(CnhCategoriaUtils::CnhCategoria(), 'categoria', 'categoria', 'form-control form-cascade-control',($colaborador['pessoa']['pess_categoria_cnh']) ?? null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <label for="dt_venc_cnh">Data Vencimento CNH</label>
                <div class='input-group date' >
                    <input name='dt_venc_cnh' id='dt_venc_cnh' type='text' class="form-control input-datepicker" value="{{ $colaborador['pessoa']['pess_vencimento_cnh'] ?? '' }}" required/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="perfil">Perfil</label>
                    <?php FormUtils::select(UsuarioUtils::PerfilUsuario(), 'perfil', 'perfil', 'form-control form-cascade-control', ($colaborador['perfil']) ?? null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $colaborador['email'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="frota">Senha</label>
                    <input type="password" class="form-control" id="password" name="password" >
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->

    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    @if(!isset($colaborador['id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Cadastro de Produto" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@if(isset($colaborador['id']))
        <script src={{ asset('bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js') }}></script>
        <script src={{ asset('bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js') }}></script>
        <script src={{ asset('js/custom/jquery-calendario.js') }}></script>
        <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
        <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
@endif