@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastrar Status de Ordens de Serviço</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                @include('configuracao.status_os.formulario')
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
@endpush