<form role="form" class="form_status" id="form_status" method="post" action="{{url('status_ordem_servico')}}">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="nome">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" maxlength="50" value="{{ $status['sose_titulo'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="abreviacao">Abreviação</label>
                    <input type="text" class="form-control" id="abreviacao" name="abreviacao" value="{{ $status['sose_abreviacao'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="descricao">Descrição</label>
                    <input type="text" class="form-control" id="descricao" name="descricao" value="{{ $status['sose_descricao'] ?? '' }}" required>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->

    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

    @if(!isset($status['sose_id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Cadastro de Status" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@if(isset($status['sose_id']))
        <script src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
        <script src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
@endif