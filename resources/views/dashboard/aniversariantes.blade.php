@extends('adminlte::page')

@section('content_header')
    <h1>
        Aniversariantes do Dia
        <small><strong> {{\App\Repository\EmpresaRepository::viewDashboard()['clem_nome_fantasia']}} </strong></small>
    </h1>
@stop

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        @foreach($aniversariantes as $index => $pessoa)
            <div class="col-md-4">
                <!-- Widget: user widget style 1 -->
                <div class="box box-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-black background"></div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3 class="widget-user-username pull-center">{{ ucwords(strtolower($pessoa['pess_nome'])) }}</h3>
                            </div>
                            <div class="col-sm-12 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        <?php
                                            $contato = ($pessoa['pessoa_contato']['cont_cel_1']) ? \App\Utils\Mask::telCelular($pessoa['pessoa_contato']['cont_cel_1']) : '';
                                            $contato .= (($pessoa['pessoa_contato']['cont_tel_fixo'])) ? '/ ' . \App\Utils\Mask::telComercial($pessoa['pessoa_contato']['cont_tel_fixo']) : '';

                                            echo $contato;
                                        ?>
                                    </h5>
                                    <span class="description-text">CONTATO</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            {{--@if($pessoa['pessoa_contato']['cont_cel_1'] || $pessoa['pessoa_contato']['cont_cel_2'])--}}
                                {{--<div class="col-sm-12 pull-center">--}}
                                    {{--<button class="btn btn-default" data-contato="{{ $pessoa['pessoa_contato']['cont_id'] }}" onclick="jQueryDashboard.enviarFelicitacao($(this))"><i class="fa fa-birthday-cake"></i> Enviar Felicitações</button>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>
            <!-- /.col -->
        @endforeach
    </div>
    <!-- /.row -->
@stop

@push('scripts')
    <script src={{ asset('js/custom/jquery-dashboard.js') }}></script>
@endpush