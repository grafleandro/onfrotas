@extends('adminlte::page')

@push('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Veiculos com Licenciamento Vencidos</h3>
                </div>

                <div class="box">
                    <!-- /.box-header -->


                    <div class="box-body table-responsive table-lista-doc-veiculo">

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-doc-veiculo-vencido.js') }}></script>
@endpush