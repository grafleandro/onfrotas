<html>
<body>
<p>O <strong>onFortas</strong> possui um novo cliente.</p>
<p><strong>Razão Social: </strong> {{ $razao_social }}</p>
<p><strong>Email: </strong> {{ $email }}</p>
<p><strong>Contato: </strong> {{ $contato}}</p>
<p><strong>CNPJ: </strong> {{ $cnpj }}</p>
<p><strong>Quantidade de Veículos: </strong> {{ $qtd_veiculo }}</p>
<p><strong>Forma de Pagamento: </strong> {{ ucwords($tipo) }}</p>
<hr>
@include('email.radape')
</body>
</html>