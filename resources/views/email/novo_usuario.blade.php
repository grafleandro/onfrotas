<html>
<body>
    <p>Ola {{ $nome }}, bem vindo ao <a href="http://www.onfrotas.com.br">OnFrotas</a>, o sistema de Gestão de Frotas mais completo. Hoje estamos liberando o seu acesso da plataforma, segue as instruções a baixo.</p>
    <p><a href="http://gestao.onfrotas.com.br/login" target="_blank">Link de Acesso</a></p>
    <p>Login: {{ $email }}</p>
    <p>Senha: {{ $senha }}</p>
    <hr>
    @include('email.radape')
</body>
</html>