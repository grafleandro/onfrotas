<?php
use App\Utils\FormUtils;
use App\Utils\UsuarioUtils;
?>

@extends('adminlte::page')

@push('css')
    {{--CSS--}}
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastrar Usuário Primário</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form" id="form-empresa-usuario" method="post"  action="{{url('colaboradores')}}" >
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4 col-lg-6">
                                <div class="form-group">
                                    <label for="nome">Nome</label>
                                    <input type="text" class="form-control" id="nome" name="nome" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-6">
                                <div class="form-group">
                                    <label for=cpf>CPF</label>
                                    <input type="text" class="form-control mask-cpf" id="cpf" name="cpf" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-6">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control j" id="email" name="email" required>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-6">
                                <div class="form-group">
                                    <label for="perfil">Perfil</label>
                                    <?php FormUtils::select(UsuarioUtils::PerfilUsuario(), 'perfil', 'perfil', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset class="form-group">
                                    <legend style="font-size: 18px !important;">Permissão do Usuário</legend>
                                    <div id="permissao-usuario">
                                        {!! $menus !!}
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" id="usuario_primario" name="usuario_primario" value="1">
                    <input type="hidden" id="clem_id" name="clem_id" value="{{ ($empresa) ?? '' }}">

                    <div class="box-footer">
                        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                        <button type="button" class="btn btn-primary" data-title="Cadastro de Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-checktree.js') }}></script>
    <script>
        $(".checktree").checktree();
    </script>
@endpush