<div class="row">
    <div class="col-md-3 col-lg-3">
        <div class="form-group">
            <label for="phone_fixo">Telefone Fixo</label>
            <input type="text" class="form-control telephone" id="phone_fixo" name="phone_fixo" value="{{ $empresa['empresa_contato']['cont_tel_fixo'] ?? '' }}" >
        </div>
    </div>
    <div class="col-md-3 col-lg-3">
        <div class="form-group">
            <label for="telefone_cel">Telefone Celular 1</label>
            <input type="text" class="form-control  cell_phone_1" id="telefone_cel_1" name="telefone_cel_1" value="{{ $empresa['empresa_contato']['cont_cel_1'] ?? '' }}" >
        </div>
    </div>
    <div class="col-md-3 col-lg-3">
        <div class="form-group">
            <label for="telefone_cel1">Telefone Celular 2</label>
            <input type="text" class="form-control cell_phone_2" id="telefone_cel_2" name="telefone_cel_2" value="{{ $empresa['empresa_contato']['cont_cel_2'] ?? '' }}">
        </div>
    </div>
    <div class="col-md-3 col-lg-3">
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ $empresa['empresa_contato']['cont_email'] ?? '' }}"  required>
        </div>
    </div>
</div>