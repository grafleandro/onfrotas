<?php
use App\Utils\FormUtils;
?>

<div class="row">
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="tipo_logradouro">Tipo do Logradouro</label>
            <?php FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
        </div>
    </div>
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="logradouro">Logradouro</label>
            <input type="text" class="form-control" id="logradouro" name="logradouro" value="{{ $empresa['empresa_endereco']['ende_logradouro_titulo'] ?? '' }}"  required>
        </div>
    </div>
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="numero">Numero</label>
            <input type="text" class="form-control" id="numero" name="numero" value="{{ $empresa['empresa_endereco']['ende_numero'] ?? '' }}" required>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="bairro">Bairro</label>
            <input type="text" class="form-control" id="bairro" name="bairro" value="{{ $empresa['empresa_endereco']['ende_bairro'] ?? '' }}" required>
        </div>
    </div>
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="cep">CEP</label>
            <input type="text" class="form-control" id="cep" name="cep" value="{{ $empresa['empresa_endereco']['ende_cep'] ?? '' }}"  required>
        </div>
    </div>
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="cidade">Cidade</label>
            <input type="text" class="form-control" id="cidade" name="cidade" value="{{ $empresa['empresa_endereco']['ende_cidade'] ?? '' }}" required>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4 col-lg-4">
        <div class="form-group">
            <label for="estado">Estado</label>
            <input type="text" class="form-control" id="estado" name="estado" value="{{ $empresa['empresa_endereco']['ende_estado'] ?? '' }}" required>
        </div>
    </div>
    <div class="col-md-8 col-lg-8">
        <div class="form-group">
            <label for="complemento">Complemento</label>
            <input type="text" class="form-control" id="complemento" name="complemento" value="{{ $empresa['empresa_endereco']['ende_complemento'] ?? '' }}" required>
        </div>
    </div>
</div>