<form role="form" id="form_empresa" method="post"  action="{{url('empresa')}}" >
    <div class="box-body">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="razao_social">Razão Social</label>
                    <input type="text" class="form-control" id="razao_social" name="razao_social" value="{{ $empresa['clem_razao_social'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-3 col-lg-3">
                <div class="form-group">
                    <label for=nome_fantasia>Nome Fantasia</label>
                    <input type="text" class="form-control" id="nome_fantasia" name="nome_fantasia" value="{{ $empresa['clem_nome_fantasia'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="cnpj">CNPJ</label>
                    <input type="text" class="form-control mask-cnpj" id="cnpj" name="cnpj" value="{{ $empresa['clem_cnpj'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-3 col-lg-3">
                <div class="form-group">
                    <label for="inscricao_estadual">Inscrição Estadual</label>
                    <input type="text" class="form-control" id="inscricao_estadual" name="inscricao_estadual" value="{{ $empresa['clem_insc_estadual'] ?? '' }}"  required>
                </div>
            </div>
        </div>
        <hr>
        <!-- Formulario de Contato -->
        @include('empresa.contato')
        <hr>
        <!-- Formulario de Endereco -->
        @include('empresa.endereco')
    </div>
    <!-- /.box-body -->

    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

    @if(!isset($mecanica['ofic_id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Cadastro de Empresa" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-search-cep.js') }}></script>
@endpush