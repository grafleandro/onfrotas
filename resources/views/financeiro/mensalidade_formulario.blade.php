<?php
use App\Utils\FormUtils;
?>

<form role="form"   id="form_mecanica" class="form_mecanica" method="post"  action="{{url('mecanicas')}}" >
    <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="quantidade_veiculo">Quantidade de Veículos</label>
                    <input type="text" class="form-control" id="quantidade_veiculo" name="quantidade_veiculo" maxlength="50" value="{{ $mecanica['ofic_titulo'] ?? '' }}" onblur="jQueryFinanceiro.calcularMensalidade($(this))" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="quantidade_veiculo">Quantidade de Veículos</label>
                    <?php FormUtils::select(\App\Utils\FinanceiroUtils::tiposDePlanos(), 'tipo_plano', 'tipo_plano', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="valor">Valor</label>
                    <input type="text" class="form-control mask-money" id="valor" name="valor" value="{{ $mecanica['ofic_responsavel'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="quantidade_parcelas">Quantidade de Parcelas</label>
                    <input type="text" class="form-control" id="quantidade_parcelas" name="quantidade_parcelas" value="{{ $mecanica['ofic_especializacao'] ?? '' }}" >
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->

    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

    @if(!isset($mecanica['ofic_id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Cadastro de Mecanicas" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@if(isset($mecanica['ofic_id']))
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-financeiro.js') }}></script>
@endif