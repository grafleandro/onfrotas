<script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

@yield('adminlte_js')

<!-- CUSTOM JS -->
<script type="text/javascript" src="{{ asset('js/custom/jquery-app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom/jquery-form.js') }}"></script>
@stack('scripts')