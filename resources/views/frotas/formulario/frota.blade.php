@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
@endpush

<!-- form start -->
@if(isset($frota['id_frota']))
    <form id="form-frotas" role="form" action="/frotas/{{ $frota['id_frota'] }}" method="put">
@else
    <form id="form-frotas" role="form" action="/frotas" method="post">
@endif
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="fazenda">Nome da Unidade</label>
                    <input type="text" class="form-control" id="unidade" name="unidade" maxlength="50" onfocus="jQueryUnidade.selecionarUnidade($(this))" value="{{ ($frota['unidade']) ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="responsavel">Gerente Responsável</label>
                    <input type="text" class="form-control" id="responsavel" value="{{ ($pessoa['pess_nome']) ?? '' }}" name="responsavel" onfocus="jQueryColaborador.selecionarColaborador($(this))">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="responsavel">Código da Frota</label>
                    <input type="text" class="form-control" id="cod_frota" value="{{ ($frota['cod_frota']) ?? '' }}" name="cod_frota">
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <input type="hidden" id="id_unidade" name="id_unidade" value="{{ ($frota['id_unidade']) ?? '' }}">
    <input type="hidden" id="id_frota" name="id_frota" value="{{ ($frota['id_frota']) ?? '' }}">
    <input type="hidden" id="id_pessoa" name="id_pessoa" value="{{ ($pessoa['pess_id']) ?? '' }}">
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" onclick="jQueryFrota.proximoStapVeiculo($(this))"><i class="fa fa-arrow-right"></i> Pŕoximo</button>
    </div>
</form>

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-frota.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-unidade.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-colaborador.js') }}></script>
@endpush