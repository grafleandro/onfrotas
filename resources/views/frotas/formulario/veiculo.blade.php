@push('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

<!-- /.box-header -->
<div class="box-body">
    <button class="btn btn-default" type="button" onclick="jQueryFrota.selecionarTodosVeiculo($(this))"><i class="fa fa-check-square-o"></i> Selecionar Todos</button>
    <button class="btn btn-primary" type="button" onclick="jQueryFrota.salvarVeiculo($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    <hr>
    <div class="box-body table-responsive table-lista-veiculo" data-tipo="{{ \App\Utils\VeiculoUtils::_FROTA }}">

    </div>
</div>
<!-- /.box-body -->

@push('scripts')
    <script src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-veiculo.js') }}></script>
@endpush