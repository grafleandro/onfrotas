<?php
/**
 * Created by PhpStorm.
 * User: julio
 * Date: 20/07/18
 * Time: 18:17
 */
?>

<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
<!-- Theme style -->
<link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

@yield('adminlte_css')

<!-- CUSTOM JS -->
<link rel="stylesheet" href="{{ asset('css/custom/style.css') }}">

@stack('css')
