<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        {{--<img src="" class="user-image" alt="User Image">--}}
        <span class="hidden-xs">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <div class="text-center position-fixed">
            {{--<img src="" class="img-circle" alt="User Image">--}}

            <p>
                {{\Illuminate\Support\Facades\Auth::user()->name}}
{{--                <h5> <strong> {{\App\Repository\EmpresaRepository::viewDashboard()['clem_nome_fantasia']}} </strong></h5>--}}
            </p>
            </div>
        </li>
        <!-- Menu Footer-->
        <li class="user-footer">
            <div class="pull-left">

                <a href="#">
                    <i class="fa fa-address-card-o"></i>
                    Perfil
                </a>
            </div>
            <div class="pull-right">
                <a href="#"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                >
                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                </a>
                <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                    @if(config('adminlte.logout_method'))
                        {{ method_field(config('adminlte.logout_method')) }}
                    @endif
                    {{ csrf_field() }}
                </form>
            </div>
        </li>
    </ul>
</li>