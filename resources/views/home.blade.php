@extends('adminlte::page')

@push('css')
    <link rel=" stylesheet" href="{{ asset('bower_components/Ionicons/css/ionicons.min.css')}} ">
    <link rel=" stylesheet" href="{{ asset('bower_components/morris.js/morris.css')}} ">
@endpush

@section('content_header')
    <h1>
        Painel de Controle
{{--        <small><strong> {{\App\Repository\EmpresaRepository::viewDashboard()['clem_nome_fantasia']}} </strong></small>--}}
    </h1>
@stop

@section('content')
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{count(\App\Model\OrdemServicoModel::where('clem_id', \Illuminate\Support\Facades\Session::get('clem_id'))->get())}}</h3>

                    <p>Ordens de Serviços </p>
                </div>
                <div class="icon">
                    <i class="icon ion-settings"></i>
                </div>
                <a href="#" class="small-box-footer">Mais Informações<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{count(\App\Model\FrotaModel::where('clem_id', \Illuminate\Support\Facades\Session::get('clem_id'))->get())}}<sup style="font-size: 20px"></sup></h3>

                    <p>Nº de Frotas</p>
                </div>
                <div class="icon">
                    <i class="icon fa fa-bus" style="padding-top: 22px;"></i>
                    <i class="icon ion-android-car" style="right: 100px"></i>
                </div>
                <a href="#" class="small-box-footer">Mais Informações<i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>{{count(\App\Model\VeiculosModel::where('clem_id', \Illuminate\Support\Facades\Session::get('clem_id'))->get())}}</h3>

                    <p>Nº de Veículos</p>
                </div>
                <div class="icon">
                    <i class="icon ion-android-car"></i>
                </div>
                <a href="#" class="small-box-footer">Mais Informações <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <!-- ./col -->
    </div>
    <div class="row">
        <div class="col-md-8">
            <!-- LINE CHART -->
            <div class="box box-primary">
                <div class="box-header with-border pull-center">
                    <h3 class="box-title">Ordem de Serviços Abertas</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="ordem-servico-grafico" style="height: 300px;"></div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <div class="col-md-4">
            <!-- PRODUCT LIST -->
            <div class="box box-primary">
                <div class="box-header with-border" style="text-align: center">
                    <h3 class="box-title">Lembretes</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <ul class="products-list product-list-in-box">
                        <li class="item">
                            <div class="product-img">
                                <i class="fa  fa-bullhorn fa-2x"></i>
                            </div>
                            <div class="product-info">
                                <a href="javascript:void(0)" class="product-title">Avisos Gerais</a>
                                <span class="product-description">
                                    Visualizar Avisos Gerais.
                                </span>
                            </div>
                        </li>
                        <!-- /.item -->
                        @if($aniversariantes)
                            <li class="item">
                                <div class="product-img">
                                    <i class="fa  fa-birthday-cake fa-2x"></i>
                                </div>
                                <div class="product-info">
                                    <a href="{{ url('dashboard/aniversariantes') }}" class="product-title">
                                        Aniversariantes do Dia
                                        <span class="label label-info pull-right">{{$aniversariantes}}</span>
                                    </a>
                                    <span class="product-description">
                                        Hoje temos aniversariantes!!!
                                    </span>
                                </div>
                            </li>
                        @endif
                        <!-- /.item -->

                        @if($veiculo_doc_vencimento)
                            <li class="item">
                                <div class="product-img">
                                    <i class="fa fa-automobile fa-2x"></i>
                                </div>
                                <div class="product-info">
                                    <a href="{{ url('dashboard/doc_veiculo') }}" class="product-title">
                                        Documento de Veículo
                                        <span class="label label-danger pull-right">{{$veiculo_doc_vencimento}}</span>
                                    </a>
                                    <span class="product-description">
                                        Documentos Próximo ao vencimento.
                                    </span>
                                </div>
                            </li>
                        @endif
                        <!-- /.item -->
                        @if($cnh_vencida)
                            <li class="item">
                                <div class="product-img">
                                    <i class="fa fa-address-card-o fa-2x"></i>
                                </div>
                                <div class="product-info">
                                    <a href="{{ url('dashboard/cnh_vencidas') }}" class="product-title">
                                        Cateira de Habilitação
                                        <span class="label label-danger pull-right">{{$cnh_vencida}}</span>
                                    </a>
                                    <span class="product-description">
                                    Cateira de Habilitação vencendo hoje
                                </span>
                                </div>
                            </li>
                        @endif
                    <!-- /.item -->
                        @if($ordem_servico_solicitacao)
                        <li class="item">
                            <div class="product-img">
                                <i class="fa fa-files-o fa-2x"></i>
                            </div>
                            <div class="product-info">
                                <a href="{{ url('ordem_servico/listar') }}" class="product-title">
                                    Solicitação de Ordens de Serviços
                                    <span class="label label-warning pull-right">{{$ordem_servico_solicitacao}}</span>
                                </a>
                                <span class="product-description">
                                    Ordem de Serviço Aguardando Aprovação.
                                </span>
                            </div>
                        </li>
                        @endif
                        <!-- /.item -->
                    </ul>
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                    <a href="javascript:void(0)" class="uppercase">Ver Todos</a>
                </div>
                <!-- /.box-footer -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
@stop

@push('scripts')
    <script src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-veiculo.js') }}></script>

    <!-- Morris.js charts -->
    <script src={{ asset('bower_components/raphael/raphael.min.js') }}></script>
    <script src={{ asset('bower_components/morris.js/morris.min.js') }}></script>

    <script>

        $.ajax({
            type: 'get',
            dataType: 'json',
            url: 'dashboard/grafico-os',
            data: {},
            success: function(data){
                if(data.success){
                    let area = new Morris.Area({
                        element: 'ordem-servico-grafico',
                        resize: true,
                        data: data.dados,
                        xkey: 'date',
                        parseTime: false,
                        ykeys: ['amount'],
                        labels: ['OS\'s Abertas'],
                        lineColors: ['#3c8dbc'],
                        hideHover: 'auto',
                        smooth: 'false',
                        fillOpacity: 0.2,
                    });
                }
            },
            error: function(xhr){
                console.log(xhr);
            }
        });
    </script>
@endpush