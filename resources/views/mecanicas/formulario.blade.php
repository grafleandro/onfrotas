@push('css')
    <link rel="stylesheet" href="{{ asset('plugins/iCheck/all.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
@endpush

<form role="form"   id="form_mecanica" class="form_mecanica" method="post"  action="{{url('mecanicas')}}" >
    <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="fazenda">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" maxlength="50" value="{{ $mecanica['ofic_titulo'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for=phone>Telefone</label>
                    <input type="text" class="form-control telephone" id="phone" name="phone" value="{{ $mecanica['mecanica_contato']['cont_cel_1'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="responsavel">Responsavel</label>
                    <input type="text" class="form-control" id="responsavel" name="responsavel" value="{{ $mecanica['mecanica_responsavel']['pess_nome'] ?? '' }}" onfocus="jQueryColaborador.selecionarColaborador($(this))" required>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="ordem_servico_executar" >Está oficina é:</label>
                    <div class="form-group">
                        <label style="margin-right: 10px !important;">
                            <input type="radio" name="oficina_int_ext" class="minimal" value="{{ \App\Utils\OrdemServicoUtils::_INTERNA }}" {{ (isset($mecanica['ofic_interna_externa']) && $mecanica['ofic_interna_externa'] == \App\Utils\OrdemServicoUtils::_INTERNA) ? 'checked' : '' }}> Interna
                        </label>
                        <label>
                            <input type="radio" name="oficina_int_ext" class="minimal" value="{{ \App\Utils\OrdemServicoUtils::_EXTERNA }}" {{ (isset($mecanica['ofic_interna_externa']) && $mecanica['ofic_interna_externa'] == \App\Utils\OrdemServicoUtils::_EXTERNA) ? 'checked' : '' }}> Externa
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="form-group">
                    <label for="especializacao">Qualificação</label>
                    <input type="text" class="form-control" id="especializacao" name="especializacao" value="{{ $mecanica['ofic_especializacao'] ?? '' }}" >
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->

    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="id_pessoa" name="id_pessoa" value="{{ ($mecanica['pess_id']) ?? '' }}">

    @if(!isset($mecanica['ofic_id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Cadastro de Mecanicas" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('plugins/iCheck/icheck.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-colaborador.js') }}></script>

    <script>
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        })
    </script>
@endpush