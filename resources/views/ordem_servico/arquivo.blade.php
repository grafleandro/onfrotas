<?php
use App\Utils\FilesUtils;
use Illuminate\Support\Facades\Storage;
?>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Arquivos da Ordem de Servico</h3>
            </div>
        </div>
        <div class="box-body">
            <button class="btn btn-primary" data-referencia="{{ $ordem_servico['id_os'] }}" data-url="{{ url('/ordem_servico_arquivo') }}" onclick="jQueryUploadFile.uploadFile($(this))"><i class="fa fa-plus"></i> Adicionar Arquivo(s)</button>
            <hr>
            <div class="row">
                @isset($arquivos)
                    @foreach($arquivos as $index => $arquivo)
                        <div class="col-md-2 load-arquivos" id="file-{{ $arquivo['orsa_id'] }}">
                            <img style="height: 100px; border-radius: 10px;" onclick="jQueryUploadFile.visualizarArquivo($(this))" src="{{ asset($arquivo['orsa_thumbnail']) }}" data-arquivo="{{ $arquivo['orsa_id'] }}" data-link="{{ asset($arquivo['orsa_thumbnail']) }}" alt="1" class="image rounded mx-auto d-block">
                            <label>{{ \Carbon\Carbon::parse($arquivo['created_at'])->format('d/m/Y H:i:s') }}</label>
                        </div>
                    @endforeach
                @endisset
            </div>
        </div>
    </div>
</div>

@push('scripts')
    <script type="text/javascript" src={{ asset('js/custom/jquery-upload-file.js') }}></script>
@endpush