<form role="form" class="form_listarOS" id="form_listarOS" method="post" action="{{url('ordem_servico')}}">
    <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="status">Filtrar por Status:</label>
                    <?php App\Utils\FormUtils::select($status_os, 'status', 'status', 'form-control form-cascade-control', null, false, array(), '-- Todos --', true, 'sose_id', 'sose_titulo'); ?>
                </div>
            </div>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
        </div>
    </div>
</form>


