<?php
use App\Utils\FormUtils;
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Solicitar Ordem de Servico</h3>
            </div>

            @if(isset($ordem_servico['id_os']))
                <form role="form" action="{{ url('ordem_servico/' . $ordem_servico['id_os']) }}" method="put">
            @else
                <form role="form" action="{{ url('ordem_servico') }}" method="post">
            @endif
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade">Unidade</label>
                                <input type="text" class="form-control" id="unidade" name="unidade" onfocus="jQueryUnidade.selecionarUnidade($(this))" maxlength="50" value="{{ ($ordem_servico['unidade']) ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="oficina">Oficina</label>
                                <input type="text" class="form-control" id="oficina" name="oficina" maxlength="50" onfocus="jQueryMecanica.selecionarMecanica($(this))" value="{{ ($ordem_servico['oficina']) ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="veiculo">Selecionar Veículo</label>
                                <input type="text" class="form-control" id="veiculo" name="veiculo" maxlength="50" placeholder="Marca, Modelo ou Placa" onfocus="jQueryVeiculo.selecionarVeiculo($(this))" value="{{ ($ordem_servico['veiculo']) ?? '' }}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo">Tipo da Manutenção</label>
                                <?php FormUtils::select($tipo_manutencao, 'tipo_manutencao', 'tipo_manutencao', 'form-control form-cascade-control', ($ordem_servico['id_mati']) ?? null, false, array(), '-- Selecionar --', true, 'mati_id', 'mati_titulo'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="prioridade">Prioridade da Manutenção</label>
                                <?php FormUtils::select($prioridade, 'prioridade_manutencao', 'prioridade_manutencao', 'form-control form-cascade-control', ($ordem_servico['id_mapr']) ?? null, false, array(), '-- Selecionar --', true, 'mapr_id', 'mapr_titulo'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status_os">Status da Ordem de Serviço</label>
                                <?php  FormUtils::select($status_os, 'status_os', 'status_os', 'form-control form-cascade-control', ($ordem_servico['os_status']) ?? null, false, array(), '-- Selecionar --', true, 'sose_id', 'sose_titulo'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="ordem_servico_executar" >Excutar Ordem de Serviço</label>
                                <div class="form-group">
                                    <label style="margin-right: 10px !important;">
                                        <input type="radio" name="ordem_servico_executar" id="ordem_servico_executar" class="minimal" value="{{ \App\Utils\OrdemServicoUtils::_INTERNA }}" {{ (isset($ordem_servico['executar']) && $ordem_servico['executar'] == \App\Utils\OrdemServicoUtils::_INTERNA) ? 'checked' : '' }}> Interna
                                    </label>
                                    <label>
                                        <input type="radio" name="ordem_servico_executar" id="ordem_servico_executar" class="minimal" value="{{ \App\Utils\OrdemServicoUtils::_EXTERNA }}" {{ (isset($ordem_servico['executar']) && $ordem_servico['executar'] == \App\Utils\OrdemServicoUtils::_EXTERNA) ? 'checked' : '' }}> Externa
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="veiculo">Valor da Ordem de Serviço</label>
                                <input type="text" class="form-control mask-money" id="ordem_servico_valor" name="ordem_servico_valor" maxlength="50" placeholder="R$ 0,00" value="{{ ($ordem_servico['valor']) ?? '' }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="desc_serv">Descrição do Serviço</label>
                                <textarea rows="3" class="form-control" id="desc_serv" name="desc_serv">{{ ($ordem_servico['desc_serv']) ?? '' }}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="desc_pecas">Descrição das Peças</label>
                                <textarea rows="3" class="form-control" id="desc_pecas" name="desc_pecas">{{ ($ordem_servico['desc_pecas']) ?? '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="id_unidade" name="id_unidade" value="{{ ($ordem_servico['id_unidade']) ?? '' }}">
                <input type="hidden" id="id_oficina" name="id_oficina" value="{{ ($ordem_servico['id_oficina']) ?? '' }}">
                <input type="hidden" id="id_veiculo" name="id_veiculo" value="{{ ($ordem_servico['id_veiculo']) ?? '' }}">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                <div class="box-footer">
                    <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                    <button type="button" class="btn btn-primary" data-title="Gerando de Ordem de Serviços" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-files-o"></i> Gerar OS</button>
                </div>
            </form>
        </div>
    </div>
</div>