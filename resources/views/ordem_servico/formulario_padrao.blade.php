<?php
use App\Utils\FormUtils;
use App\Utils\UsuarioUtils;
?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Solicitar Ordem de Servico</h3>
            </div>

            <form role="form" action="{{ url('ordem_servico') }}" method="post">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="unidade">Unidade</label>
                                <input type="text" class="form-control" id="unidade" name="unidade" onfocus="jQueryUnidade.selecionarUnidade($(this))" maxlength="50" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="veiculo">Selecionar Veículo</label>
                                <input type="text" class="form-control" id="veiculo" name="veiculo" maxlength="50" placeholder="Modelo ou Placa" onfocus="jQueryVeiculo.selecionarVeiculo($(this))" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="tipo">Tipo da Manutenção</label>
                                <?php FormUtils::select($tipo_manutencao, 'tipo_manutencao', 'tipo_manutencao', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'mati_id', 'mati_titulo'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="prioridade">Prioridade da Manutenção</label>
                                <?php FormUtils::select($prioridade, 'prioridade_manutencao', 'prioridade_manutencao', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'mapr_id', 'mapr_titulo'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="status_os">Status da Ordem de Serviço</label>
                                <?php  FormUtils::select($status_os, 'status_os', 'status_os', 'form-control form-cascade-control', ($ordem_servico['os_status']) ?? null, false, array(), '-- Selecionar --', true, 'sose_id', 'sose_titulo'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="arquivo">Anexar Arquivo</label>
                                <input type="file" class="form-control" id="arquivo" name="arquivo" multiple>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="desc_serv">Descrição do Serviço</label>
                                <textarea rows="3" class="form-control" id="desc_serv" name="desc_serv"  required></textarea>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="desc_pecas">Descrição das Peças</label>
                                <textarea rows="3" class="form-control" id="desc_pecas" name="desc_pecas"  required></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <input type="hidden" id="id_unidade" name="id_unidade" value="{{ ($frota['id_unidade']) ?? '' }}">
                <input type="hidden" id="id_oficina" name="id_oficina" value="{{ ($frota['id_oficina']) ?? '' }}">
                <input type="hidden" id="id_veiculo" name="id_veiculo" value="{{ ($frota['id_veiculo']) ?? '' }}">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                <div class="box-footer">
                    <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
                    <button type="button" class="btn btn-primary" data-title="Solicitar Ordem de Serviços" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-files-o"></i> Solicitar</button>
                </div>
            </form>
        </div>
    </div>
</div>