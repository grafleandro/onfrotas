<form role="form" class="form_relatorioVeiculo" id="form_relatorioVeiculo" method="post" action="{{url('relatorio')}}">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="veiculo">Selecionar Veículo</label>
                    <input type="text" class="form-control" id="veiculo" name="veiculo" maxlength="50" placeholder="Modelo ou Placa" onfocus="jQueryVeiculo.selecionarVeiculo($(this))" required>
                </div>
            </div>
        </div>
        <div class="row imprimir" hidden>
            <div class="col-md-12 pull-center">
                <a class="btn btn-app" id="imprimir" data-url="{{url('relatorio/imprimir')}}" data-imprimi=""  onclick="jQueryForm.imprimir($(this))">
                    <i class="fa fa-print"></i> Imprimir
                </a>
            </div>
        </div>
        <input type="hidden" id="id_veiculo" name="id_veiculo" >
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    </div>
</form>


