@extends('adminlte::page')

<meta name="csrf-token" content="{{ csrf_token() }}">

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush



@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-header with-border">
                    <h3 class="box-title">Histórico do Veículo</h3>
                </div>

                    @include('relatorio.veiculo.formulario')

                    <div class="box-body table-responsive table-lista-ordem_servico">

                    </div>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-historico-veiculo.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-veiculo.js') }}></script>
@endpush