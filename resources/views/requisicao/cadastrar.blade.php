@extends('adminlte::page')

<meta name="csrf-token" content="{{ csrf_token() }}">

@push('css')
    {{--CSS--}}
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Solicitar Requisição</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                @include('requisicao.formulario')
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-unidade.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-veiculo.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
@endpush