<form role="form" class="form_relatorioVeiculo" id="form_relatorioVeiculo" method="post" action="{{url('relatorio')}}">
    <div class="box-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="tipo_ordem_servico">Tipo de Requisição</label>
                    <?php App\Utils\FormUtils::select(\App\Utils\TipoRequisicao::tipoRequisicao(), 'tipo_requisicao', 'tipo_requisicao', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="veiculo">Selecionar Veículo</label>
                    <input type="text" class="form-control" id="veiculo" name="veiculo" maxlength="50" placeholder="Modelo ou Placa" onfocus="jQueryVeiculo.selecionarVeiculo($(this))" required>
                </div>
            </div>
            <div class="col-md-3">
                <label for="dt_inicio">Data Inicio</label>
                <div class='input-group date'>
                    <input name='dt_inicio' id='dt_inicio' type='text' class="form-control input-datepicker" required/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
            <div class="col-md-3">
                <label for="dt_final">Data Final</label>
                <div class='input-group date'>
                    <input name='dt_final' id='dt_final' type='text' class="form-control input-datepicker" required/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
        <div class="row imprimir" hidden>
            <div class="col-md-12 pull-center">
                <a class="btn btn-app" id="imprimir" data-url="{{url('relatorio/imprimirManutencao')}}" onclick="jQueryHistoricoManutencao.imprimirManutencao($(this))">
                    <i class="fa fa-print"></i> Imprimir
                </a>
            </div>
        </div>
        <input type="hidden" id="id_veiculo" name="id_veiculo" >
        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    </div>
    <div class="box-footer">
        <button type="reset" class="btn btn-default" onclick="jQueryRequisicao.reset($(this))"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Lista de Reuisição" data-loading-text="Salvando dados..." onclick="jQueryRequisicao.init($(this))"><i class="fa fa-search"></i> Buscar</button>
    </div>
</form>


