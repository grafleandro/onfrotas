<form role="form"   id="form_requisicao" class="form_requisicao" method="post"  action="{{url('requisicao')}}" >
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="tipo_requisicao">Tipo de Requisição</label>
                    <?php App\Utils\FormUtils::select(\App\Utils\TipoRequisicao::tipoRequisicao(), 'tipo_requisicao', 'tipo_requisicao', 'form-control form-cascade-control', ($requisicao['requ_tipo_requisicao']) ?? null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for=unidade>Unidade</label>
                    <input type="text" class="form-control" id="unidade" name="unidade" onfocus="jQueryUnidade.selecionarUnidade($(this))" maxlength="50" value="{{ ($requisicao['requisicao_unidade']['unid_titulo']) ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="veiculo">Selecionar Veículo</label>
                    <input type="text" class="form-control" id="veiculo" name="veiculo" maxlength="50" placeholder="Marca, Modelo ou Placa" onfocus="jQueryVeiculo.selecionarVeiculo($(this))" value="{{ ($requisicao['requisicao_veiculo']['veic_modelo']) ?? '' }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <div class="form-group">
                    <label for="descricao">Descrição</label>
                    <input type="text" class="form-control" id="descricao" name="descricao" value="{{ $requisicao['requ_descricao'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="form-group">
                    <label for="quantidade">Quantidade</label>
                    <input type="text" class="form-control" id="quantidade" name="quantidade" value="{{ $requisicao['requ_quantidade'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-2 col-lg-2">
                <div class="form-group">
                    <label for="valor">Valor</label>
                    <input type="text" class="form-control mask-money" id="valor" name="valor" maxlength="50" placeholder="R$ 0,00" value="{{ ($requisição['requ_valor']) ?? '' }}">
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->

    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="id_unidade" name="id_unidade" value="{{ ($requisicao['requisicao_unidade']['unid_id']) ?? '' }}">
    <input type="hidden" id="id_veiculo" name="id_veiculo" value="{{ ($requisicao['requisicao_veiculo']['veic_id']) ?? '' }}">

    @if(!isset($requisicao['requ_id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Solicitação de Requisição" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@if(isset($requisicao['requ_id']))
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-unidade.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-veiculo.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskMoney.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
@endif