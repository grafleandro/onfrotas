<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>onFrotas - Simulador</title>
    <meta name="author" content="OutPut Web - Soluções em Tecnologia ">
    <meta name="description" content="Simulador onFrotas. Nesta página, é possível realizar o cálculo de quanto será para utilizar o nosso sistema em sua frota.">
    <meta name="keywords" content="Simulador,onFrotas,Sistema de Gestão Veicular,Veiculos,Carros,Administrar Frota,Frota">

    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('icon/favicon-32x32.png') }}">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/custom/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom/simulador.css') }}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pull-center">
                    <img height="120px" src="{{ asset('image/logo.png') }}">
                </div>
                <h3  class="pull-center">Olá, bem vindo ao <strong>Simulador do onFrotas</strong>.</h3>
                <hr>
                <div class="row">
                    <div class="col-md-4 pull-center">
                        <img width="220px" src="{{ url('image/homem-pensando.jpg') }}">
                    </div>
                    <div class="col-md-8">
                        <h4 class="pull-center">Nosso objetivo é reduzir gastos de sua frota, proporcionando a você nosso cliente o total controle. Desta forma é possível saber informações de seus veículos em questão de segundos</h4>
                        <h4 class="pull-center">
                            E aí? Já pensou quão bom seria ter ter total controle de sua frota pelo celular, tablet ou PC, reduzir os gastos, receber alertas de vencimento de CNH e documentação de seus veículos, com data e valor a ser pago, com apenas alguns cliques?
                            <br><br>
                            Faça parte do onFrotas você também!
                        </h4>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-8">
                        <div class="pull-center">
                            <img style="border-radius: 20px" width="220px" src="{{ url('image/simulador-transacao.jpeg') }}">
                        </div>
                        <h4 class="pull-center">
                            Simule AQUI, com base na quantidade de veículos de sua frota, dentre eles motonetas, motos, carros, caminhões, maquinários agrícolas e outros de seu interesse.<br>
                            É importante salientar, que realizando seu cadastro, você tem direito a <strong>15 dias gratuito</strong>, isso mesmo, você pode
                            testar nosso sistema, e caso satisfaça-o, iremos iremos de fato finalizar seu cadastro.
                        </h4>
                    </div>
                    <div class="col-md-4 form-simulador">
                        <form>
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="contato">Contato</label>
                                            <input type="text" class="form-control cell_phone_1" id="contato" name="contato" maxlength="50" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="qtd-veiculo">Quantidade de Veículos</label>
                                            <input type="text" class="form-control mask-integer" id="qtd-veiculo" name="qtd-veiculo" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer">
                                <button type="button" class="btn btn-primary btn-block" onclick="jQuerySimulador.simulador($(this))"><i class="fa fa-floppy-o"></i> SIMULAR</button>
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="pricing-simulator hidden">
            <hr>
            <div class="col-md-12 bg-mensal pull-center" style="margin-bottom: 20px; border-radius: 5px; padding-bottom: 10px; padding-top: 10px">
                <i style="color: #fff" class="fa fa-star fa-2x"></i>
                <h3 style="color: #fff">Valor Mensal de: <strong><label id="valor-mensal"></label></strong></h3>
                <button class="btn btn-warning" data-tipo="mensal" onclick="jQuerySimulador.preencherForm($(this))"><i class="fa fa-location-arrow"></i> APROVEITAR</button>
            </div>

            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="pricingTable">
                        <div class="pricingTable-header bg-trimestral">
                            <h3 class="title">Trimestral</h3>
                        </div>
                        <div class="pricing-content trimestral">
                            <div class="price-value trimestral">
                                <i class="fa fa-star"></i>
                                <span class="amount" id="valor-tri"></span>
                            </div>
                            <ul class="ul-color-trimestral">
                                <li>15 Dias de Teste</li>
                                <li>Armazenamento Ilimitado</li>
                                <li>Notificação por Email</li>
                                <li>Notificação por SMS</li>
                                <li>Economizar <label id="economia-tri"></label></li>
                            </ul>
                            <a href="#" data-tipo="trimestral" onclick="jQuerySimulador.preencherForm($(this))" class="pricingTable-signup btn-trimestral"><i class="fa fa-location-arrow"></i> APROVEITAR</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="pricingTable">
                        <div class="pricingTable-header bg-semestral">
                            <h3 class="title">Semestral</h3>
                        </div>
                        <div class="pricing-content semestral">
                            <div class="price-value semestral">
                                <i class="fa fa-star"></i>
                                <span class="amount" id="valor-sem"></span>
                            </div>
                            <ul class="ul-color-semestral">
                                <li>15 Dias de Teste</li>
                                <li>Armazenamento Ilimitado</li>
                                <li>Notificação por Email</li>
                                <li>Notificação por SMS</li>
                                <li>Economizar <label id="economia-sem"></label></li>
                            </ul>
                            <a href="#" data-tipo="semestral" onclick="jQuerySimulador.preencherForm($(this))" class="pricingTable-signup btn-semestral"><i class="fa fa-location-arrow"></i> APROVEITAR</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="pricingTable">
                        <div class="pricingTable-header bg-anual">
                            <h3 class="title">Anual</h3>
                        </div>
                        <div class="pricing-content anual">
                            <div class="price-value anual">
                                <i class="fa fa-star"></i>
                                <span class="amount" id="valor-anual"></span>
                            </div>
                            <ul class="ul-color-anual">
                                <li>15 Dias de Teste</li>
                                <li>Armazenamento Ilimitado</li>
                                <li>Notificação por Email</li>
                                <li>Notificação por SMS</li>
                                <li>Economizar <label id="economia-anual"></label></li>
                            </ul>
                            <a href="#" data-tipo="anual" onclick="jQuerySimulador.preencherForm($(this))" class="pricingTable-signup btn-anual"><i class="fa fa-location-arrow"></i> APROVEITAR</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-inf" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
        <div class="modal-dialog ">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close btn-close-x" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>

    <!-- CUSTOM JS -->
    <script type="text/javascript" src="{{ asset('js/custom/jquery-app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-form.js') }}"></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-mask-custom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/custom/jquery-simulador.js') }}"></script>
</body>
</html>