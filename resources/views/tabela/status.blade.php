<?php

use App\Utils\FormUtils;

FormUtils::select($osStatus, 'status_os', 'status_os', 'form-control form-cascade-control', ($sose_id) ?? null, false, array('data-orse="'. $orse_id .'"', 'onchange="jQueryOrdemServico.alterarStatus($(this))"', 'onfocus="jQueryOrdemServico.statusAnterior($(this))"'), '-- Selecionar --', true, 'sose_id', 'sose_titulo');