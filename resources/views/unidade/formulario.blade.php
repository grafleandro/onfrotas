<?php
    use App\Utils\FormUtils;
?>

@push('css')
    <link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}">
@endpush

<form role="form"   id="form_unidade" class="form_unidade" method="post"  action="{{url('unidade')}}" >
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="titulo">Nome da Unidade</label>
                    <input type="text" class="form-control" id="titulo" name="titulo" value="{{ $unidade['unid_titulo'] ?? '' }}" maxlength="50" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="razao_social">Razão Social</label>
                    <input type="text" class="form-control" id="razao_social" name="razao_social" value="{{ $unidade['unid_razao_social'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="cnpj">CNPJ</label>
                    <input type="text" class="form-control mask-cnpj" id="cnpj" name="cnpj" value="{{ $unidade['unid_cnpj'] ?? '' }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="insc_estadual">Insc. Estadual</label>
                    <input type="text" class="form-control" id="insc_estadual" name="insc_estadual" value="{{ $unidade['unid_insc_estadual'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="reponsavel">Responsavel</label>
                    <input type="text" class="form-control" id="responsavel" name="responsavel" value="{{ $unidade['unidade_responsavel']['pess_nome'] ?? '' }}" onfocus="jQueryColaborador.selecionarColaborador($(this))">
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="phone_fixo">Telefone Fixo</label>
                    <input type="text" class="form-control telephone" id="phone_fixo" name="phone_fixo" value="{{ $unidade['unidade_contato']['cont_tel_fixo'] ?? '' }}" >
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="telefone_cel">Telefone Celular</label>
                    <input type="text" class="form-control  cell_phone_1" id="telefone_cel" name="telefone_cel" value="{{ $unidade['unidade_contato']['cont_cel_1'] ?? '' }}" >
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="telefone_cel1">Telefone Celular</label>
                    <input type="text" class="form-control cell_phone_2" id="telefone_cel1" name="telefone_cel1" value="{{ $unidade['unidade_contato']['cont_cel_2'] ?? '' }}">
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ $unidade['unidade_contato']['cont_email'] ?? '' }}"  required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="form-group">
                    <label for="obs">Observações</label>
                    <input type="text" class="form-control" id="obs" name="obs" value="{{ $unidade['unid_observacoes'] ?? '' }}" >
                </div>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="tipo_logradouro">Tipo do Logradouro</label>
                    <?php FormUtils::select($tipoLogradouro, 'tipo_logradouro', 'tipo_logradouro', 'form-control form-cascade-control', null, false, array(), '-- Selecionar --', true, 'enlo_id', 'enlo_titulo'); ?>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="logradouro">Logradouro</label>
                    <input type="text" class="form-control" id="logradouro" name="logradouro" value="{{ $unidade['unidade_endereco']['ende_logradouro_titulo'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="numero">Numero</label>
                    <input type="text" class="form-control" id="numero" name="numero" value="{{ $unidade['unidade_endereco']['ende_numero'] ?? '' }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="bairro">Bairro</label>
                    <input type="text" class="form-control" id="bairro" name="bairro" value="{{ $unidade['unidade_endereco']['ende_bairro'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="cep">CEP</label>
                    <input type="text" class="form-control" id="cep" name="cep" value="{{ $unidade['unidade_endereco']['ende_cep'] ?? '' }}"  required>
                </div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="cidade">Cidade</label>
                    <input type="text" class="form-control" id="cidade" name="cidade" value="{{ $unidade['unidade_endereco']['ende_cidade'] ?? '' }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="form-group">
                    <label for="estado">Estado</label>
                    <input type="text" class="form-control" id="estado" name="estado" value="{{ $unidade['unidade_endereco']['ende_estado'] ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-8 col-lg-8">
                <div class="form-group">
                    <label for="complemento">Complemento</label>
                    <input type="text" class="form-control" id="complemento" name="complemento" value="{{ $unidade['unidade_endereco']['ende_complemento'] ?? '' }}">
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" id="id_pessoa" name="id_pessoa" value="{{ ($unidade['pess_id']) ?? '' }}">

    @if(!isset($unidade['unid_id']))
    <div class="box-footer">
        <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
        <button type="button" class="btn btn-primary" data-title="Cadastro de Unidade" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
    </div>
    @endif
</form>

@push('scripts')
    <script type="text/javascript" src={{ asset('vendor/jquery-ui/jquery-ui.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery.maskedinput.min.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-mask-custom.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-search-cep.js') }}></script>
    <script type="text/javascript" src={{ asset('js/custom/jquery-colaborador.js') }}></script>
@endpush