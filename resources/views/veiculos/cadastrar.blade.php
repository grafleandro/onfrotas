<?php
use App\Utils\FormUtils;
?>

@extends('adminlte::page')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Cadastrar Veículo</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                @include('veiculos.formulario')
            </div>
        </div>
    </div>
@stop