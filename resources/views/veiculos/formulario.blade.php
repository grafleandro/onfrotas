<?php
use App\Utils\FormUtils;
use App\Utils\EstadosUtils;
?>

<form id="form-veiculo" role="form" action="/veiculos" method="post">
    <div class="box-body">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for=tipo_veiculo>Tipo de Veículo</label>
                    <?php FormUtils::select($veiculo_tipo, 'tipo_veiculo', 'tipo_veiculo', 'form-control form-cascade-control', ($veiculo['veti_id']) ?? null, false, array(), '-- Selecionar --', true, 'veti_id', 'veti_titulo'); ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="combustivel">Combustível</label>
                    <?php FormUtils::select($veiculo_combustivel, 'combustivel', 'combustivel', 'form-control form-cascade-control', ($veiculo['veic_combustivel']) ?? null, false, array(), '-- Selecionar --', true, 'id', 'titulo'); ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="cor">Cor</label>
                    <?php FormUtils::select($veiculo_cor, 'cor', 'cor', 'form-control form-cascade-control', ($veiculo['veco_id']) ?? null, false, array(), '-- Selecionar --', true, 'veco_id', 'veco_titulo'); ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="km">UF Emplacamento</label>
                    <?php FormUtils::select(EstadosUtils::estados(), 'uf_emplacamento', 'uf_emplacamento', 'form-control form-cascade-control', ($veiculo['veic_uf_emplacamento']) ?? null, false, array(), '-- Selecionar --', true, "sigla", "nome"); ?>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for=modelo>Modelo</label>
                    <input type="text" class="form-control" id="modelo" name="modelo" value="{{ ($veiculo['veic_modelo']) ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="placa">Placa</label>
                    <input type="text" class="form-control" id="placa" name="placa" value="{{ ($veiculo['veic_placa']) ?? '' }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="renavam">Renavam</label>
                    <input type="text" class="form-control" id="renavam" name="renavam" value="{{ ($veiculo['veic_renavam']) ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="km">KM</label>
                    <input type="text" class="form-control" id="km" name="km" value="{{ ($veiculo['veic_km']) ?? '' }}" required>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="form-group">
                    <label for="ano">Ano</label>
                    <input type="text" class="form-control" id="ano" name="ano" value="{{ ($veiculo['veic_ano']) ?? '' }}" required>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="form-group">
                    <label for="obs">Observações</label>
                    <textarea rows="3" class="form-control" id="obs" name="obs" value="{{ isset($veiculo['veic_']) ?? '' }}" ></textarea>
                </div>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

    @if(!isset($veiculo['veic__id']))
        <div class="box-footer">
            <button type="reset" class="btn btn-default"><i class="fa fa-refresh"></i> Limpar</button>
            <button type="button" class="btn btn-primary" data-title="Salvando Veiculo" data-loading-text="Salvando dados..." onclick="jQueryForm.send_form($(this))"><i class="fa fa-floppy-o"></i> Salvar</button>
        </div>
    @endif
</form>