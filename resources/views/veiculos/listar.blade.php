@extends('adminlte::page')

@push('css')
    <link rel="stylesheet" href="{{ asset('bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endpush

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Listar Veículos</h3>
                </div>

                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive table-lista-veiculo" data-tipo="{{ \App\Utils\VeiculoUtils::_VEICULO }}">

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src={{ asset('bower_components/datatables.net/js/jquery.dataTables.js') }}></script>
    <script src={{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}></script>
    <script src={{ asset('js/custom/jquery-veiculo.js') }}></script>
@endpush