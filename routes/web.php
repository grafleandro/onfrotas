<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/email', 'EmailController@sendEmail');

//Route::get('/event', function(){
//    event(new \App\Events\OrdemServicoEvent('Ola Mundo'));
//});
//
//Route::get('/listen', function(){
//    return view('listenBroadcast');
//});

Route::group(['namespace' => 'Aparelho'], function()
{
    Route::resource('aparelho', 'AparelhoController');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Cliente'], function()
{
    Route::resource('empresa', 'EmpresaController');
});

Route::group(['namespace' => 'OrdemServico'], function()
{
    Route::resource('ordem_servico', 'OrdemServicoController');
    Route::resource('ordem_servico_arquivo', 'OrdemServicoArquivoController');
});

Route::group(['namespace' => 'Unidade'], function()
{
    Route::resource('unidade', 'UnidadeController');
});

Route::group(['namespace' => 'Frotas'], function()
{
    Route::resource('frotas', 'FrotasController');
});

Route::group(['namespace' => 'Veiculos'], function()
{
    Route::resource('veiculos', 'VeiculosController');
});

Route::group(['namespace' => 'Mecanicas'], function()
{
    Route::resource('mecanicas', 'MecanicasController');
});

Route::group(['namespace' => 'Colaboradores'], function()
{
    Route::resource('colaboradores', 'ColaboradoresController');
});

Route::group(['namespace' => 'Configuracao'], function()
{
    Route::resource('ordem_servico_status', 'StatusOrdemServicoController');
});

Route::group(['namespace' => 'Relatorio'], function()
{
    Route::resource('relatorio', 'RelatorioController');
});

Route::group(['namespace' => 'Financeiro'], function()
{
    Route::resource('mensalidade', 'MensalidadeController');
});

Route::group(['namespace' => 'Requisicao'], function()
{
    Route::resource('requisicao', 'RequisicaoController');
});

Route::group(['namespace' => 'Dashboard'], function()
{
    Route::resource('dashboard', 'DashboarController');
});

Route::group(['namespace' => 'Simulador'], function()
{
    Route::resource('simulador', 'SimuladorController');
});

Route::group(['namespace' => 'Abastecimento'], function()
{
    Route::resource('abastecimento', 'AbastecimentoController');
});